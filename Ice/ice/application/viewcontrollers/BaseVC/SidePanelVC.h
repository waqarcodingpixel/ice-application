//
//  MySidePanelVC.h
//  Doo
//
//  Created by Jose on 12/16/15.
//  Copyright © 2015 simon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"

@interface SidePanelVC : LGSideMenuController

@property (nonatomic, strong) UINavigationController    *ActivitiesNavigation;
@property (nonatomic, strong) UINavigationController    *MyProfileNavigation;
@property (nonatomic, strong) UINavigationController    *ICENavigation;
@property (nonatomic, strong) UINavigationController    *SearchNavigation;
@property (nonatomic, strong) UINavigationController    *SettingsNavigation;

@end
