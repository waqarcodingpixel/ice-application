//
//  IceTVCell.m
//  ICE
//
//  Created by LandToSky on 11/12/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "IceTVCell.h"

@implementation IceTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
