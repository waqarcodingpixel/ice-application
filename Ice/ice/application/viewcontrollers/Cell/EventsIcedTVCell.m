//
//  EventsIcedTVCell.m
//  ICE
//
//  Created by LandToSky on 12/28/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "EventsIcedTVCell.h"

@implementation EventsIcedTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
     [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [commonUtils setRoundedRectBorderView:self.liveLbl withBorderWidth:1.0f withBorderColor:appController.appBlueColor withBorderRadius:0.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
