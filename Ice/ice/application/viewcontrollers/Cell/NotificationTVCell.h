//
//  NotificationTVCell.h
//  ICE
//
//  Created by LandToSky on 11/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *dateContainerView;
@property (weak, nonatomic) IBOutlet UILabel *weekdayLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *monthLbl;

@property (weak, nonatomic) IBOutlet UIImageView *userIv;
@property (weak, nonatomic) IBOutlet UIButton *showUserProfileVCBtn;

@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *liveLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *showEventDetailVCBtn;


@end
