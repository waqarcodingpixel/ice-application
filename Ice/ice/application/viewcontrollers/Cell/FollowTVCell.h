//
//  FollowTVCell.h
//  ICE
//
//  Created by LandToSky on 1/10/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowTVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *userAddImg;
@property (strong, nonatomic) IBOutlet UIButton *userAddBtn;
@property (weak, nonatomic) IBOutlet UIImageView *userIV;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *followersCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventsCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *followingLbl;
@property (weak, nonatomic) IBOutlet UILabel *followersLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventsLbl;

@property (strong, nonatomic) IBOutlet UIButton *userProfileBtn;

@end
