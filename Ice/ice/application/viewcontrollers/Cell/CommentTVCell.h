//
//  CommentTVCell.h
//  ICE
//
//  Created by LandToSky on 11/23/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTVCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *userIv;
@property (nonatomic, strong) IBOutlet UILabel *userNameLbl;
@property (nonatomic, strong) IBOutlet UILabel *commentLbl;
@property (nonatomic, strong) IBOutlet UILabel *timeLbl;

@end
