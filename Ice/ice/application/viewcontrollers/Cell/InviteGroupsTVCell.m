//
//  InviteGroupsTVCell.m
//  ICE
//
//  Created by LandToSky on 1/21/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "InviteGroupsTVCell.h"

@implementation InviteGroupsTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
