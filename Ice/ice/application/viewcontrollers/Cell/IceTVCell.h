//
//  IceTVCell.h
//  ICE
//
//  Created by LandToSky on 11/12/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HWViewPager.h>

@interface IceTVCell : UITableViewCell

// Top View
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *topTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *topTimerLbl;
@property (weak, nonatomic) IBOutlet UILabel *topLocationLbl;
@property (weak, nonatomic) IBOutlet UIButton *goDetailBtn;
@property (weak, nonatomic) IBOutlet UILabel *eventDayLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventMonthLBl;
@property (weak, nonatomic) IBOutlet UILabel *membersCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *commentsCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *likesCountLbl;
@property (weak, nonatomic) IBOutlet UIImageView *iceUserImg;
@property (weak, nonatomic) IBOutlet UILabel *iceUserName;
@property (weak, nonatomic) IBOutlet UILabel *iceTimeAgo;
@property (weak, nonatomic) IBOutlet UIButton *iceLikeBtn;
@property (weak, nonatomic) IBOutlet UIButton *userProfileBtn;



// Media View
@property (weak, nonatomic) IBOutlet UIView *mediaView;



// Bottom View
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet HWViewPager *eventImageCv;
@property (weak, nonatomic) IBOutlet UIButton *showCommentBtn;
@property (weak, nonatomic) IBOutlet UIImageView *likeIV;

@property (weak, nonatomic) IBOutlet UIImageView *videoplayImg;


@end
