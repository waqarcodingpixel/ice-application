//
//  EventImageCVCell.h
//  ICE
//
//  Created by LandToSky on 11/13/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventImageCVCell : UICollectionViewCell


@property (nonatomic, strong) IBOutlet UIImageView *eventIv;
@property (weak, nonatomic) IBOutlet UIButton *eventImageSelectBtn;


// Detail Edit Page
@property (nonatomic, strong) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet UIImageView *videoplayImg;
@property (weak, nonatomic) IBOutlet UIButton *videoPlayBtn;

@end
