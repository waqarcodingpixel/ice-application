//
//  NotificationTVCell.m
//  ICE
//
//  Created by LandToSky on 11/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "NotificationTVCell.h"

@implementation NotificationTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

   }

@end
