//
//  SearchEventsVC.m
//  ICE
//
//  Created by LandToSky on 1/11/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "SearchEventsVC.h"
#import "SearchEventsTVCell.h"
#import "UserProfileVC.h"
#import "DetailVC.h"

@interface SearchEventsVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *mainTV;
    NSMutableArray *tagDatas;
    
    NSString *strText;
}


@end

@implementation SearchEventsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) initData
{
    
}


- (void)ReloadData :(NSString *)SearchText
{
    NSLog(@"====> Event SearchText %@",SearchText);
    strText = SearchText;
    [self GetUserSearch:SearchText];

}


-(void)GetUserSearch:(NSString *)SearchText{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        
        NSString *serverUrl = [NSString stringWithFormat:@"%@search?time_zone=%@&radius=18&search_key=%@",ServerUrl,commonUtils.getTimeZone,SearchText];
        NSLog(@"serverUrl %@",serverUrl);
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                // NSLog(@"user data is %@",json);
                
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        NSDictionary *successDic = [json[@"successData"]mutableCopy];
                        NSLog(@"successDic %@",successDic);
                        
                        
                        
                        
                        tagDatas  = [successDic[@"taged_ices"]mutableCopy];
                        commonUtils.taggedData = [tagDatas mutableCopy];
                        [mainTV reloadData];
                        
                        
                    });
                    
                    
                    
                    
                    //  NSLog(@"live array is %@",commonUtils.comingSoonArray);
                    
                }
                if(Jerror!=nil)
                {
                    NSLog(@"json error:%@",Jerror);
                }
            }
        }
    });
}



#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Count ====> %lu",(unsigned long)tagDatas.count);
    return tagDatas.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchEventsTVCell *cell = (SearchEventsTVCell*) [tableView dequeueReusableCellWithIdentifier:@"SearchEventsTVCell"];
    if (cell == nil) {
        cell = [[SearchEventsTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SearchEventsTVCell"];
    }
    
    
    NSMutableDictionary *dictData = tagDatas[indexPath.row];
    
    
    NSString *eventDescStr = dictData[@"description"];
    
    
    cell.lblDay.text = dictData[@"event_day"];
    cell.lblDate.text = dictData[@"event_day_start"];
    cell.lblTime.text = dictData[@"event_day"];
    cell.lblCount.text = [NSString stringWithFormat:@"%lu",[dictData[@"get_members"] count]];
    cell.lblLiked.text = [NSString stringWithFormat:@"%lu",[dictData[@"get_likes"] count]];
    cell.lblMonth.text = dictData[@"event_month"];
    cell.lblIceAge.text = dictData[@"timeago"];
    cell.lblComments.text = [NSString stringWithFormat:@"%lu",[dictData[@"get_comments"] count]];
    cell.lblLocation.text = dictData[@"location"];
    cell.lblUserName.text = dictData[@"get_user"][@"first_name"];
    NSString *imageUrlStr = [NSString stringWithFormat:@"%@",dictData[@"get_user"][@"photo"]];
    [cell.userIV sd_setImageWithURL:[NSURL URLWithString:imageUrlStr]
                       placeholderImage:[UIImage imageNamed:@"user_avatar"]
                                options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];

    [cell.eventDescLbl setAttributedText:[commonUtils changeFontColorOfSubstring:eventDescStr
                                                                       toKeystr:strText
                                                               withKeyFontColor:appController.appBlueColor
                                                          withEventOwnerNameStr:dictData[@"title"]
                                                               withEventNameStr:dictData[@"title "]]];
 
    // Add Button Action
    cell.userProfileBtn.tag = indexPath.row;
    cell.detailBtn.tag = indexPath.row;
    
    [cell.userProfileBtn addTarget:self action:@selector(onUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
    cell.detailBtn.tag = indexPath.row;
    [cell.detailBtn addTarget:self action:@selector(onDetailVC:) forControlEvents:UIControlEventTouchUpInside];
    if (IS_IPHONE_6_OR_ABOVE) {
        
    }
    
    return cell;
}


#pragma mark - Button Action
- (void)onUserProfileVC:(UIButton*)sender
{
    
    NSMutableDictionary *dictData = tagDatas[sender.tag];
    
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = dictData[@"get_user"][@"id"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onDetailVC:(UIButton*)sender
{
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.selectedTag = sender.tag;
    vc.iceDetails = tagDatas[sender.tag];
vc.eventType = @"tagged";
    [self.navigationController pushViewController:vc animated:YES];
}

@end
