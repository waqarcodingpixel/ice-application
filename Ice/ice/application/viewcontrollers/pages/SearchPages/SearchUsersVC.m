//
//  SearchUsersVC.m
//  ICE
//
//  Created by LandToSky on 1/11/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "SearchUsersVC.h"
#import "FollowTVCell.h"
#import "UserProfileVC.h"

@interface SearchUsersVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *mainTV;
    NSMutableArray *userDatas;
    NSString *strText;
}
@end

@implementation SearchUsersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [mainTV setContentInset:UIEdgeInsetsMake(15,0,0,0)];
}

- (void) initData
{
    userDatas = [[NSMutableArray alloc] init];
    
}


- (void)ReloadData :(NSString *)SearchText
{
    NSLog(@"====> User SearchText %@",SearchText);
    
    strText = SearchText;
    [self GetUserSearch:SearchText];
}



-(void)GetUserSearch:(NSString *)SearchText{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        
        NSString *serverUrl = [NSString stringWithFormat:@"%@search?time_zone=%@&radius=18&search_key=%@",ServerUrl,commonUtils.getTimeZone,SearchText];
        NSLog(@"serverUrl %@",serverUrl);
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                // NSLog(@"user data is %@",json);
                
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        NSDictionary *successDic = [json[@"successData"]mutableCopy];
                        NSLog(@"successDic %@",successDic);
                        
                        
                        
                        
                       userDatas  = [successDic[@"users"]mutableCopy];
                        
                        
//                        [commonUtils hideActivityIndicator];
                        [mainTV reloadData];
                        
                        
                    });
                    
                    
                    
                    
                    //  NSLog(@"live array is %@",commonUtils.comingSoonArray);
                    
                }
                if(Jerror!=nil)
                {
                    NSLog(@"json error:%@",Jerror);
                }
            }
        }
    });
}



#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return userDatas.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowTVCell *cell = (FollowTVCell*) [tableView dequeueReusableCellWithIdentifier:@"FollowTVCell"];
    if (cell == nil) {
        cell = [[FollowTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FollowTVCell"];
    }
    
    cell.userProfileBtn.tag = indexPath.row;
    [cell.userProfileBtn addTarget:self action:@selector(onUserProfilePage:) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableDictionary *dict = userDatas[indexPath.row];
    
    cell.userAddBtn.tag = indexPath.row;
    
//    dict
    
    cell.userNameLbl.text = dict[@"first_name"];
    cell.followersCountLbl.text = [NSString stringWithFormat:@"%d",[dict[@"follow_count"] intValue] ];
    cell.followingCountLbl.text = [NSString stringWithFormat:@"%d",[dict[@"following_count"] intValue] ];
    cell.eventsCountLbl.text = [NSString stringWithFormat:@"%d",[dict[@"iced_count"] intValue] ];
    
    [cell.userAddBtn addTarget:self action:@selector(onUserAdd:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *imageUrl = [NSString stringWithFormat:@"%@",dict[@"photo"]];
    [cell.userIV sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                    placeholderImage:[UIImage imageNamed:@"image0"]
                             options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    
    
    
    if ([dict[@"isfollowing_count"] boolValue]) {
        [cell.userAddImg setImage:[UIImage imageNamed:@"follow-check"]];
    } else {
        [cell.userAddImg setImage:[UIImage imageNamed:@"follow-plus"]];
    }
    if (IS_IPHONE_6_OR_ABOVE) {
        [cell.followingLbl setFont:[UIFont fontWithName:@"Lato-Light" size:11]];
          [cell.followersLbl setFont:[UIFont fontWithName:@"Lato-Light" size:11]];
          [cell.eventsLbl setFont:[UIFont fontWithName:@"Lato-Light" size:11]];
          [cell.eventsCountLbl setFont:[UIFont fontWithName:@"Lato-Light" size:11]];
          [cell.followingLbl setFont:[UIFont fontWithName:@"Lato-Light" size:11]];
          [cell.followersCountLbl setFont:[UIFont fontWithName:@"Lato-Light" size:11]];
      
    }
    
    return cell;
}


- (void)onUserProfilePage:(UIButton *)sender
{
    
    NSMutableDictionary *dict = userDatas[sender.tag];
    
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    NSLog(@"other user id is %@",dict[@"id"]);
    vc.otherUserID = dict[@"id"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onUserAdd:(UIButton*)sender
{
    
    NSMutableDictionary *dict = userDatas[sender.tag];
    
    if ([dict[@"isfollowing_count"] boolValue]) {
        [self FollowUnfollowAPI:[NSString stringWithFormat:@"%d",[dict[@"id"] intValue]] AddOrRemove:@"un_follower"];
    } else {
        [self FollowUnfollowAPI:[NSString stringWithFormat:@"%d",[dict[@"id"] intValue]] AddOrRemove:@"add_follower"];
    }
}



-(void)FollowUnfollowAPI:(NSString*)followedID AddOrRemove:(NSString*)addOrRemove{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        //there-is-no-connection warning
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:followedID forKey:@"followed_id"];
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *serverUrl = [NSString stringWithFormat:@"%@%@",ServerUrl,addOrRemove];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:serverUrl];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        

        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if([statusis isEqualToString:@"success"]){

                                                  [self GetUserSearch:strText];
                                              }else if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                              });
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              NSLog(@"Server not responding");
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                              });
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
}

@end
