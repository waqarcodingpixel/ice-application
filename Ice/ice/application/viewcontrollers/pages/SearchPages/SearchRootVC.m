//
//  SearchRootVC.m
//  ICE
//
//  Created by LandToSky on 1/11/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "SearchRootVC.h"
#import "SearchEventsVC.h"
#import "SearchUsersVC.h"
#import "SearchMapVC.h"

@interface SearchRootVC ()<UIScrollViewDelegate, UITextFieldDelegate>
{
    SearchEventsVC *searchEventsVC;
    SearchUsersVC *searchUsersVC;
    SearchMapVC *searchMapVC;
    
    BOOL isLoadedData;
    
    int pageNumber;
    // Custom Search Bar
    IBOutlet UIView *searchView;
    IBOutlet UITextField *searchTxt;
    IBOutlet UIView *searchRemoveView;
    IBOutlet UILabel *searchResultLbl;
    
    
    // TabBar
    IBOutletCollection(UIButton) NSMutableArray *tabBarBtns;
    IBOutlet UILabel *underLbl;
    
    // ScrollView
    IBOutlet UIScrollView *tabScrollView;
    IBOutletCollection(UIView) NSMutableArray *pageView;
    
    // Container View
    IBOutlet UIView *searchEventsContainer;
    IBOutlet UIView *searchUsersContainer;
    IBOutlet UIView *searchMapContainer;
}

@end

@implementation SearchRootVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    pageNumber = 0;
    [self initUI];
    [self initData];
}

- (void)initUI
{
    tabScrollView.delegate = self;
    
    /* Adjust Frame Layout */
    CGRect frame = CGRectZero;
    frame = tabScrollView.frame;
    for (int i = 0; i < 3; i++)
    {
        frame.origin.x = tabScrollView.frame.size.width * i;
        [(UIView*)pageView[i] setFrame:frame];
    }
    
    [tabScrollView setContentSize:CGSizeMake(tabScrollView.frame.size.width*3, tabScrollView.frame.size.height)];
    [tabScrollView setPagingEnabled:YES];
    tabScrollView.delaysContentTouches = NO;
    
    // Search TextField
    searchTxt.delegate = self;
    searchTxt.attributedPlaceholder = [[NSAttributedString alloc]
                                       initWithString:@"Search ICE"
                                           attributes:@{
                                                        NSForegroundColorAttributeName:[UIColor colorWithHex:@"#585858" alpha:1.0f],
                                                                  NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:13.0f]
                                                        }
                                    ];
    
   // [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen)]];
    
    // Hide Container at first
    [searchEventsContainer setHidden:YES];
    [searchUsersContainer setHidden:YES];
    [searchMapContainer setHidden:YES];
    
    [searchView setHidden:NO];
    [searchRemoveView setHidden:YES];
    [searchResultLbl setHidden:YES];
}


- (void)initData
{
    isLoadedData = NO;
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"segue-search-events"]) {
        searchEventsVC = segue.destinationViewController;
    } else if ([segueName isEqualToString:@"segue-search-users"]){
        searchUsersVC = segue.destinationViewController;
    }  else if ([segueName isEqualToString:@"segue-search-map"]){
        searchMapVC = segue.destinationViewController;
    }
}


#pragma mark - onTabBar Button
- (IBAction)onTabBarButton:(UIButton*)sender
{
    NSInteger index = [tabBarBtns indexOfObject:sender];
    NSLog(@"index %ld",(long)index);
    pageNumber = index;
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [tabScrollView setContentOffset:CGPointMake(tabScrollView.frame.size.width * index, 0)];
                         
                     }];
}
#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView != tabScrollView) return;
    
    CGRect frame = underLbl.frame;
    frame.origin.x = scrollView.contentOffset.x / 3;
    underLbl.frame = frame;
    
    /* Get Current Page Number */
    CGFloat width = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    
    pageNumber = page;
    
    if (page == 0) {
        
        [underLbl setBackgroundColor:appController.appBlueColor];
        
    } else if (page == 1){
        [underLbl setBackgroundColor:appController.appPurpleColor];
    } else if (page == 2){
        [underLbl setBackgroundColor:appController.appGrayColor];
    }

    
    [scrollView setScrollEnabled:YES];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(self.isLoadingBase) return NO;

    return YES;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.isLoadingBase) return NO;
    NSString *newStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"New String = %@", newStr);
    if ([newStr length] > 0) {
        [searchRemoveView setHidden:NO];
    } else {
        [searchRemoveView setHidden:YES];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.isLoadingBase) return NO;
    if ( ![commonUtils isEmptyString:textField.text]) {
        [self request];
    } else {
        [commonUtils showAlert:@"Warning" withMessage:@"Please enter search keyword"];
    }
    return [textField resignFirstResponder];
}

#pragma mark - onSearch Button Action
- (IBAction)onRemoveText:(id)sender
{
    if(self.isLoadingBase) return;
    [searchTxt setText:@""];
    [self.view endEditing:YES];
}

- (IBAction)onSearch:(id)sender
{
//    if (self.isLoadingBase) return;
    
    if (!isLoadedData) {
        [self request];
    
    } else {
        
//         Hide Container at first
        [searchEventsContainer setHidden:YES];
        [searchUsersContainer setHidden:YES];
        [searchMapContainer setHidden:YES];
        
        [searchView setHidden:NO];
        [searchRemoveView setHidden:YES];
        [searchResultLbl setHidden:YES];
        
        [searchTxt setText:@""];
        [searchTxt becomeFirstResponder];
        
        isLoadedData = NO;
//        [tabScrollView setContentOffset:CGPointMake(0, 0)];
    }
    
}

#pragma mark - ScrollView Tap
- (void) onTappedScreen {
    if (self.isLoadingBase) return;
    [self.view endEditing:YES];
}

#pragma mark - Request
- (void) request
{
    [self.view endEditing:YES];
//    [tabScrollView setContentOffset:CGPointMake(0, 0)];
    
    
    if (pageNumber == 0) {
        [searchEventsVC ReloadData:searchTxt.text];
    }else if (pageNumber == 1){
        [searchUsersVC ReloadData:searchTxt.text];
    }else {
      [searchMapVC ReloadData:searchTxt.text];
      
    }
    
    
    // Hide Container at first
    [searchEventsContainer setHidden:NO];
    [searchUsersContainer setHidden:NO];
    [searchMapContainer setHidden:NO];
    
    [searchView setHidden:YES];
    [searchRemoveView setHidden:YES];
    [searchResultLbl setHidden:NO];
    
    searchResultLbl.text = searchTxt.text;
    isLoadedData = YES;
}

@end
