//
//  LeftMenuVC.m
//  ICE
//
//  Created by LandToSky on 11/14/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "LeftMenuVC.h"
#import "MyProfileVC.h"
#import "HomeVC.h"
#import "LGSideMenuController.h"

@interface LeftMenuVC () <LGSideMenuControllerDelegate>
{
    SidePanelVC *sidePanelVC;
    UINavigationController *currentNavigation;
    IBOutletCollection(UIButton) NSArray *bergBtns;
    NSInteger activitiesBtnIndex;
    
    __weak IBOutlet UIView *activityView;
    __weak IBOutlet UIView *iceView;
    __weak IBOutlet UIView *searchView;
    __weak IBOutlet UIView *settingsView;
    
}

@end

@implementation LeftMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    [activityView.layer setOpacity:0.5f];
    [iceView.layer setOpacity:0.5f];
    [searchView.layer setOpacity:0.5f];
    [settingsView.layer setOpacity:0.5f];
    CGSize sizeOfImage = CGSizeMake(50, 50);
  _userImage.image =   [self imageByCroppingImage:_userImage.image toSize:sizeOfImage];
  
    }

- (void) initData
{
    activitiesBtnIndex = -1;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    sidePanelVC = (SidePanelVC*) self.sideMenuController;
    sidePanelVC.delegate = self;
   
}


#pragma mark - SidePanel Delegate
- (void)willShowLeftView:(UIView *)leftView sideMenuController:(LGSideMenuController *)sideMenuController
{
    [activityView.layer setOpacity:0.5f];
    [iceView.layer setOpacity:0.5f];
    [searchView.layer setOpacity:0.5f];
    [settingsView.layer setOpacity:0.5f];
}

- (void)didShowLeftView:(UIView *)leftView sideMenuController:(LGSideMenuController *)sideMenuController
{
    HomeVC * homeVC = [[sidePanelVC.ActivitiesNavigation viewControllers] firstObject];
 
    
}



- (void) didHideLeftView:(UIView *)leftView sideMenuController:(LGSideMenuController *)sideMenuController
{
    if (activitiesBtnIndex != -1) {
        HomeVC *homeVC = [[sidePanelVC.ActivitiesNavigation viewControllers] firstObject];
        [homeVC onShowTabView:activitiesBtnIndex];
    }
    activitiesBtnIndex = -1;
}

#pragma mark - Button Action

- (IBAction)onMyProfile:(id)sender
{
    NSLog(@"check log 1");
    if (!sidePanelVC.MyProfileNavigation) {
        
        MyProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileVC"];
        vc.fullName = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
        //if (![[commonUtils.userData valueForKey:@"photo"] isKindOfClass:[NSNull class]]&&![[commonUtils.userData valueForKey:@"photo"]isEqualToString:@""]) {
//            NSString *imageUrlString = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"photo"]];
//            NSURL *url = [NSURL URLWithString:imageUrlString];
//            
//            NSData *data = [NSData dataWithContentsOfURL:url];
//            vc.userImage = [UIImage imageWithData:data];
//            
//            
//        }
//        else {
//           vc.userImage = [UIImage imageNamed:@"user-avatar"];
//            
//            
//        }
//
        vc.userImage = self.userImage.image;
        if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
            vc.iceDesc =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
        }
        else {
            vc.iceDesc = @"";
        }

        sidePanelVC.MyProfileNavigation = [[UINavigationController alloc] initWithRootViewController:vc];
        [sidePanelVC.MyProfileNavigation setNavigationBarHidden:YES];
    }
    
    [sidePanelVC setRootViewController:sidePanelVC.MyProfileNavigation];
    [sidePanelVC hideLeftViewAnimated:sender];
    [self initUI];
}

- (IBAction)onActivities :(id)sender
{
    // Change Highlight Color
    [activityView.layer setOpacity:1.0f];
    [iceView.layer setOpacity:0.5f];
    [searchView.layer setOpacity:0.5f];
    [settingsView.layer setOpacity:0.5f];
    
    if (!sidePanelVC.ActivitiesNavigation) {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        sidePanelVC.ActivitiesNavigation = [[UINavigationController alloc] initWithRootViewController:vc];
        [sidePanelVC.ActivitiesNavigation setNavigationBarHidden:YES];
    }
    
    if (sidePanelVC.rootViewController != sidePanelVC.ActivitiesNavigation) {
        self.fromActivitySelector  = YES;
        [sidePanelVC setRootViewController:sidePanelVC.ActivitiesNavigation];
        self.fromActivitySelector = NO;
    }    
    [sidePanelVC hideLeftViewAnimated:sender];
    
}


- (IBAction)onActivitiesBtns:(UIButton*)sender
{
    
    if (!sidePanelVC.ActivitiesNavigation) {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        sidePanelVC.ActivitiesNavigation = [[UINavigationController alloc] initWithRootViewController:vc];
        [sidePanelVC.ActivitiesNavigation setNavigationBarHidden:YES];
    }
    
    if (sidePanelVC.rootViewController != sidePanelVC.ActivitiesNavigation) {
        self.fromActivitySelector  = YES;
        [sidePanelVC setRootViewController:sidePanelVC.ActivitiesNavigation];
        self.fromActivitySelector = NO;
    }
    
    // Check which index activities button clicked
    activitiesBtnIndex = [bergBtns indexOfObject:sender];
    
    [sidePanelVC hideLeftViewAnimated:sender];
}


- (IBAction)onIce:(id)sender
{
    // Change Highlight Color
    [activityView.layer setOpacity:0.5f];
    [iceView.layer setOpacity:1.0f];
    [searchView.layer setOpacity:0.5f];
    [settingsView.layer setOpacity:0.5f];
    
    // Goto ICEVC
    if (!sidePanelVC.ICENavigation) {
        
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ICEVC"];
        sidePanelVC.ICENavigation = [[UINavigationController alloc] initWithRootViewController:vc];
        [sidePanelVC.ICENavigation setNavigationBarHidden:YES];
    }
    
    [sidePanelVC setRootViewController:sidePanelVC.ICENavigation];
    [sidePanelVC hideLeftViewAnimated:sender];
}



- (IBAction)onSearch:(id)sender
{
    // Change Highlight Color
    [activityView.layer setOpacity:0.5f];
    [iceView.layer setOpacity:0.5f];
    [searchView.layer setOpacity:1.0f];
    [settingsView.layer setOpacity:0.5f];
    
    // Goto SearchRootVC
    if (!sidePanelVC.SearchNavigation) {
        
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchRootVC"];
        sidePanelVC.SearchNavigation = [[UINavigationController alloc] initWithRootViewController:vc];
        [sidePanelVC.SearchNavigation setNavigationBarHidden:YES];
    }

    [sidePanelVC setRootViewController:sidePanelVC.SearchNavigation];
    [sidePanelVC hideLeftViewAnimated:sender];

}

- (IBAction)onSettings:(id)sender
{
    // Change Highlight Color
    [activityView.layer setOpacity:0.5f];
    [iceView.layer setOpacity:0.5f];
    [searchView.layer setOpacity:0.5f];
    [settingsView.layer setOpacity:1.0f];
    
    // Goto SettingsVC
    if (!sidePanelVC.SettingsNavigation) {
        
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
        sidePanelVC.SettingsNavigation = [[UINavigationController alloc] initWithRootViewController:vc];
        [sidePanelVC.SettingsNavigation setNavigationBarHidden:YES];
    }

    [sidePanelVC setRootViewController:sidePanelVC.SettingsNavigation];
    [sidePanelVC hideLeftViewAnimated:sender];
}
-(void)updateUserDetails {
    
          if (![[commonUtils.userData valueForKey:@"photo"] isKindOfClass:[NSNull class]]&&![[commonUtils.userData valueForKey:@"photo"]isEqualToString:@""]) {
              NSString *imageUrlString = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"photo"]];
              NSURL *url = [NSURL URLWithString:imageUrlString];
              
              NSData *data = [NSData dataWithContentsOfURL:url];
              
              _userImage.image = [UIImage imageWithData:data];
          }
          else {
              [_userImage setImage:[UIImage imageNamed:@"user-avatar"]];
          }
          _userName.text = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
          _userFN.text = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
        
    
  

}
- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}
@end
