//
//  RightMenuVC.m
//  ICE
//
//  Created by LandToSky on 11/14/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "RightMenuVC.h"
#import "pUpcomingContentCell.h"
#import "FSCalendar.h"

#import "UserProfileVC.h"
#import "DetailVC.h"

#import "UIViewController+KNSemiModal.h"
#import "DIYCalendarCell.h"

@interface RightMenuVC ()<UITableViewDelegate, UITableViewDataSource, FSCalendarDataSource, FSCalendarDelegate,FSCalendarDelegateAppearance, UIScrollViewDelegate>
{
    IBOutlet FSCalendar *mCalendar;
    IBOutlet UIView *iconDescView;
    IBOutlet UITableView *mTableView;
    
    NSDateFormatter *dateFormatter;
    NSCalendar *gregorian;
    
    NSMutableArray *icedDates, *addedDates,*cellArray,*calendarActivities,*icedDatesOnly,*addedDatesOnly;
    
    BOOL noSelectDate;
    NSString *selectedDate;
    
    
}
@property (nonatomic, strong) IBOutlet UIView *deleteEventsView;


@end

@implementation RightMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUI];
}

- (void)initData
{
    noSelectDate = NO;
    selectedDate = nil;
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEEE MM/dd/yy";
    gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    addedDates = [[NSMutableArray alloc] initWithObjects:@"Wednesday 08/16/17",@"Tuesday 08/15/17", nil];
    addedDates = [NSMutableArray new];
    addedDatesOnly = [NSMutableArray new];
    icedDatesOnly = [NSMutableArray new];
    calendarActivities = [NSMutableArray new];


  //  addedDates = calendarActivities;
    NSLog(@"added dates are %@",addedDates);
    //selectedDate = addedDates[0];
    
}


- (void)initUI
{
    
  
    
    // Calendar Init
    mCalendar.scopeGesture.enabled = YES;
    mCalendar.allowsMultipleSelection = NO; // Allow Multiple selection
    mCalendar.appearance.borderRadius = 0; // Boder type is Rectangle
    mCalendar.appearance.headerMinimumDissolvedAlpha = 0.0f; // Hide Left & Right Month Title
    mCalendar.placeholderType = FSCalendarPlaceholderTypeFillHeadTail; // Calendar Header & Tail Type
    mCalendar.appearance.titlePlaceholderColor = [UIColor colorWithHex:@"#ffffff" alpha:.2];
    
    
    // Canlendar Header View
    mCalendar.calendarHeaderView.backgroundColor = [UIColor clearColor];
    mCalendar.appearance.headerTitleColor = [UIColor whiteColor];
    mCalendar.appearance.headerTitleFont = [UIFont fontWithName:@"Lato-Bold" size:100];
    
    
    // Canlendar Weekday View
    mCalendar.calendarWeekdayView.backgroundColor = [UIColor clearColor];
    mCalendar.appearance.weekdayTextColor = [UIColor whiteColor];
    mCalendar.appearance.weekdayFont = [UIFont fontWithName:@"Lato-Bold" size:20.0f];
    
    
    // Calendar Text Color Setting
    mCalendar.appearance.eventDefaultColor = [UIColor greenColor];
    mCalendar.appearance.selectionColor = [UIColor colorWithHex:@"#6285a5" alpha:1.0f];
    mCalendar.appearance.borderSelectionColor = [UIColor colorWithHex:@"#8dc9f0" alpha:1.0f];
    mCalendar.appearance.titleDefaultColor = [UIColor whiteColor];
    mCalendar.appearance.titleFont = [UIFont fontWithName:@"Lato-Light" size:12.0f];
   
    
    // Calendar Today Setting
    mCalendar.appearance.todayColor = [UIColor clearColor];
    mCalendar.appearance.titleTodayColor = [UIColor whiteColor];
    mCalendar.appearance.todaySelectionColor = [UIColor colorWithHex:@"#6285a5" alpha:1.0f];
    
    // Event Image
    mCalendar.appearance.imageOffset = CGPointMake(0, -10);
    mCalendar.appearance.separators = FSCalendarSeparatorNone;

    [mCalendar registerClass:[DIYCalendarCell class] forCellReuseIdentifier:@"cell"];
    
    
    // Top Border
    mCalendar.topBorder.frame = CGRectMake(0, 70.5f, mCalendar.frame.size.width, 1);
    [mCalendar.topBorder setBackgroundColor:[UIColor colorWithHex:@"#ffffff" alpha:0.05f]];
    
    mCalendar.dataSource = self;
    mCalendar.delegate = self;
    UIView *cancelBtnView = [self.view viewWithTag:1001];
    [commonUtils setRoundedRectBorderView:cancelBtnView withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#767677" alpha:1.0f] withBorderRadius:0.0f];
    
    mTableView.allowsSelection = YES;
    
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    if (commonUtils.userData) {
        [self getCalendarActivities:[NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"id"]]];
    }
    for (int i = 0; i<calendarActivities.count; i++) {
        [dateFormatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        NSDate *serverStartDate = [dateFormatter dateFromString:calendarActivities[i][@"server_start_time"]];
        [dateFormatter setDateFormat:@"EEEE MM/dd/yy"];
        NSString *existing = [dateFormatter stringFromDate:serverStartDate];
        BOOL is_Iced = [calendarActivities[i][@"is_iced"] boolValue];
        BOOL is_added = [calendarActivities[i][@"is_added"] boolValue];
        NSLog(@"is iced is %d",(int)is_Iced);
        if (is_Iced) {
            [icedDatesOnly addObject:existing];
        }
        if (is_added) {
            [addedDatesOnly addObject:existing];
        }

        if ([addedDates containsObject:existing]) {
            
        }
        else{
            [addedDates addObject:existing];
            
        }
        
    }
  
    
    
    if (addedDates.count>0) {
        cellArray = [[NSMutableArray alloc]initWithCapacity:addedDates.count];
    
    }
    for (int i = 0; i<addedDates.count; i++) {
        [cellArray addObject:[NSMutableArray new]];
    }
    NSLog(@"cell array is %@",cellArray);
  //  NSLog(@"added dates are %@",addedDates);
    if (addedDates.count>0) {
        
    
    for (int i = 0; i<calendarActivities.count; i++) {
         [dateFormatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:calendarActivities[i][@"server_start_time"]];
     
        
     //   int index = (int)[addedDates indexOfObject:[dateFormatter stringFromDate:date]];
          [dateFormatter setDateFormat:@"EEEE MM/dd/yy"];
        NSString *dateString = [dateFormatter stringFromDate:date];
        NSLog(@"datestring is %@",dateString);
 
        NSUInteger index = [addedDates indexOfObject:[dateFormatter stringFromDate:date]];
        NSLog(@"index is %lu",(unsigned long)index);
        NSMutableArray *cellArrayObj = [NSMutableArray new];
        cellArrayObj = cellArray[index];
        [cellArrayObj addObject:[[NSMutableDictionary alloc]initWithDictionary:calendarActivities[i]]];
        cellArray[index] = cellArrayObj;
        
    }
        NSLog(@"cell array is %@",cellArray);
    }
    commonUtils.calendarArray = cellArray;
    [mCalendar reloadData];
    [mTableView reloadData];
}

//- (BOOL) prefersStatusBarHidden
//{
//    return YES;
//}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationFade;
}


#pragma mark - Calendar Delegate
- (FSCalendarCell *)calendar:(FSCalendar *)calendar cellForDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    DIYCalendarCell *cell = [calendar dequeueReusableCellWithIdentifier:@"cell" forDate:date atMonthPosition:monthPosition];
    return cell;
}

- (void)calendar:(FSCalendar *)calendar willDisplayCell:(FSCalendarCell *)cell forDate:(NSDate *)date atMonthPosition: (FSCalendarMonthPosition)monthPosition
{
    [self configureCell:cell forDate:date atMonthPosition:monthPosition];
}

// Unable out of current month
- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    //NSLog(@"fs %lu",(unsigned long)FSCalendarMonthPositionCurrent);
    return monthPosition == (FSCalendarMonthPositionCurrent && [addedDates containsObject:[dateFormatter stringFromDate:date]])||(FSCalendarMonthPositionCurrent && [icedDates containsObject:[dateFormatter stringFromDate:date]]);
  //  return YES;
}

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    calendar.frame = (CGRect){calendar.frame.origin,bounds.size};
    
    CGFloat delta = 4.0f;
    
    //Update mTableView Frame
    if (mCalendar.scope == FSCalendarScopeWeek) {
        [iconDescView setHidden:YES];
        [commonUtils moveView:iconDescView withMoveX:0 withMoveY:mCalendar.frame.size.height -delta];
        [commonUtils moveView:mTableView withMoveX:0 withMoveY:calendar.frame.size.height - delta ];
        [commonUtils resizeFrame:mTableView withWidth:mTableView.frame.size.width withHeight:self.view.frame.size.height- 20.0f-calendar.frame.size.height + delta];
        
    } else if (mCalendar.scope == FSCalendarScopeMonth) {
        [iconDescView setHidden:NO];
        [commonUtils moveView:iconDescView withMoveX:0 withMoveY:mCalendar.frame.size.height - delta];
        [commonUtils moveView:mTableView withMoveX:0 withMoveY:calendar.frame.size.height + iconDescView.frame.size.height - delta];
        [commonUtils resizeFrame:mTableView withWidth:mTableView.frame.size.width withHeight:self.view.frame.size.height- 20.0f-calendar.frame.size.height- iconDescView.frame.size.height + delta];
    }
}


- (UIImage *)calendar:(FSCalendar *)calendar imageForDate:(NSDate *)date
{
    NSLog(@"DATA : \n = %@", [dateFormatter stringFromDate:date]);
    if ([icedDatesOnly containsObject:[dateFormatter stringFromDate:date]] && [addedDatesOnly containsObject:[dateFormatter stringFromDate:date]]) {
        return [commonUtils resizeImage: [UIImage imageNamed:@"menu-iced-added"] newSize:CGSizeMake(2.4f, 2.0f)];
        
    } else if ([icedDatesOnly containsObject:[dateFormatter stringFromDate:date]]){
        return [commonUtils resizeImage: [UIImage imageNamed:@"menu-iced"] newSize:CGSizeMake(2.0f, 2.0f)];
        
    } else if ([addedDatesOnly containsObject:[dateFormatter stringFromDate:date]]){
        return [commonUtils resizeImage: [UIImage imageNamed:@"menu-added"] newSize:CGSizeMake(2.0f, 2.0f)];
    }
    return nil;
}


- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    
    if ([addedDates containsObject:[dateFormatter stringFromDate:date]]){
        NSInteger section = [addedDates indexOfObject:[dateFormatter stringFromDate:date]];
        [self tableViewScrollToEventDate:section];
    } else {
//        selectedDate = [dateFormatter stringFromDate:calendar.selectedDate];
//        [self configureVisibleCells];
    }
}


- (void)calendar:(FSCalendar *)calendar didDeselectDate:(nonnull NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
 //   [self configureVisibleCells];
}


// Setting Today boder Color
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderDefaultColorForDate:(NSDate *)date{
    if ([gregorian isDateInToday:date]) {
        
        return [UIColor colorWithHex:@"#324a66" alpha:1.0f];
    }
    return nil;
}


#pragma mark - Private methods

- (void)configureVisibleCells
{
    [mCalendar.visibleCells enumerateObjectsUsingBlock:^(__kindof FSCalendarCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDate *date = [mCalendar dateForCell:obj];
        FSCalendarMonthPosition position = [mCalendar monthPositionForCell:obj];
        [self configureCell:obj forDate:date atMonthPosition:position];
    }];
}

- (void)configureCell:(FSCalendarCell *)cell forDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    DIYCalendarCell *diyCell = (DIYCalendarCell *)cell;
    
    // Custom today circle
    diyCell.todayShowIv.hidden = ![gregorian isDateInToday:date];
    
    // Configure selection layer
    if (monthPosition == FSCalendarMonthPositionCurrent || mCalendar.scope == FSCalendarScopeWeek)
    {
        if ([selectedDate isEqualToString:[dateFormatter stringFromDate:date]]) {
            diyCell.backgroundView.backgroundColor = [UIColor clearColor];
            
        } else {
            diyCell.backgroundView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
        }
        
        
    } else if (monthPosition == FSCalendarMonthPositionNext || monthPosition == FSCalendarMonthPositionPrevious) {
        
//        diyCell.selectionLayer.hidden = YES;
        if ([mCalendar.selectedDates containsObject:date]) {
            diyCell.titleLabel.textColor = mCalendar.appearance.titlePlaceholderColor; // Prevent placeholders from changing text color
        }
    }
}



#pragma mark - < UITableView Delegate>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   
    return addedDates.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cellArray[section] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0)
        return @"Section 1";
    else
        return @"Section 2";
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [view setBackgroundColor:[UIColor colorWithHex:@"#2a445c" alpha:1.0f]];
    
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, tableView.frame.size.width -20, 30)];
    [label setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
    [label setTextColor:[UIColor whiteColor]];
    
    
    NSString *string = [commonUtils upperCaseWeekdaysFromDateStr:addedDates[section]];
    [label setText:string];
    
    // Add seperator border lineLable
    UILabel *lineLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 28, tableView.frame.size.width, 1)];
    [lineLbl setBackgroundColor:RGBA(255, 255, 255, 0.05f)];
    
    [view addSubview:label];
    [view addSubview:lineLbl];
    
    return view;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    pUpcomingContentCell *upComingContentCell;
    upComingContentCell = [tableView dequeueReusableCellWithIdentifier:@"pUpcomingContentCell"];
    if (upComingContentCell == nil) {
        upComingContentCell = [[pUpcomingContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pUpcomingContentCell"];
    }
    
    NSDictionary *dic = cellArray[indexPath.section][indexPath.row];
    // User Name Label
    NSMutableAttributedString *userNameStr = [[NSMutableAttributedString alloc] initWithString:@"via "
                                                                                    attributes:@{                                                                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Light" size:11.0f],                                                                                                 NSForegroundColorAttributeName:[UIColor colorWithHex:@"#d1d5d9" alpha:1.0f]
                                                                                                 }];
    NSString *userName = dic[@"get_user"][@"first_name"];
    NSMutableAttributedString *userNameStr1 = [[NSMutableAttributedString alloc] initWithString:userName
                                                                              attributes:@{                                                                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:11.0f],                                                                                                 NSForegroundColorAttributeName:[UIColor colorWithHex:@"#d1d5d9" alpha:1.0f]                                                                                                                                                                                            }];
    [userNameStr appendAttributedString:userNameStr1];    
    [upComingContentCell.userNameLbl setAttributedText:userNameStr];
    NSString *imageUrlStr =  [NSString stringWithFormat:@"%@",dic[@"get_user"][@"photo"]];
    [upComingContentCell.userIv sd_setImageWithURL:[NSURL URLWithString:imageUrlStr]
                       placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    upComingContentCell.titleLbl.text = dic[@"title"];
    upComingContentCell.addressLbl.text = dic[@"location"];
    BOOL is_live = [dic[@"is_live"]boolValue];
    if (is_live) {
        upComingContentCell.liveLbl.hidden = NO;
    }
    else{
        upComingContentCell.liveLbl.hidden = YES;
    }
    NSDate *today = [NSDate date];
    
    
    NSDate *todayDate = [self toLocalTime:today];
    
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    
    NSDate * endDateFromApi = [startTimeformatter dateFromString:dic[@"server_end_time"]];
    // your date
    
    
    NSComparisonResult result;
    
    
    result = [endDateFromApi compare:today]; // comparing two dates
    
    
    NSLog(@"today date is %@",[startTimeformatter stringFromDate:today]);
    NSLog(@"end date from api is %@",[startTimeformatter stringFromDate: endDateFromApi]);

    
    
    if(result==NSOrderedAscending)
    {
        NSLog(@"NSOrderedAscending"); // Today is large
        NSString *endDate = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
        NSString *endMonth = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
        upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"ended at %@.%@th",endMonth,endDate];
        
    }
    
    else if(result==NSOrderedDescending){
        NSLog(@"server date  is greater");

        NSLog(@"NSOrderedDescending");
        
        NSString *endDate = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
        NSString *endMonth = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
        upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"ends %@.%@th",endMonth,endDate];
    }
    
    
    else{
        NSLog(@"Equal");
        NSLog(@"end date from api is %@",[startTimeformatter stringFromDate: endDateFromApi]);
       upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"until %@",dic[@"event_end_time"]];
       NSLog(@"Both dates are same");
    }

//    upComingContentCell.showDetailBtn.userInteractionEnabled = NO;
//    upComingContentCell.showUserProfileBtn.userInteractionEnabled = NO;
    upComingContentCell.showDetailBtn.tag = indexPath.row;
    [upComingContentCell.showDetailBtn setTitle:[NSString stringWithFormat:@"%ld",(long)indexPath.section] forState:UIControlStateNormal];
    [upComingContentCell.showDetailBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [upComingContentCell.showDetailBtn addTarget:self action:@selector(onShowEventDetailVC:) forControlEvents:UIControlEventTouchUpInside];
    upComingContentCell.showUserProfileBtn.tag = indexPath.row;
    [upComingContentCell.showUserProfileBtn setTitle:[NSString stringWithFormat:@"%ld",(long)indexPath.section] forState:UIControlStateNormal];
    [upComingContentCell.showUserProfileBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [upComingContentCell.showUserProfileBtn addTarget:self action:@selector(onShowUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
    return upComingContentCell;
}


// When user click date in Calendar, table view scroll to that event section
- (void) tableViewScrollToEventDate:(NSInteger)section
{
    noSelectDate = YES;
    CGRect rect = [mTableView rectForSection:section];
    [mTableView setContentOffset:CGPointMake(0, rect.origin.y) animated:YES];

    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
//    [mTableView scrollToRowAtIndexPath:indexPath
//                     atScrollPosition:UITableViewScrollPositionTop
//                             animated:YES];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
   noSelectDate = NO;
    
   NSIndexPath *firstVisibleIndexPath = [[mTableView indexPathsForVisibleRows] objectAtIndex:0];
   selectedDate = addedDates[firstVisibleIndexPath.section];
   [self configureVisibleCells];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (noSelectDate)
        return;
    
    NSIndexPath *firstVisibleIndexPath = [[mTableView indexPathsForVisibleRows] objectAtIndex:0];
    
    static NSInteger prevSection = -1;
    if (prevSection != firstVisibleIndexPath.section) {
        
        selectedDate = addedDates[firstVisibleIndexPath.section];
        [self configureVisibleCells];
        prevSection = firstVisibleIndexPath.section;
        
    }
    
//    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height + 3.0f;
//    if (bottomEdge >= scrollView.contentSize.height) {
//        if (![selectedDate isEqualToString:finalSelectDate]) {
//            selectedDate = finalSelectDate;
//            [self configureVisibleCells];
//            prevSection = [mTableView numberOfSections] - 1;
//        }
//    }

}


#pragma mark - On Other VC
- (void)onShowUserProfileVC:(UIButton*)sender
{
    
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    NSInteger index = sender.tag;
    NSInteger sectionIndex = [sender.titleLabel.text integerValue];
    NSDictionary *dic = cellArray[sectionIndex][index];
    vc.otherUserID = [NSString stringWithFormat:@"%@",dic[@"get_user"][@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowEventDetailVC:(UIButton*) sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.isShowFromRightMenuEvents = YES;
        vc.eventType = @"calendar";
     vc.selectedTag = sender.tag;
    vc.comingFrom = @"calendar";
    NSInteger sectionIndex = [sender.titleLabel.text integerValue];
    vc.sectionTag = sectionIndex;
    NSLog(@"vc section tag %ld",(long)vc.sectionTag);
    NSLog(@"vc selected tag %ld",(long)vc.selectedTag);
    vc.iceDetails = cellArray[sectionIndex][sender.tag];

    [self.navigationController pushViewController:vc animated:YES];
    
    
    
   

}



#pragma mark - OnLeft Right
- (IBAction)previousClicked:(id)sender
{
    NSDate *currentMonth = mCalendar.currentPage;
    NSDate *previousMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [mCalendar setCurrentPage:previousMonth animated:YES];
}

- (IBAction)nextClicked:(id)sender
{
    NSDate *currentMonth = mCalendar.currentPage;
    NSDate *nextMonth = [gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [mCalendar setCurrentPage:nextMonth animated:YES];
}

#pragma mark - my Calendar Function
-(BOOL)isDateFalls:(NSDate *)date withYourYear:(NSInteger)year andYourMonth:(NSInteger)month
{
    NSDateComponents *dateComponents = [gregorian components:(NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:date];
    if(dateComponents.year == year && dateComponents.month == month){
        
        return YES;
    }
    return NO;
}

#pragma mark - Select Calendar Date

- (void)onShowBottomView{
    if(self.isLoadingBase) return;
    
    [self presentSemiView:self.deleteEventsView withOptions:@{
                                                        KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                        KNSemiModalOptionKeys.parentAlpha : @(1.0),
                                                        KNSemiModalOptionKeys.transitionStyle : @(KNSemiModalTransitionStyleFadeIn),
                                                        KNSemiModalOptionKeys.animationDuration : @(0.1)
                                                        }];
}

- (IBAction)onDeleteEvents:(id)sender
{
    NSLog(@"Delete Events");
    [self dismissSemiModalView];
}

- (IBAction)onCancel:(id)sender
{
    [self dismissSemiModalView];
}
-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    
    
}
-(void)getCalendarActivities: (NSString*)userID{
    
    //Background Thread
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
  
    NSString *serverUrl = [NSString stringWithFormat:@"%@get_activities_calender/%@?time_zone=%@",ServerUrl,userID,commonUtils.getTimeZone];
    NSLog(@"serverUrl %@",serverUrl);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
    // Create a mutable copy of the immutable request and add more headers
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
    NSLog(@"%@", request.allHTTPHeaderFields);
    
    
    NSURLResponse *response;
    
    NSError *error = nil;
    
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(error!=nil)
    {
        NSLog(@"web service error:%@",error);
    }
    else
    {
        if(receivedData !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:receivedData
                                 options:kNilOptions
                                 error:&Jerror];
            // NSLog(@"user data is %@",json);
            
            if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    //Run UI Updates
                    
                    NSDictionary *successDic = [json[@"successData"]mutableCopy];
                    NSLog(@"successDic %@",successDic);
                    calendarActivities = successDic[@"activities"];
                    
                    
                  
                    
                });
                
                
                
                
                //  NSLog(@"live array is %@",commonUtils.comingSoonArray);
                
            }
            if(Jerror!=nil)
            {
                NSLog(@"json error:%@",Jerror);
            }
        }
    }
     });
}

@end
