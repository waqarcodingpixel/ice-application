//
//  locationPickerViewController.h
//  ICE
//
//  Created by Vengile on 19/07/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlaces/GooglePlaces.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
@interface locationPickerViewController : BaseViewController<MKMapViewDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate,GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *locationPickerMap;
- (IBAction)locationPickedBtn:(id)sender;
@property (weak, nonatomic) IBOutlet GMSMapView *gmapView;


@end
