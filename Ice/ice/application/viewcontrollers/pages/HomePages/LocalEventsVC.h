//
//  LocalEventsVC.h
//  ICE
//
//  Created by LandToSky on 11/14/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocalEventsVC : BaseViewController
    @property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *radiusLbl;
- (IBAction)sliderValueChanged:(id)sender;
- (IBAction)addNewIceBtn:(id)sender;

@end
