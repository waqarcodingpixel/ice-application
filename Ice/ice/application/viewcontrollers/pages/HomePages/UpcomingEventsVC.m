//
//  UpcomingEventsVC.m
//  ICE
//
//  Created by LandToSky on 11/14/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "UpcomingEventsVC.h"
#import "IceTVCell.h"
#import "EventImageCVCell.h"
#import <HWViewPager.h>
#import "UserProfileVC.h"
#import "ICEVC.h"
#import "DetailVC.h"
#import "CommentVC.h"
#import "PhotoVideoShowVC.h"
#import "ICEVC.h"
#import <SVProgressHUD.h>
@interface UpcomingEventsVC ()< UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, HWViewPagerDelegate>
{
    IBOutlet UITableView *table;
    NSMutableArray *upnextArrayCopy;
}

@end

@implementation UpcomingEventsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)initData
{
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"upcoming"
                                               object:nil];

    [self getDashBoard];
}
#pragma mark - UITableView
#pragma mark - UITableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return 10;
    return commonUtils.upNextEventsArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if (commonUtils.upNextEventsArray.count>0)
    {
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        table.backgroundView = nil;
    }
    else
    {
       
    }
    
    return numOfSections;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 2 || indexPath.row == 5) {
        return tableCellHeight1;
    }
    // "Else"
    return tableCellHeight0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"IceTVCell";
    NSMutableArray *getCommentsArray = commonUtils.upNextEventsArray[indexPath.row][@"get_comments"];
    NSMutableArray *getLikesArray = commonUtils.upNextEventsArray[indexPath.row][@"get_likes"];
    IceTVCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.topTitleLbl.text = commonUtils.upNextEventsArray[indexPath.row][@"title"];
    cell.topLocationLbl.text = commonUtils.upNextEventsArray[indexPath.row][@"location"];
    cell.membersCountLbl.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"get_members_count"]];
    cell.commentsCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)getCommentsArray.count];
    cell.likesCountLbl.text =  [NSString stringWithFormat:@"%lu",(unsigned long)getLikesArray.count];
    cell.eventDayLbl.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"event_day"]];
    cell.eventDateLbl.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"event_day_start"]];
    cell.eventMonthLBl.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"event_month"]];
    NSString *imageUrlStr =  [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"get_user"][@"photo"]];
    [cell.iceUserImg sd_setImageWithURL:[NSURL URLWithString:imageUrlStr]
                       placeholderImage:[UIImage imageNamed:@"image0"]
                                options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    cell.iceUserName.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"get_user"][@"first_name"]];
    cell.topTimerLbl.text = [NSString stringWithFormat:@"begins %@",commonUtils.upNextEventsArray[indexPath.row][@"event_start_time"]];
    cell.iceTimeAgo.text = [NSString stringWithFormat:@"iced %@",commonUtils.upNextEventsArray[indexPath.row][@"timeago"]];
    if (cell == nil) {
        cell = [[IceTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    if (indexPath.row == 2 || indexPath.row == 5) {
        [cell.mediaView setHidden:YES];
    } else {
        [cell.mediaView setHidden:NO];
    }
    
    cell.eventImageCv.pagerDelegate = self;
    cell.eventImageCv.dataSource = self;
    cell.eventImageCv.tag = indexPath.row;
    
    cell.goDetailBtn.tag = indexPath.row;
    [cell.goDetailBtn addTarget:self action:@selector(onShowDetailVC:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.showCommentBtn.tag = indexPath.row;
    [cell.showCommentBtn addTarget:self action:@selector(onShowCommentVC:) forControlEvents:UIControlEventTouchUpInside];
    cell.iceLikeBtn.tag = indexPath.row;
    [cell.iceLikeBtn addTarget:self action:@selector(onLikeIce:) forControlEvents:UIControlEventTouchUpInside];
    BOOL is_Like = [commonUtils.upNextEventsArray[indexPath.row][@"is_like_count"]boolValue];
    if (is_Like) {
        cell.likesCountLbl.textColor = [UIColor whiteColor];
        cell.likeIV.image = [UIImage imageNamed:@"card-count-icon3"];
    }
    else{
        cell.likesCountLbl.textColor = [UIColor redColor];
        cell.likeIV.image = [UIImage imageNamed:@"card-count-icon2"];
    }

    return cell;
}

#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSMutableArray *getImages =commonUtils.upNextEventsArray[collectionView.tag][@"get_images"];
    return getImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
  
    NSMutableArray *imagesArray = commonUtils.upNextEventsArray[collectionView.tag][@"get_images"];
    

    EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
    cell.videoPlayBtn.tag = indexPath.row;
    [cell.videoPlayBtn setTitle:[NSString stringWithFormat:@"%ld",(long)collectionView.tag] forState:UIControlStateNormal];
    [cell.videoPlayBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [cell.videoPlayBtn addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];

    NSString *imageUrl;
    if ([imagesArray[indexPath.row][@"type"]isEqualToString:@"image"]) {
        imageUrl  = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,imagesArray[indexPath.row][@"image"]];
        [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"image0"]
                                 options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        cell.videoPlayBtn.hidden = YES;
        
    }
    else{
        imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,imagesArray[indexPath.row][@"poster"]];
        [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"image0"]
                                 options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        cell.videoPlayBtn.hidden = NO;
    }

    
    
    cell.eventImageSelectBtn.tag = indexPath.item;
    [cell.eventImageSelectBtn setTitle:[NSString stringWithFormat:@"%ld",(long)collectionView.tag] forState:UIControlStateNormal];
    [cell.eventImageSelectBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [cell.eventImageSelectBtn addTarget:self action:@selector(onSelectEventImageView:) forControlEvents:UIControlEventTouchUpInside];
 

    return cell;
    
}

#pragma mark- cell button methods
- (void) onShowDetailVC:(UIButton*) sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.eventType = @"upcoming";
    vc.comingFrom = @"upcoming";
    if (sender.tag == 2 || sender.tag ==5) {
        vc.isNoExistPhoto = YES;
    } else {
        vc.isNoExistPhoto = NO;
    }
    vc.selectedTag = sender.tag;
    vc.iceDetails = commonUtils.upNextEventsArray[sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void) onLikeIce:(UIButton*)sender{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    IceTVCell *cell = [table cellForRowAtIndexPath:indexPath];
    BOOL is_Like = [commonUtils.upNextEventsArray[sender.tag][@"is_like_count"]boolValue];
    if (is_Like) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *likesCount = [f numberFromString:cell.likesCountLbl.text];
        NSInteger likes = [likesCount integerValue];
        if (likes>0) {
            likes--;
        }
        cell.likeIV.image = [UIImage imageNamed:@"card-count-icon2"];
        cell.likesCountLbl.textColor = [UIColor redColor];
        cell.likesCountLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
        [upnextArrayCopy[sender.tag]setValue:@"0" forKey:@"is_like_count"];
        commonUtils.upNextEventsArray = [[NSMutableArray alloc]initWithArray:upnextArrayCopy copyItems:NO];
        [commonUtils unLikeICE:commonUtils.upNextEventsArray[sender.tag][@"id"] :cell];
    }
    else{
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *likesCount = [f numberFromString:cell.likesCountLbl.text];
        NSInteger likes = [likesCount integerValue];
        likes++;
        cell.likeIV.image = [UIImage imageNamed:@"card-count-icon3"];
        cell.likesCountLbl.textColor = [UIColor whiteColor];
        cell.likesCountLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
        [upnextArrayCopy[sender.tag]setValue:@"1" forKey:@"is_like_count"];
        commonUtils.upNextEventsArray = [[NSMutableArray alloc]initWithArray:upnextArrayCopy copyItems:NO];
        [commonUtils likeICE:commonUtils.upNextEventsArray[sender.tag][@"id"] :cell ];
    }
    
    
}
- (void) onShowCommentVC:(UIButton*) sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    IceTVCell *cell = [table cellForRowAtIndexPath:indexPath];
    
    CommentVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentVC"];
    vc.iceDetailsArray = commonUtils.upNextEventsArray[sender.tag][@"get_comments"];
    NSString *iceID = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[sender.tag][@"id"]];
    vc.iceID = iceID;
    vc.passedTag = sender.tag;
    vc.providesPresentationContextTransitionStyle = YES;
    vc.definesPresentationContext = YES;
    vc.cell = cell;
    vc.comingfrom = @"upnext";
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}
- (void)onSelectEventImageView:(UIButton*) sender
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    NSLog(@"Selected Event Image View");
    PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
    NSNumber *CVTag = [f numberFromString:sender.titleLabel.text];
    NSLog(@"collectionview tag is %@",CVTag);
    vc.currentPageNum = sender.tag;
    vc.collectionViewTag = [CVTag integerValue];
    vc.eventType = @"upcoming";
    vc.comingFrom = @"upcoming";
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - HWViewPagerDelegate
-(void)pagerDidSelectedPage:(NSInteger)selectedPage{
    //    NSLog(@"FistViewController, SelectedPage : %d",(int)selectedPage);
}
-(void)getDashBoard{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });

        NSString *serverUrl = [NSString stringWithFormat:@"%@get_up_next?time_zone=%@",ServerUrl,commonUtils.getTimeZone];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                 NSLog(@"user data is %@",json);
                
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        NSDictionary *successDic = [json[@"successData"]mutableCopy];
                        commonUtils.upNextEventsArray = [successDic[@"upnext"]mutableCopy];
                        upnextArrayCopy = [NSMutableArray new];
                        for (int i = 0; i<commonUtils.upNextEventsArray.count; i++) {
                            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:commonUtils.upNextEventsArray[i]];
                            [upnextArrayCopy addObject:dic];
                        }
                     
                        [table reloadData];
                        if (commonUtils.upNextEventsArray.count==0) {
                            UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, table.bounds.size.width, table.bounds.size.height)];
                            noDataLabel.text             = @"No Upcoming ICE Available";
                            noDataLabel.textColor        = [UIColor blackColor];
                            noDataLabel.textAlignment    = NSTextAlignmentCenter;
                            table.backgroundView = noDataLabel;
                            table.separatorStyle = UITableViewCellSeparatorStyleNone;
                            
                        }
                        
                    });
                    
                    
                    
                    
                    //  NSLog(@"live array is %@",commonUtils.liveEventsArray);
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, table.bounds.size.width, table.bounds.size.height)];
                    noDataLabel.text             = @"No Upcoming ICE Available";
                    noDataLabel.textColor        = [UIColor blackColor];
                    noDataLabel.textAlignment    = NSTextAlignmentCenter;
                    table.backgroundView = noDataLabel;
                    table.separatorStyle = UITableViewCellSeparatorStyleNone;
                }
                if(Jerror!=nil)
                {
                    NSLog(@"json error:%@",Jerror);
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
   
}



- (IBAction)onUserProfile:(UIButton*) sender{
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[sender.tag][@"user_id"]];
    vc.activityIndex = sender.tag;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void) reloadTable:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"upcoming"]){
        NSLog (@"Successfully received the test notification!");
        
        [self getDashBoard];
        
    }
}
-(IBAction)AddNewIce:(UIButton *)sender {
    ICEVC *newIce = [self.storyboard instantiateViewControllerWithIdentifier:@"ICEVC"];
    newIce.isshowMenu = true;
    [self.navigationController pushViewController:newIce animated:true];
}
#pragma mark - PlayVideo
- (void)onPlayVideo:(UIButton*) sender{
    NSInteger collectionTag = [sender.titleLabel.text integerValue];
    NSMutableArray *imagesArray = commonUtils.upNextEventsArray[collectionTag][@"get_images"];
    NSURL *videoUrl;
    
    videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,imagesArray[sender.tag][@"image"]]];
    
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}

@end
