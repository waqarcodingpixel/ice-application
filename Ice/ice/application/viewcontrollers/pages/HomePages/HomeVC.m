//
//  HomeVC.m
//  ICE
//
//  Created by LandToSky on 11/12/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "HomeVC.h"

#import "LiveEventsVC.h"
#import "UpcomingEventsVC.h"
#import "RecentlyAddedVC.h"
#import "LocalEventsVC.h"

#import "NotificationShowVC.h"
#import "LeftMenuVC.h"
#import "MyProfileVC.h"
#import <SVProgressHUD.h>
#import "DetailVC.h"
#import "UserProfileVC.h"
@interface HomeVC ()<UIScrollViewDelegate, DismissNotificationVCDelegate >
{
    
    LiveEventsVC *liveEventVC;
    UpcomingEventsVC *upComingEventsVC;
    RecentlyAddedVC *recentlyAddedVC;
    LocalEventsVC *localEventsVC;
    
    UILabel *notificationLbl;
    UILabel *titleLbl;
    
    // TabBar
    IBOutletCollection(UIButton) NSMutableArray *tabBarBtns;
    IBOutlet UILabel *underLbl;
    
    // ScrollView

    IBOutletCollection(UIView) NSMutableArray *pageView;
   
    NSInteger previousScrollViewIndex;
}

@end

@implementation HomeVC
@synthesize tabScrollView,menuVC;

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self initUI];
    [self initData];
    [commonUtils setUserDefault:@"loggedIn" withFormat:@"login"];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [tabScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * previousScrollViewIndex, 0)];
   
    
}

- (void)initUI
{
    tabScrollView.delegate = self;
    
    /* Adjust Frame Layout */
    CGRect frame = CGRectZero;
    frame = tabScrollView.frame;
     for (int i = 0; i < 4; i++)
    {
        frame.origin.x = SCREEN_WIDTH * i;
        [(UIView*)pageView[i] setFrame:frame];
    }
    
    [tabScrollView setContentSize:CGSizeMake(SCREEN_WIDTH * 4, tabScrollView.frame.size.height)];
    [tabScrollView setPagingEnabled:YES];
    
    /* Top Label */
    titleLbl = (UILabel *) [self.view viewWithTag:1002];
    
    notificationLbl = (UILabel*)[self.view viewWithTag:1001];
    [commonUtils setRoundedRectView:notificationLbl withCornerRadius:notificationLbl.frame.size.height/2];
    [self updateSideMenu];

}

- (void)initData
{
    previousScrollViewIndex = 0;
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
//    [self getNotfications:0];

}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"segue-LiveEventsVC"]) {
        liveEventVC = segue.destinationViewController;
    } else if ([segueName isEqualToString:@"segue-UpcomingEventsVC"]){
        upComingEventsVC = segue.destinationViewController;
    }else if ([segueName isEqualToString:@"segue-RecentlyAddedVC"]){
        recentlyAddedVC = segue.destinationViewController;
    }else if ([segueName isEqualToString:@"segue-LocalEventsVC"]){
        localEventsVC = segue.destinationViewController;
    }
  
}

#pragma mark - onShow Notifications
- (IBAction)onShowNotifications:(id)sender
{
    NotificationShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationShowVC"];
    
    vc.delegate = self;
    
    vc.providesPresentationContextTransitionStyle = YES;
    vc.definesPresentationContext = YES;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.navigationController presentViewController:vc animated:NO completion:nil];
}

- (void)showEventDetailVC:(UIButton*)sender
{
    NSLog(@"sender tag %ld",(long)sender.tag);
  
        DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
        vc.eventType = @"notification";
        vc.comingFrom = @"notification";
    
    
        vc.selectedTag = sender.tag;
        vc.iceDetails = commonUtils.notificationsArray[sender.tag][@"get_ice"];
        [self.navigationController pushViewController:vc animated:YES];
}

- (void)showUserProfileVC:(UIButton*)sender
{
    
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = [NSString stringWithFormat:@"%@",commonUtils.notificationsArray[sender.tag][@"get_ice"][@"user_id"]];
    vc.activityIndex = sender.tag;

    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - onTabBar Button
- (IBAction)onTabBarButton:(UIButton*)sender
{
    NSInteger index = [tabBarBtns indexOfObject:sender];
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [tabScrollView setContentOffset:CGPointMake(tabScrollView.frame.size.width * index, 0)];
                         
                     }];
}


- (void)onShowTabView:(NSInteger) index
{
    [tabScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * index, 0)];
}

#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView != tabScrollView) return;
    
   menuVC = (LeftMenuVC*) self.sideMenuController.leftViewController;
    if(menuVC.fromActivitySelector)
  	      return;
    
    CGRect frame = underLbl.frame;
    frame.origin.x = scrollView.contentOffset.x / 4;
    underLbl.frame = frame;
    
    /* Get Current Page Number */
    CGFloat width = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    previousScrollViewIndex = page;
    
    
    switch (page) {
        case 0:
            [underLbl setBackgroundColor:appController.appBlueColor];
            [notificationLbl setBackgroundColor:appController.appBlueColor];
            [titleLbl setText:@"Live"];
            break;
        case 1:
            [underLbl setBackgroundColor:appController.appGreenColor];
            [notificationLbl setBackgroundColor:appController.appGreenColor];
            [titleLbl setText:@"Up Next"];
            break;
        case 2:
            [underLbl setBackgroundColor:appController.appRedColor];
            [notificationLbl setBackgroundColor:appController.appRedColor];
            [titleLbl setText:@"Coming Soon"];
            break;
        case 3:
            [underLbl setBackgroundColor:appController.appGrayColor];
            [notificationLbl setBackgroundColor:appController.appGrayColor];
            [titleLbl setText:@"Near You"];
             break;
        default:
            break;
    }
    
}
-(void)updateSideMenu {
    menuVC = (LeftMenuVC*) self.sideMenuController.leftViewController;
    [menuVC view];
    if (![[commonUtils.userData valueForKey:@"photo"] isKindOfClass:[NSNull class]]&&![[commonUtils.userData valueForKey:@"photo"]isEqualToString:@""]) {
        NSString *imageUrlString = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"photo"]];
        
        NSURL *url = [NSURL URLWithString:imageUrlString];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        [menuVC.userImage setImage:[UIImage imageWithData:data]];
        CGSize sizeOfImage = CGSizeMake(50, 50);
        menuVC.userImage.image = [self imageByCroppingImage:menuVC.userImage.image toSize:sizeOfImage];
      
    }
    else {
        menuVC.userImage.image = [UIImage imageNamed:@"user-avatar"];
       
        
    }
    commonUtils.userImage = menuVC.userImage.image;
    menuVC.userName.text = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
    
    menuVC.userFN.text= [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];

}
- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}
-(void)getNotfications:(int)currentPage{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        [commonUtils showHud:self.view];
        NSString *skip = [NSString stringWithFormat:@"%d",currentPage];
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"http://139.162.37.73/iceapp/api/v1/get_all_notifications/%@?time_zone=%@",skip,timezone];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    
                    NSMutableDictionary *successDic = [[NSMutableDictionary alloc]init];
                    successDic = json[@"successData"];
                    //                    icedActiviteis = successDic[@"iced_activities"];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [commonUtils hideHud];
                        
                        NSString *notifications =  [NSString stringWithFormat:@"%@",successDic[@"un_read"]];
                        NSInteger count = [notifications integerValue];
                        if (count < 4) {
                            notificationLbl.text = notifications;
                        }
                        else{
                            notificationLbl.text = @"3+";
                        }
                        // [mainTV reloadData];
                    });
                    
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
    }
    
}


@end
