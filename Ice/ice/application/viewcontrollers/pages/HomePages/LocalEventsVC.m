//
//  LocalEventsVC.m
//  ICE
//
//  Created by LandToSky on 11/14/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

typedef NS_ENUM(NSInteger, Weekday)
{
    WeekdaySunday = 1,
    WeekdayMonday,
    WeekdayTuesday,
    WeekdayWednesday,
    WeekdayThursday,
    WeekdayFriday,
    WeekdaySaturday
};

#import "LocalEventsVC.h"

#import "Place.h"
#import "PlaceMark.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"


#import "MapCalloutViewUser.h"
#import "MapCalloutViewEvent.h"
#import "EventDatePinView.h"
#import "UserProfileVC.h"
#import "DetailVC.h"
#import "ICEVC.h"

@interface LocalEventsVC ()<MKMapViewDelegate,UIGestureRecognizerDelegate>
{
   
    IBOutlet MKMapView *mapSearchView;
    NSMutableArray *eventDatas;
    DXAnnotationSettings *annotationSettings;
    
    
   IBOutlet UISlider *slider;
   IBOutlet UIButton *eventBtn;
   IBOutlet UIButton *userBtn;
    BOOL isShow;
    BOOL isEventBtnSelect;

}

@end

@implementation LocalEventsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    [[UISlider appearance] setThumbImage:[UIImage imageNamed:@"slider-thumb"]
                                forState:UIControlStateNormal];
    
}

- (void)initData
{

    isEventBtnSelect = YES;
    commonUtils.is_event_select = YES;
    eventDatas = [[NSMutableArray alloc] init];
    
    // Test Code
    //eventDatas = appController.tempEventDatas;
    
    
    mapSearchView.delegate = self;
    
    annotationSettings = [DXAnnotationSettings defaultSettings];
    annotationSettings.calloutOffset = 5.0f;
    annotationSettings.calloutCornerRadius = 3.0f; // Half value of MapCalloutView Height
    annotationSettings.calloutBorderColor = [UIColor clearColor];
    annotationSettings.calloutBorderWidth = 1.0f;
    annotationSettings.animationType = DXCalloutAnimationFadeIn;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEndLoadingEventsData:)
                                                 name:@"endLoadingEventsData"
                                               object:nil];

    // Test Code
    _radiusLbl.text = @"1mi";
    [slider setContinuous:NO];
    [self GetUserSearch:@"1"];
 
}
    
-(void)receiveEndLoadingEventsData:(NSNotification*) notification {
    
    if ([notification.name isEqualToString:@"endLoadingEventsData" ])
    {
//        eventDatas = notification.userInfo[@"eventDatas"];
//        [self updateMapView];
       
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
}
- (void)updateMapView {
    
    [commonUtils removeAnnotationsFromMapView:mapSearchView];
    
    // Add Current User Location
    Place *home = [[Place alloc] init];
    home.isMain = 1;
    
    // Testing setting
    //    home.latitude = [[appController.currentUser objectForKey:@"user_location_latitude"] floatValue];
    //    home.longitude = [[appController.currentUser objectForKey:@"user_location_longitude"] floatValue];
    NSLog(@"current user lat is %@",[commonUtils getUserDefault:@"currentLatitude"]);
    NSLog(@"current user lat is %@",[commonUtils getUserDefault:@"currentLongitude"]);
    home.latitude =[[commonUtils getUserDefault:@"currentLatitude"]doubleValue] ;
    home.longitude = [[commonUtils getUserDefault:@"currentLongitude"]doubleValue];
    
    PlaceMark *homePlaceMark = [[PlaceMark alloc] initWithPlace:home];
    [mapSearchView addAnnotation:homePlaceMark];
 
    
    // Add Restaurant
    NSMutableArray *places = [[NSMutableArray alloc] init];
    for(NSMutableDictionary *dic in eventDatas)
    {
        Place *place = [[Place alloc] init];
        place.eventID = [dic[@"id"] intValue];
        place.title = [dic objectForKey:@"title"];
       // place.time = [NSString stringWithFormat:@"%@-%@ PST",dic[@"event_start_time"],dic[@"event_end_time"]];
        
        
        NSDate *today = [NSDate date];
        
        
        NSDate *todayDate = [self toLocalTime:today];
//        NSLog(@"today date is %@",todayDate);
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        
        NSDate * endDateFromApi = [startTimeformatter dateFromString:dic[@"server_end_time"]];
        // your date
        
//        NSLog(@"server time %@",endDateFromApi);
//        NSLog(@"server time string %@",[startTimeformatter stringFromDate:endDateFromApi]);
        
        NSComparisonResult result;
        //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
        
        result = [todayDate compare:endDateFromApi]; // comparing two dates
//        NSLog(@"array date is %@",dic[@"server_end_time"]);
        if(result==NSOrderedDescending)
        {
            NSLog(@"today is less");
            place.weekday = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            place.eventDate = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            place.eventMnth = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            place.time = [NSString stringWithFormat:@"until %@",dic[@"event_end_time"]];
            
        }
        
        else if(result==NSOrderedAscending){
            NSLog(@"server date  is less");
            place.eventMnth = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
            place.weekday = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
            place.eventDate = [[self getRequiredDate:@"dd" dateToConvert:today]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            place.time = [NSString stringWithFormat:@"ends %@.%@th",endMonth,endDate];
            
        }
        
        
        else{
            place.weekday = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            place.eventDate = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            place.eventMnth = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            place.time = [NSString stringWithFormat:@"until %@",dic[@"event_end_time"]];
            NSLog(@"Both dates are same");
        }

        
        
        
        
        
        place.address = dic[@"location"];
        place.latitude =  [dic[@"lat"] floatValue];
        place.longitude = [dic[@"lng"] floatValue];
        
        place.date = dic[@"server_start_time"];
//        place.weekday = dic[@"event_day"];
//        place.eventMnth = dic[@"event_month"];
//        place.eventDate = dic[@"event_day_start"];
        place.user_photo_url = dic[@"get_user"][@"photo"];
        place.isLiveEvent = YES;
        place.isMain = 0;
        
        PlaceMark *placeMark = [[PlaceMark alloc] initWithPlace:place];
        [places addObject:placeMark];
        [mapSearchView addAnnotation:placeMark];
    }
    
    
    // Zoom Mapview to show all annotations at once
//    [mapSearchView showAnnotations:mapSearchView.annotations animated:YES];
    [self mapViewToCenterLocationToZoomAllPins:homePlaceMark withPins:places];
//    [self initMyLocationCallOutShow];
    
}
    
// Show User location when starting
- (void)initMyLocationCallOutShow {
    id annotation;
    for(annotation in mapSearchView.annotations) {
        PlaceMark *placeMark = (PlaceMark *)annotation;
        if(placeMark.isMain == 1) {
            [mapSearchView selectAnnotation:annotation animated:YES];
            break;
        }
    }
    
}


#pragma mark - MapView Delegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(PlaceMark*)annotation {
    
    if ([annotation isKindOfClass:[PlaceMark class]]) {
        
        EventDatePinView *pinView = [[[NSBundle mainBundle] loadNibNamed:@"EventDatePinView" owner:self options:nil] firstObject];
        UIView *calloutView = nil;
        MapCalloutViewEvent *eventCalloutView = [[MapCalloutViewEvent alloc]init];
        MapCalloutViewUser *userCalloutView = [[MapCalloutViewUser alloc]init];
        userCalloutView = [[[NSBundle mainBundle] loadNibNamed:@"MapCalloutViewUser" owner:self options:nil] firstObject];
        eventCalloutView = [[[NSBundle mainBundle] loadNibNamed:@"MapCalloutViewEvent" owner:self options:nil] firstObject];
        DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([DXAnnotationView class])];
        
//        if (!annotationView) {
        
            /* In case User Annotation */
            if (annotation.isMain == 1)            {
             
               
               

                pinView = [[UIImageView alloc]initWithImage:commonUtils.userImage];
                [pinView setContentMode:UIViewContentModeScaleAspectFill];
                pinView.clipsToBounds = YES;
                pinView.maskView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user-mask"]];
                ;
                [commonUtils resizeFrame:pinView withWidth:38 withHeight:57];


                [pinView setContentMode:UIViewContentModeScaleAspectFill];
                pinView.clipsToBounds = YES;
                
                annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                              reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                      pinView:pinView
                                                                  calloutView:nil
                                                                     settings:annotationSettings];
                
                return annotationView;
            }
            
            
            /* In case Other Event Annotation */
            if (isEventBtnSelect) { // In case Event Btn Selected
               
                pinView = [[[NSBundle mainBundle] loadNibNamed:@"EventDatePinView" owner:self options:nil] firstObject];
                pinView.weekdayLbl.text = annotation.weekday;
                pinView.dateLbl.text = annotation.eventDate;
                pinView.monthLbl.text = annotation.eventMnth;
               // pinView.weekdayLbl.text = annotation.weekday;
                [commonUtils resizeFrame:pinView withWidth:38 withHeight:55];

                
                eventCalloutView = [[[NSBundle mainBundle] loadNibNamed:@"MapCalloutViewEvent" owner:self options:nil] firstObject];
                [eventCalloutView.titleLbl setText:annotation.title];
                [eventCalloutView.timeLbl setText:annotation.time];
                [eventCalloutView.dateLbl setText:annotation.eventDate];
                 [eventCalloutView.monthLbl setText:annotation.eventMnth];
                 [eventCalloutView.weekdayLbl setText:annotation.weekday];
                [eventCalloutView.addressLbl setText:annotation.address];
                [eventCalloutView.userIv sd_setImageWithURL:[NSURL URLWithString:annotation.user_photo_url]
                                           placeholderImage:[UIImage imageNamed:@"user_avatar"]
                                                    options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload)];
      
                // Add Button
                NSLog(@"usercollection view %@",userCalloutView);
                eventCalloutView.detailBtn.tag = annotation.eventID;
                eventCalloutView.userProfileBtn.tag = annotation.eventID;
                [eventCalloutView.userProfileBtn addTarget:self action:@selector(onUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
               
              
                [eventCalloutView.detailBtn addTarget:self action:@selector(onDetailVC:) forControlEvents:UIControlEventTouchUpInside];
                
                calloutView = eventCalloutView;
                
            } else { // User Button Selected
                UIImageView *otherUserIV = [[UIImageView alloc]init];
                [otherUserIV sd_setImageWithURL:[NSURL URLWithString:annotation.user_photo_url]
                                           placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                                    options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload)];
                pinView = otherUserIV;
                [pinView setContentMode:UIViewContentModeScaleAspectFill];
                pinView.clipsToBounds = YES;
                pinView.maskView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user-mask"]];
                ;
                [commonUtils resizeFrame:pinView withWidth:38 withHeight:57];

                
                userCalloutView = [[[NSBundle mainBundle] loadNibNamed:@"MapCalloutViewUser" owner:self options:nil] firstObject];
                [userCalloutView.titleLbl setText:annotation.title];
                [userCalloutView.timeLbl setText:annotation.time];
                 [userCalloutView.dateLbl setText:annotation.eventDate];
                 [userCalloutView.monthLbl setText:annotation.eventMnth];
                 [userCalloutView.weekdayLbl setText:annotation.weekday];
                [userCalloutView.addressLbl setText:annotation.address];
                [userCalloutView.userIv sd_setImageWithURL:[NSURL URLWithString:annotation.user_photo_url]
                                           placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                                    options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload)];

                
                // Add Button Action
                userCalloutView.userProfileBtn.tag = annotation.eventID;
                [userCalloutView.userProfileBtn addTarget:self action:@selector(onUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
                eventCalloutView.detailBtn.tag = annotation.eventID;
                NSLog(@"event id is %d",annotation.eventID);
                 NSLog(@"button text is %ld",(long)userCalloutView.detailBtn.tag);
                [userCalloutView.detailBtn addTarget:self action:@selector(onDetailVC:) forControlEvents:UIControlEventTouchUpInside];
                
                calloutView = userCalloutView;
            }
            
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:calloutView
                                                                 settings:annotationSettings];
            
            
//        }
        
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
        if ([view isKindOfClass:[DXAnnotationView class]]) {
           [((DXAnnotationView *)view)hideCalloutView];
            view.layer.zPosition = -1;
           
         
        }

   }


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (isEventBtnSelect) {
        
    
    UITapGestureRecognizer *annotationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(annotationTapRecognized:)];
    annotationTap.numberOfTapsRequired = 1;
    annotationTap.delegate = self;
   [view addGestureRecognizer:annotationTap];
  
    if (isShow) {
        NSArray *selectedAnnotations = mapSearchView.selectedAnnotations;
        for(id annotation in selectedAnnotations) {
            [mapSearchView deselectAnnotation:annotation animated:NO];
        }
        isShow = FALSE;
        if ([view isKindOfClass:[DXAnnotationView class]]) {
            
            [((DXAnnotationView *)view)showCalloutView];
            view.layer.zPosition = 0;
            
            // Move MapView to center this annotation
            PlaceMark *placeMark = view.annotation;
            [self mapViewCenterToAnnotation:placeMark];
            
        }
    }
}
    else{
        if ([view isKindOfClass:[DXAnnotationView class]]) {
            [((DXAnnotationView *)view)showCalloutView];
            view.layer.zPosition = 0;
            
            // Move MapView to center this annotation
            PlaceMark *placeMark = view.annotation;
            [self mapViewCenterToAnnotation:placeMark];
        }

    }
    
}
- (void)annotationTapRecognized:(UITapGestureRecognizer *)gesture
{
    
    NSArray *selectedAnnotations = mapSearchView.selectedAnnotations;
    for(id annotation in selectedAnnotations) {
        [mapSearchView deselectAnnotation:annotation animated:NO];
    }
     isShow = TRUE;
   
}
#pragma mark - Center User Location and Zoom to show all pins
- (void)mapViewToCenterLocationToZoomAllPins:(PlaceMark*)center withPins:(NSArray*) points
{
    // pad our map by 10% around the farthest annotations
    #define MAP_PADDING 1.2
    
    // we'll make sure that our minimum vertical span is about a kilometer
    // there are ~111km to a degree of latitude. regionThatFits will take care of
    // longitude, which is more complicated, anyway.
    #define MINIMUM_VISIBLE_LATITUDE 0.01

    MKCoordinateRegion region;
    double maxLat = 0;
    double maxLon = 0;
    
    CLLocation *currentLocation;
    double deltaLatitude = 0.0, deltaLongitude = 0.0;
    
    for (PlaceMark *point in points)
    {
        currentLocation = (CLLocation*)point;
        deltaLatitude = fabs(center.coordinate.latitude - currentLocation.coordinate.latitude);
        deltaLongitude = fabs(center.coordinate.longitude - currentLocation.coordinate.longitude);
        
        if (deltaLatitude > maxLat )
            maxLat = deltaLatitude;
        
        if (deltaLongitude > maxLon)
            maxLon = deltaLongitude;
    }
    deltaLatitude *= 2.0f;
    deltaLongitude *= 2.0f;
    
    deltaLatitude = deltaLatitude * MAP_PADDING +0.06;
    deltaLatitude = (deltaLatitude < MINIMUM_VISIBLE_LATITUDE) ? MINIMUM_VISIBLE_LATITUDE : deltaLatitude;
    
    deltaLongitude = deltaLongitude * MAP_PADDING + 0.06;
    
    [mapSearchView setCenterCoordinate:center.coordinate animated:YES];
    region.center = center.coordinate;
    region.span.latitudeDelta = deltaLatitude;
    region.span.longitudeDelta = deltaLongitude;
    
    region = [mapSearchView regionThatFits:region];
    [mapSearchView setRegion:region animated:YES];
    
}


#pragma mark - Move MapView Center to Annotation
- (void)mapViewCenterToAnnotation:(PlaceMark*) placeMark
{
    MKMapRect r = [mapSearchView visibleMapRect];
    MKMapPoint pt = MKMapPointForCoordinate([placeMark coordinate]);
    r.origin.x = pt.x - r.size.width * 0.2;
    r.origin.y = pt.y - r.size.height * 0.5;
    [mapSearchView setVisibleMapRect:r animated:YES];
}


#pragma mark - Button Action
- (void)onUserProfileVC:(UIButton*)sender
{
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    NSInteger eventId = sender.tag;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %d", eventId ];
    NSArray *filtered = [eventDatas filteredArrayUsingPredicate:predicate];
    vc.otherUserID = [NSString stringWithFormat:@"%@",filtered[0][@"user_id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onDetailVC:(UIButton*)sender
{
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];

    NSInteger eventId = sender.tag;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %d", eventId ];
    NSArray *filtered = [eventDatas filteredArrayUsingPredicate:predicate];
    NSUInteger index = [eventDatas  indexOfObjectPassingTest:^(id obj, NSUInteger idx, BOOL *stop) {
        return [predicate evaluateWithObject:obj];
    }];
    NSLog(@"index is %lu",(unsigned long)index);
    NSLog(@"filtered array is %@",filtered);
    NSDictionary *item = [filtered objectAtIndex:0];
    vc.iceDetails = [item mutableCopy];
    vc.selectedTag = index;
    vc.eventType = @"tagged";
    vc.comingFrom = @"tagged";
    //NSLog(@"filtered dic is %@",item);
    //NSLog(@"tag is %ld",(long)sender.tag);
        [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Event Action Method
- (IBAction)onEventSelect:(id)sender {
    
    isEventBtnSelect = YES;
    commonUtils.is_event_select = YES;
    [UIView animateWithDuration:1 animations:^{
        [eventBtn setImage:[UIImage imageNamed:@"event-select-left"] forState:UIControlStateNormal];
        [userBtn setImage:[UIImage imageNamed:@"event-select-right"] forState:UIControlStateNormal];
    }];
    [self updateMapView];
    
}
    
- (IBAction)onUserSelect:(id)sender {
    
    isEventBtnSelect = NO;
    commonUtils.is_event_select = NO;
    [UIView animateWithDuration:1 animations:^{
        [eventBtn setImage:[UIImage imageNamed:@"user-select-left"] forState:UIControlStateNormal];
        [userBtn setImage:[UIImage imageNamed:@"user-select-right"] forState:UIControlStateNormal];
    }];
    [self updateMapView];
}
-(void)GetUserSearch:(NSString*)radius{

        //Background Thread
        
        
        NSString *serverUrl = [NSString stringWithFormat:@"%@search?time_zone=%@&radius=%@&search_key=",ServerUrl,commonUtils.getTimeZone,radius];
        NSLog(@"serverUrl %@",serverUrl);
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                // NSLog(@"user data is %@",json);
                
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        NSDictionary *successDic = [json[@"successData"]mutableCopy];
                        NSLog(@"successDic %@",successDic);
                        
                        
                        
                        
                        eventDatas = successDic[@"ice"];
                        commonUtils.taggedData = [eventDatas mutableCopy];
                        [self updateMapView];
                        
                    });
                    
                    
                    
                    
                    //  NSLog(@"live array is %@",commonUtils.comingSoonArray);
                    
                }
                if(Jerror!=nil)
                {
                    NSLog(@"json error:%@",Jerror);
                }
            }
        }
   
}


- (IBAction)sliderValueChanged:(id)sender {
    UISlider *slider2 = (UISlider *)sender;
    NSLog(@"SliderValue ... %d",(int)[slider2 value]);
    _radiusLbl.text = [NSString stringWithFormat:@"%dmi",(int)slider2.value];
    [self GetUserSearch:[NSString stringWithFormat:@"%d",(int)slider2.value]];
    
    
}

- (IBAction)addNewIceBtn:(id)sender {
    ICEVC *newIce = [self.storyboard instantiateViewControllerWithIdentifier:@"ICEVC"];
    newIce.isshowMenu = true;
    [self.navigationController pushViewController:newIce animated:true];
}
- (IBAction)sliderStop:(id)sender {
    NSLog(@"value stop");
    
}
-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    //    [converter setDateFormat:format];
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    //    NSDate *convertedDate = [converter dateFromString:PassedDate];
    //    NSString *convertedDateString = [converter stringFromDate:convertedDate];
    //    return convertedDateString;
    
}

@end
