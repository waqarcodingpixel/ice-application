//
//  locationPickerVc2.m
//  ICE
//
//  Created by MAC MINI on 10/08/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "locationPickerVc2.h"
#import "DetailEditVC.h"
@interface locationPickerVc2 ()

@end

@implementation locationPickerVc2
{
    CLLocationCoordinate2D touchMapCoordinate;
    BOOL locationPicked;
     CLLocationManager *locationManager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initGmap];
    // Do any additional setup after loading the view.
    
    
    _gmapView.delegate = self;
    _locationPickerMap.delegate = self;
    
    _locationPickerMap.showsUserLocation = YES;
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        // Add observer
        //        [self.locationPickerMap.userLocation addObserver:self
        //                                    forKeyPath:@"location"
        //                                       options:(NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld)
        //                                       context:NULL];
    }
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.5; //user needs to press for 2 seconds
    [self.locationPickerMap addGestureRecognizer:lpgr];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    CLLocation *previousLoc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude];
    NSLog(@"previous loc is %f",previousLoc.coordinate.latitude);
    if (previousLoc.coordinate.latitude != 0.000000) {
        [self dropPreviousPin];
        locationPicked = YES;
    }
    else{
        locationPicked = NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Map Methods
- (void)removeAllPinsButUserLocation
{
    id userLocation = _locationPickerMap.userLocation;
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[_locationPickerMap annotations]];
    if ( userLocation != nil ) {
        [pins removeObject:userLocation]; // avoid removing user location off the map
    }
    
    [_locationPickerMap removeAnnotations:pins];
    
    pins = nil;
}
-(void)setInitialMapZoom
{
    NSLog(@"Did update user location");
    MKCoordinateRegion mapRegion;
    mapRegion.center = _locationPickerMap.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [_locationPickerMap setRegion:mapRegion animated: YES];
}

- (IBAction)locationPickedBtn:(id)sender {
    if (!locationPicked) {
        CLLocation *pickedloc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude];
        
        if (CLLocationCoordinate2DIsValid(touchMapCoordinate)&&!(touchMapCoordinate.latitude==0)&&!(touchMapCoordinate.longitude==0)) {
            locationPicked = YES;
            
            [self getAddressFromGoogle];
        }
        
        
        else if(pickedloc.coordinate.latitude != 0.000000 ) {
         //   [self locationPickedGoBack];
        }
        else {
            [commonUtils showAlert:@"Error!" withMessage:@"Please select ICE location"];
        }
    }
    else{
        
        [self getAddressFromGoogle];
        
    }
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Gesture Recognizer

// Gesture recognizer method
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    [self removeAllPinsButUserLocation];
    CGPoint touchPoint = [gestureRecognizer locationInView:self.locationPickerMap];
    commonUtils.userPickedCord = touchPoint;
    touchMapCoordinate =
    [self.locationPickerMap convertPoint:touchPoint toCoordinateFromView:self.locationPickerMap];
    
    MKPointAnnotation *annot = [[MKPointAnnotation alloc] init];
    annot.coordinate = touchMapCoordinate;
    [self.locationPickerMap addAnnotation:annot];
    
}

#pragma mapview methods
-(void)getAddressFromCoordinates{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      
                      //  NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      
                      
                      NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
                      DetailEditVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];
                      dispatch_async(dispatch_get_main_queue(), ^(void){
                          //Run UI Updates
                          vc.locationLbl.text = locatedAt;
                          [commonUtils showAlert:@"Success" withMessage:@"ICE location is set"];
                          [self.navigationController popViewControllerAnimated:YES];
                      });
                      
                  }
                  else {
                      NSLog(@"Could not locate");
                  }
              }
     ];
}
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapView.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [mapView setRegion:mapRegion animated: YES];
}

-(void)dropPreviousPin{
        CLLocationCoordinate2D coordinate =CLLocationCoordinate2DMake(commonUtils.pickedLocationCord.latitude,commonUtils.pickedLocationCord.longitude);
        GMSMarker *pickedLoc = [GMSMarker markerWithPosition:coordinate];
        pickedLoc.title = @"Picked Location";
        // london.icon = [UIImage imageNamed:@"house"];
        pickedLoc.map = _gmapView;
        
    }

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    CLLocation  *currentLocation = [locations lastObject];
    
    NSLog(@"gmap loc is %f",currentLocation.coordinate.latitude);
    double currentlat = currentLocation.coordinate.latitude;
    double currentlng = currentLocation.coordinate.longitude;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentlat
                                                            longitude:currentlng
                                                                 zoom:15];
    
    [_gmapView animateToCameraPosition:camera];
    [locationManager stopUpdatingLocation];
    
}
-(void)getAddressFromGoogle{
    
    //NSLog(@"App id is %@",AppUtility.deviceId);
    NSString * authenticate_url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true/false",commonUtils.pickedLocationCord.latitude,commonUtils.pickedLocationCord.longitude];
    NSLog(@"url string : %@",authenticate_url);
    // NSLog(@"client id here is %@",_clienti);
    NSData * webData = [NSData dataWithContentsOfURL:[NSURL URLWithString:authenticate_url]];
    NSLog(@"come here check2");
    if(webData == nil)
    {
        NSLog(@"No data Found");
        
        
        return;
    }
    else
    {
        
        NSString * responseString = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
        NSLog(@"response string %@",responseString);
        NSError * error;
        NSDictionary *json =
        [NSJSONSerialization JSONObjectWithData: [responseString dataUsingEncoding:NSUTF8StringEncoding]
                                        options: NSJSONReadingMutableContainers
                                          error: &error];
        NSLog(@"%@",json);
        NSString *formattedAddress = [NSString stringWithFormat:@"%@",json[@"results"][0][@"formatted_address"]];
        NSLog(@"address is %@",formattedAddress);
        NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
        DetailEditVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            vc.locationLbl.text = formattedAddress;
            [commonUtils showAlert:@"Success" withMessage:@"ICE location is set"];
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    }
}
-(void)initGmap{
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    // Create a GMSCameraPosition that tells the map to display the
    _gmapView.myLocationEnabled = YES;
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 500; // meters
    [locationManager startUpdatingLocation];
    
}
- (void)mapView:(GMSMapView *)mapView
didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [_gmapView clear];
    GMSMarker *pickedLoc = [GMSMarker markerWithPosition:coordinate];
    pickedLoc.title = @"Picked Location";
    // london.icon = [UIImage imageNamed:@"house"];
    pickedLoc.map = _gmapView;
    touchMapCoordinate = coordinate;
    commonUtils.pickedLocationCord = coordinate;
    
}


@end
