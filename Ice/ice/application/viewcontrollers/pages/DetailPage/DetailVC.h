//
//  DetailVC.h
//  ICE
//
//  Created by LandToSky on 11/27/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailVC : BaseViewController

@property (nonatomic, assign) BOOL isNoExistPhoto;
@property (weak, nonatomic) IBOutlet UILabel *icedayLbl;
@property (weak, nonatomic) IBOutlet UILabel *iceDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *iceMnthLbl;
@property (weak, nonatomic) IBOutlet UILabel *iceTImeLbl;
@property (weak, nonatomic) IBOutlet UILabel *iceLocLbl;
@property (weak, nonatomic) IBOutlet UIImageView *iceUserImg;
@property (weak, nonatomic) IBOutlet UILabel *iceUserNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *imagesCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *iceTitleLbl;
@property (nonatomic,strong) NSString *comingFrom,*eventType;
// In case user click his Events from Right Menu page, Detail pages has to show "Delete Button"
@property (nonatomic,assign) BOOL isShowFromRightMenuEvents;
@property (nonatomic) NSInteger selectedTag,sectionTag;

@property (weak, nonatomic) IBOutlet UILabel *memberCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *membersNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *likesCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *likesNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *commentsCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *checkInLbl;
@property (weak, nonatomic) IBOutlet UILabel *memberCountLblOther;
@property (weak, nonatomic) IBOutlet UILabel *likesCountLblOther;
@property (strong,nonatomic) NSMutableDictionary *iceDetails;
@property (weak, nonatomic) IBOutlet UIView *commentInsideView;
- (IBAction)onDetailEdit:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *editIV;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
- (IBAction)backBtn:(id)sender;

@end
