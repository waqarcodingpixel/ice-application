//
//  IcedUsersVC.m
//  ICE
//
//  Created by LandToSky on 1/21/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "IcedUsersVC.h"
#import "FollowTVCell.h"
#import "UserProfileVC.h"

@interface IcedUsersVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIView *opacityView;
    IBOutlet UITableView *mainTV;
    NSMutableArray *userDatas;
    NSString *userParam;
}
@end

@implementation IcedUsersVC

@synthesize getmembers,iceID,isMembers;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    [opacityView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];

    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) initData
{
    userDatas = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 20; i++) {
        [userDatas addObject:@(NO)];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    if (isMembers) {
        userParam = @"member_user";
        [self getIcedUser:@"get_ice_members_detail"];
    }
    else{
        userParam = @"user";
         [self getIcedUser:@"get_ice_liked_users"];
    }
  
}
#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return getmembers.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowTVCell *cell = (FollowTVCell*) [tableView dequeueReusableCellWithIdentifier:@"FollowTVCell"];
    if (cell == nil) {
        cell = [[FollowTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FollowTVCell"];
    }
    cell.userProfileBtn.tag = indexPath.row;
    [cell.userProfileBtn addTarget:self action:@selector(onUserProfilePage:) forControlEvents:UIControlEventTouchUpInside];
    cell.userNameLbl.text = getmembers[indexPath.row][userParam][@"first_name"];
    NSString *imageUrl = [NSString stringWithFormat:@"%@",getmembers[indexPath.row][userParam][@"photo"]];
    
    [cell.userIV sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                    placeholderImage:[UIImage imageNamed:@"user-avatar"]
                             options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    
    NSMutableArray *followerArray = [[NSMutableArray alloc]initWithArray:getmembers[indexPath.row][userParam][@"follow"]];
    NSMutableArray *followingArrayFromApi = [[NSMutableArray alloc]initWithArray:getmembers[indexPath.row][userParam][@"following"]];
    NSString *iceCount = [NSString stringWithFormat:@"%@",getmembers[indexPath.row][@"iced_count"]];
    cell.followersCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)followerArray.count];
    cell.followingCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)followingArrayFromApi.count];
    BOOL isFollowing = [getmembers[indexPath.row][@"is_following_count"]boolValue];
    if (isFollowing) {
        cell.userAddImg.image = [UIImage imageNamed:@"follow-check"];
    }
    else {
        cell.userAddImg.image = [UIImage imageNamed:@"follow-plus"];
    }
    cell.userAddBtn.tag = indexPath.row;
    [cell.userAddBtn addTarget:self action:@selector(followUnfollow:) forControlEvents:UIControlEventTouchUpInside];
    if (![iceCount isEqualToString:@"(null)"]) {
         cell.eventsCountLbl.text = iceCount;
    }
    else{
         cell.eventsCountLbl.text = @"0";
    }
    NSLog(@"mty id is %ld",[[commonUtils.userData valueForKey:@"id"]integerValue]);
    NSLog(@"other id is %ld",[getmembers[indexPath.row][@"member_user"][@"id"]integerValue]);
    if ([[commonUtils.userData valueForKey:@"id"] intValue] == [getmembers[indexPath.row][userParam][@"id"]integerValue]){
     
        cell.userAddBtn.hidden = true;
        cell.userAddImg.hidden = true;
    }else {
 
    }
    
    return cell;
}


- (void)onUserProfilePage:(UIButton *)sender
{
    NSString *userID = [NSString stringWithFormat:@"%@",getmembers[sender.tag][userParam][@"id"]];
    
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = userID;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onUserAdd:(UIButton*)sender
{
    BOOL isAdded = [userDatas[sender.tag] boolValue];
    isAdded = !isAdded;
    userDatas[sender.tag] = @(isAdded);
    
    FollowTVCell *cell = (FollowTVCell*) [mainTV cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    if (isAdded) {
        [cell.userAddImg setImage:[UIImage imageNamed:@"follow-check"]];
    } else {
        [cell.userAddImg setImage:[UIImage imageNamed:@"follow-plus"]];
    }
}

#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

}
-(void)getIcedUser:(NSString*)apiParam{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
      
        NSString *urlString = [NSString stringWithFormat:@"http://139.162.37.73/iceapp/api/v1/%@/%@",apiParam,iceID];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    commonUtils.userUpcomingArray = [[NSMutableArray alloc] init];
                    NSMutableDictionary *successDic = [json[@"successData"]mutableCopy];
                    //NSLog(@"%@",commonUtils.userUpcomingArray[activityIndex][@"get_user"]);
                    getmembers = [NSMutableArray new];
                    if ([apiParam isEqualToString:@"get_ice_members_detail"]) {
                         getmembers = [successDic[@"ice_followers"]mutableCopy];
                    }
                    else{
                        getmembers = [successDic[@"ice_likes"]mutableCopy];
                    }
                    NSMutableArray *getMembersCopy = [NSMutableArray new];
                    getMembersCopy = [getmembers mutableCopy];
                 
                    
                    
                    NSMutableArray *getMemberCopy2 = [NSMutableArray new];
                    
                    for(int index= 0 ; index < getMembersCopy.count ; index++){
                        NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:getMembersCopy[index]] ;
                        
                        [getMemberCopy2 addObject:dataDict];
                    }
                    
                    
                    //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                    
                    
                    //
                    
                    
                    getmembers= [[NSMutableArray alloc] initWithArray:getMemberCopy2];

                    
        
                    
                
                    
                    
                    
                
                    [mainTV reloadData];
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
    }
    
}
-(void)followUnfollow:(UIButton*)sender{
    BOOL isFollowing = [getmembers[sender.tag][@"is_following_count"]boolValue];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    FollowTVCell *cell = [mainTV cellForRowAtIndexPath:indexPath];
    if (!isFollowing) {
        [getmembers[sender.tag] setValue:@"1" forKey:@"is_following_count"];
        cell.userAddImg.image = [UIImage imageNamed:@"follow-check"];
        NSString *otherUserID = [NSString stringWithFormat:@"%@",getmembers[sender.tag][userParam][@"id"]];
        NSLog(@"user id %@",otherUserID);
        [self FollowUnfollowAPI:otherUserID AddOrRemove:@"add_follower"];
        
    }
    else{
        [getmembers[sender.tag] setValue:@"0" forKey:@"is_following_count"];
        cell.userAddImg.image = [UIImage imageNamed:@"follow-plus"];
        NSString *otherUserID = [NSString stringWithFormat:@"%@",getmembers[sender.tag][userParam][@"id"]];
        NSLog(@"user id %@",otherUserID);
        [self FollowUnfollowAPI:otherUserID AddOrRemove:@"un_follower"];
    }
}
-(void)FollowUnfollowAPI:(NSString*)followedID AddOrRemove:(NSString*)addOrRemove{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        //there-is-no-connection warning
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:followedID forKey:@"followed_id"];
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *serverUrl = [NSString stringWithFormat:@"%@%@",ServerUrl,addOrRemove];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:serverUrl];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if([statusis isEqualToString:@"success"]){
                                                
                                                  //[mainTV reloadData];
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                              });
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              NSLog(@"Server not responding");
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                              });
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
}

@end
