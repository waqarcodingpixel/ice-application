//
//  IcedUsersVC.h
//  ICE
//
//  Created by LandToSky on 1/21/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IcedUsersVC : BaseViewController
@property(nonatomic,strong) NSMutableArray *getmembers;
@property (nonatomic,strong) NSString *iceID;
@property (nonatomic) BOOL isMembers;
@end
