//
//  DetaiilEditVC.h
//  ICE
//
//  Created by LandToSky on 11/28/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "GMImagePickerController.h"
@interface DetailEditVC : BaseViewController
@property (nonatomic,strong) NSMutableDictionary *iceDetailsDic;
@property (weak, nonatomic) IBOutlet UITextField *reminderTF;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UITextField *houtMintTF;
@property (strong, nonatomic) UIPickerView *hourPicker;
@property (strong, nonatomic) NSArray *pickerElements;
@end
