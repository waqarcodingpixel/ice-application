//
//  locationPickerVc2.h
//  ICE
//
//  Created by MAC MINI on 10/08/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <GooglePlaces/GooglePlaces.h>
@interface locationPickerVc2 : BaseViewController<MKMapViewDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate,GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *locationPickerMap;
- (IBAction)locationPickedBtn:(id)sender;
- (IBAction)backBtn:(id)sender;
@property (weak, nonatomic) IBOutlet GMSMapView *gmapView;
@end
