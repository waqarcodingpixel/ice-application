//
//  ResetPswdVC3.m
//  ICE
//
//  Created by MAC MINI on 25/07/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "ResetPswdVC3.h"
#import "ResetPswdVC2.h"
@interface ResetPswdVC3 ()

@end

@implementation ResetPswdVC3

- (void)viewDidLoad {
    [super viewDidLoad];
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _phoneNmbrTF.inputAccessoryView = numberToolbar;
    _phoneNmbrTF.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)findBtn:(id)sender {
    if (_phoneNmbrTF.text.length>0) {
        [self findPhone];
    }
    else{
        [commonUtils showAlert:@"Error!" withMessage:@"Please Enter Phone Number"];
    }
}

- (IBAction)clearBtn:(id)sender {
    _phoneNmbrTF.text = @"";
    [_phoneNmbrTF resignFirstResponder];
}
-(void)findPhone {
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable) {
            [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found!"];
        }
        else {
            //there-is-no-connection warning
            
            [commonUtils showHud:self.view];
            
            
            // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
            
            //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
            
            
            [_params setObject:_phoneNmbrTF.text forKey:@"phone"];
            
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
            NSString* FileParamConstant = @"pic";
            NSString *serverUrl = [NSString stringWithFormat:@"%@%@",ServerUrl,@"get_user_by_phone"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:serverUrl];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // add image data
            
            
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  //NSLog(@"Server Response %@",response);
                                                  NSLog(@"dictionary %@",dictionary);
                                                  NSString *message = [dictionary valueForKey:@"errorMessage"];
                                                  
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  if([statusis isEqualToString:@"success"]){
                                                      NSDictionary *successDic = [dictionary valueForKey:@"successData"];
                                                      commonUtils.userData = successDic;
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [commonUtils hideHud];
                                                          ResetPswdVC2 *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ResetPswdVC2"];
                                                          vc.userEmail = [NSString stringWithFormat:@"%@",[successDic valueForKey:@"email"]];
                                                          [self.navigationController pushViewController:vc animated:YES];
                                                          
                                                      });
                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [commonUtils hideHud];
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                      });
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  NSLog(@"no data returned");
                                                  [commonUtils hideHud];
                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                  NSLog(@"Server not responding");
                                                  [commonUtils hideHud];
                                                  //couldn't download
                                                  
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
        }
        
    }
}
-(void)cancelNumberPad{
    [_phoneNmbrTF resignFirstResponder];
    //_phoneTF.text = @"";
}

-(void)doneWithNumberPad{
    
    [_phoneNmbrTF resignFirstResponder];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _phoneNmbrTF) {
        
        
        NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        // if it's the phone number textfield format it.
        
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
        
        
        return YES;
    }
    else {
        return YES;
    }
}
-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}

@end
