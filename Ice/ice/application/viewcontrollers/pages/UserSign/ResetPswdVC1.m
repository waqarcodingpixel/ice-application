//
//  ResetPswdVC1.m
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "ResetPswdVC1.h"
#import "ResetPswdVC2.h"
@interface ResetPswdVC1 ()<UITextFieldDelegate>
{
    UITextField *currentTextField;
    IBOutlet UITextField *userNameTF;

}

@end

@implementation ResetPswdVC1

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}
- (void)initUI
{
    userNameTF.delegate = self;
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen)]];
}

- (void)initData
{
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

#pragma mark - onRemove
- (IBAction)onRemove:(id)sender
{
    [userNameTF setText:@""];
    [userNameTF resignFirstResponder];
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.isLoadingUserBase) return NO;
    return [textField resignFirstResponder];
}

#pragma mark - ScrollView Tap
- (void) onTappedScreen {
    if (self.isLoadingUserBase) return;
    [self.view endEditing:YES];
  }
- (IBAction)findBtn:(id)sender {
    if (userNameTF.text.length>0) {
        if ([commonUtils validateEmail:userNameTF.text]) {
               [self findEmail];
        }
        else{
                 [commonUtils showAlert:@"Erro!" withMessage:@"Email is not valid"];
        }
     
    }
    else {
        [commonUtils showAlert:@"Erro!" withMessage:@"Please Provide Email"];
    }
}
-(void)findEmail{
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable) {
            [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found!"];
        }
        else {
            //there-is-no-connection warning
            
            [commonUtils showHud:self.view];
      
       
            // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
            
            //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
          
      
            [_params setObject:userNameTF.text forKey:@"email"];
            
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
            NSString* FileParamConstant = @"pic";
            NSString *serverUrl = [NSString stringWithFormat:@"%@%@",ServerUrl,@"get_user_email"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:serverUrl];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // add image data
            
            
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  //NSLog(@"Server Response %@",response);
                                                  NSLog(@"dictionary %@",dictionary);
                                                  NSString *message = [dictionary valueForKey:@"errorMessage"];
                                                  
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  if([statusis isEqualToString:@"success"]){
                                                      NSDictionary *successDic = [dictionary valueForKey:@"successData"];
                                                      commonUtils.userData = successDic;
                                                    
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                             [commonUtils hideHud];
                                                          ResetPswdVC2 *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ResetPswdVC2"];
                                                          vc.userEmail = [NSString stringWithFormat:@"%@",[successDic valueForKey:@"email"]];
                                                          [self.navigationController pushViewController:vc animated:YES];
                                                          
                                                      });
                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [commonUtils hideHud];
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                      });
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  NSLog(@"no data returned");
                                                  [commonUtils hideHud];
                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                  NSLog(@"Server not responding");
                                                   [commonUtils hideHud];
                                                  //couldn't download
                                                  
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
        }
        
    }
}
@end
