//
//  WelcomeVC.h
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeVC : UserBaseViewController

@property (weak, nonatomic) IBOutlet UIView *restoreSessionView;


@end
