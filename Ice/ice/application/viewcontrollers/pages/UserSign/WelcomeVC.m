//
//  WelcomeVC.m
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "WelcomeVC.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "DetailVC.h"
@interface WelcomeVC ()
{
    __weak IBOutlet UILabel *appTitleLbl;
    NSMutableDictionary *iceDetailsDic;
}

@end

@implementation WelcomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
//For testing
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:appTitleLbl.text];
    [string addAttribute:NSForegroundColorAttributeName value:RGBA(141, 201, 240, 1.0f) range:NSMakeRange(0,6)];
    [string addAttribute:NSForegroundColorAttributeName value:RGBA(254, 118, 118,1.0f) range:NSMakeRange(7,7)];
    [string addAttribute:NSForegroundColorAttributeName value:RGBA(163, 207, 99, 1.0f) range:NSMakeRange(15,10)];
    appTitleLbl.attributedText = string;
    
  
   
  
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if (commonUtils.sessionToken) {
    
//        dispatch_async(dispatch_get_main_queue(), ^{
            if (commonUtils.is_Notification) {
            
            }
            else{
//                 _restoreSessionView.hidden = NO;
//            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SidePanelVC"] animated:YES];
            }
            
            
//        });
    }

//    if ([commonUtils getUserDefault:@"loggedIn"]) {
//          [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SidePanelVC"] animated:YES];
//    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
  //  _restoreSessionView.hidden = YES;
}
#pragma mark - Facebook Login
- (IBAction)onLoginFacebook:(id)sender {
    
    [commonUtils showHud:self.view];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    
    if([FBSDKAccessToken currentAccessToken]){
        [self fetchUserInfo];
    }else{
        [login
         logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error) {
                [commonUtils hideHud];
                 NSLog(@"Process error");
             } else if (result.isCancelled) {
                    [commonUtils hideHud];
                 NSLog(@"Cancelled");
             } else {
                 NSLog(@"Logged in with token : @%@", result.token);
                 if ([result.grantedPermissions containsObject:@"email"]) {
                       [commonUtils hideHud];
                     
                     [self performSelectorOnMainThread:@selector(fetchUserInfo) withObject:nil waitUntilDone:YES];
                 }
             }
         }];
    }
}

- (void)fetchUserInfo {
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, first_name, last_name, email, friends"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 //                 NSLog(@"facebook fetched info : %@", result);
                 
                 
                 NSDictionary *temp = (NSDictionary *)result;
                 NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
                 [userInfo setObject:[temp objectForKey:@"id"] forKey:@"user_facebook_id"];
                 //                 [userInfo setObject:@"974491135946165" forKey:@"user_facebook_id"];
                 
                 [userInfo setObject:[temp objectForKey:@"email"] forKey:@"user_email"];
                 
                 
                 
                 if([commonUtils checkKeyInDic:@"first_name" inDic:[temp mutableCopy]]) {
                     [userInfo setObject:[temp objectForKey:@"first_name"] forKey:@"user_first_name"];
                 }
                 if([commonUtils checkKeyInDic:@"last_name" inDic:[temp mutableCopy]]) {
                     [userInfo setObject:[temp objectForKey:@"last_name"] forKey:@"user_last_name"];
                 }
                 
                 
                 
                 NSString *fbProfilePhoto = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [temp objectForKey:@"id"]];
                 [userInfo setObject:fbProfilePhoto forKey:@"user_photo_url"];
                 
                 
                 
                 [self requestUserSignUp:userInfo];
                 
                 
             }else {
                 NSLog(@"Facebook Fetch Error %@",error);
             }
         }];
        
    }
    
}

- (void) requestUserSignUp:(NSDictionary *)param{
    
    NSLog(@"FB User Info ==>\n%@", param);
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [self fbRegisterLogin:param];


}
-(void)fbRegisterLogin:(NSDictionary *)userInfo{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found!"];
    }
    else {
        //there-is-no-connection warning
    
        [commonUtils showHud:self.view];
    NSString *currentLatitude = [commonUtils getUserDefault:@"currentLatitude"];
    NSString *currentLongtitude = [commonUtils getUserDefault:@"currentLongitude"];
    NSString *deviceId = @"abcdefghijklmnopqrstuvwxyzabcdefghi";
    NSString *address =  [commonUtils getUserDefault:@"location"];
    
    NSString *timeZoneOffset = [commonUtils getTimeZone];
    NSLog(@"device id is %@ address is %@ timezone seconds are %@",deviceId,address,timeZoneOffset);
    
    // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
    
    //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    NSString *userFullName = [NSString stringWithFormat:@"%@ %@",[userInfo valueForKey:@"user_first_name"],[userInfo valueForKey:@"user_last_name"]];
    NSString *userFullNamewithoutSpace =  [NSString stringWithFormat:@"%@%@",[userInfo valueForKey:@"user_first_name"],[userInfo valueForKey:@"user_last_name"]];
    [_params setObject:[userInfo valueForKey:@"user_email"] forKey:@"email"];
    [_params setObject:[NSString stringWithFormat:@"%@",userFullNamewithoutSpace] forKey:@"username"];
    [_params setObject:[userInfo valueForKey:@"user_facebook_id"] forKey:@"fb_id"];
    [_params setObject:userFullName forKey:@"full_name"];
    [_params setObject:address forKey:@"location"];
    [_params setObject:currentLatitude forKey:@"lat"];
    [_params setObject:currentLongtitude forKey:@"lng"];
    [_params setObject:[userInfo valueForKey:@"user_photo_url"] forKey:@"pic"];
    [_params setObject:[NSString stringWithFormat:@"ios"] forKey:@"device_type"];
        if (TARGET_IPHONE_SIMULATOR) {
            [_params setObject:[NSString stringWithFormat:@"%@", @"d9381cfc906fcb487396d17dceb376b83512d7c0f365422e248e0bd2c9216906"] forKey:@"device_id"];
        }
        else{
            [_params setObject:[NSString stringWithFormat:@"%@", [commonUtils getUserDefault:@"user_apns_id"]] forKey:@"device_id"];
        }
        

    [_params setObject:timeZoneOffset forKey:@"timezone"];
  
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"pic";
    NSString *serverUrl = [NSString stringWithFormat:@"%@%@",ServerUrl,@"fblogin"];
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:serverUrl];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
   
 
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    NSError *err = nil;
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if ([data length] > 0 && err == nil){
                                          NSError* error;
                                          NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                     options:kNilOptions
                                                                                                       error:&error];
                                          //NSLog(@"Server Response %@",response);
                                          NSLog(@"dictionary %@",dictionary);
                                          NSString *message = [dictionary valueForKey:@"errorMessage"];
                                          
                                          NSString *statusis = [dictionary valueForKey:@"status"];
                                          if([statusis isEqualToString:@"success"]){
                                              NSDictionary *successDic = [dictionary valueForKey:@"successData"];
                                              [commonUtils saveUserdata:successDic];
                                              [NSUserDefaults.standardUserDefaults setValue:[NSString stringWithFormat:@"%@",[successDic valueForKey:@"session_token"]] forKey:@"session_token"];
                                              commonUtils.userData = [commonUtils getUserData];
                                              commonUtils.cominfFromFB = YES;
                                              commonUtils.sessionToken = [NSUserDefaults.standardUserDefaults objectForKey:@"session_token"];
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                                    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SidePanelVC"] animated:YES];
                                                     [commonUtils hideHud];
                                                  
                                              });
                                          }
                                          if(![statusis isEqualToString:@"success"]){
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                [commonUtils hideHud];
                                                  [commonUtils showAlert:@"Error!" withMessage:message];
                                              });
                                              
                                          }
                                          
                                      }
                                      else if ([data length] == 0 && err == nil){
                                          NSLog(@"no data returned");
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                 [commonUtils hideHud];
                                          });
                                          //no data, but tried
                                      }
                                      else if (err != nil)
                                      {
                                          NSLog(@"Server not responding");
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                [commonUtils hideHud];
                                          });
                                          //couldn't download
                                          
                                      }
                                      
                                      
                                      
                                  }];
    [task resume];
    }
    
}
-(void)getIceDetail{
   
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
       // commonUtils.noti_ice_id = @"418";//Demo ice id for testing
        NSString *serverUrl = [NSString stringWithFormat:@"%@get_ice/%@",ServerUrl,commonUtils.noti_ice_id];
        NSLog(@"server url %@",serverUrl);
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                
                
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        
                        NSDictionary *successDic = [json[@"successData"]mutableCopy];
                        iceDetailsDic = [NSMutableDictionary new];
                        iceDetailsDic = [successDic mutableCopy];
                        DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
                        vc.iceDetails = iceDetailsDic;
                        vc.comingFrom = @"alert";
                        vc.eventType = @"alert";
                        commonUtils.alertDic = [iceDetailsDic mutableCopy];
                        [self.navigationController pushViewController:vc animated:YES];
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    
                        
                    });
                    
                    
                    
                    
                    NSLog(@"live array is %@",commonUtils.liveEventsArray);
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                    //Run UI Updates
             
                    
                }
                if(Jerror!=nil)
                {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                    NSLog(@"json error:%@",Jerror);
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
 
    
    
}

@end
