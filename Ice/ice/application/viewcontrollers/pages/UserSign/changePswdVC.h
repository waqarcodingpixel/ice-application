//
//  changePswdVC.h
//  ICE
//
//  Created by MAC MINI on 27/07/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface changePswdVC : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *oldPswdTF;
@property (weak, nonatomic) IBOutlet UITextField *PswdTFnew;
@property (weak, nonatomic) IBOutlet UIButton *changePswdBtn;
- (IBAction)changePswdBtn:(id)sender;
- (IBAction)backBtn:(id)sender;

@end
