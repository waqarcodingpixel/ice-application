//
//  ResetPswdVC3.h
//  ICE
//
//  Created by MAC MINI on 25/07/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPswdVC3 : UserBaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *phoneNmbrTF;
- (IBAction)findBtn:(id)sender;
- (IBAction)clearBtn:(id)sender;

@end
