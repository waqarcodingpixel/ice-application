//
//  ProfileCalendarVC.h
//  ICE
//
//  Created by LandToSky on 12/6/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCalendarVC : BaseViewController
@property (nonatomic,strong) NSString *otherUserId;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (nonatomic,strong) NSString *userName;
@end
