//
//  ProfileHeaderView.h
//  ICE
//
//  Created by LandToSky on 12/4/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GSKStretchyHeaderView.h>

@interface ProfileHeaderView : GSKStretchyHeaderView

@property (weak, nonatomic) IBOutlet UIView *containerView;

// User ImageView
@property (weak, nonatomic) IBOutlet UIImageView *userProfileIv;

// User Name/Descrption View
@property (weak, nonatomic) IBOutlet UIView *userNameContainerView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *userDescLbl;

// Bottom - Following/Followers/Events View
@property (weak, nonatomic) IBOutlet UIView *bottomView;

// Following

@property (weak, nonatomic) IBOutlet UIButton *followingBtn;
@property (weak, nonatomic) IBOutlet UILabel *followingLbl;

// Followers
@property (weak, nonatomic) IBOutlet UIButton *followersBtn;
@property (weak, nonatomic) IBOutlet UILabel *followersLbl;

// Events
@property (nonatomic,strong) IBOutlet UILabel *activityLbl;
@property (weak, nonatomic) IBOutlet UIButton *eventsBtn;
@property (weak, nonatomic) IBOutlet UILabel *eventLbl;

@end
