//
//  pEyeCandyContentCell.h
//  ICE
//
//  Created by LandToSky on 12/3/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pEyeCandyContentCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UICollectionView *eyeCandyCv;

@end
