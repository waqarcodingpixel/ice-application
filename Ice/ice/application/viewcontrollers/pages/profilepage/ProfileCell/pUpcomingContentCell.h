//
//  pUpcomingContentCell.h
//  ICE
//
//  Created by LandToSky on 12/3/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pUpcomingContentCell : UITableViewCell

// Date Container View
@property (weak, nonatomic) IBOutlet UILabel *weekdayLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *monthLbl;


// User ImageView

@property (weak, nonatomic) IBOutlet UIButton *showUserProfileBtn;
@property (weak, nonatomic) IBOutlet UIImageView *userIv;


// Upcoming Event View
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *liveLbl;

@property (weak, nonatomic) IBOutlet UIButton *showDetailBtn;



@end
