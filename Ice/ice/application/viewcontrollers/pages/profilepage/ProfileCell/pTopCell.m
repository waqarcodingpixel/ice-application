//
//  pTopCell.m
//  ICE
//
//  Created by LandToSky on 12/3/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "pTopCell.h"

@implementation pTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
