//
//  pTopCell.h
//  ICE
//
//  Created by LandToSky on 12/3/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pTopCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;

// User ImageView
@property (weak, nonatomic) IBOutlet UIImageView *userProfileIv;

// User Name/Descrption View
@property (weak, nonatomic) IBOutlet UIView *userNameContainerView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *userDescLbl;

// Bottom - Following/Followers/Events View
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *followingLbl;
@property (weak, nonatomic) IBOutlet UILabel *followersLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventLbl;



@end
