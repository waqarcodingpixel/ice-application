//
//  UserProfileVC.h
//  ICE
//
//  Created by LandToSky on 12/2/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
@interface UserProfileVC : BaseViewController
@property (nonatomic) NSInteger activityIndex;
@property (nonatomic) NSString *otherUserID;
@property (weak, nonatomic) IBOutlet UILabel *userFirstNameLbl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
