//
//  UserProfileVC.m
//  ICE
//
//  Created by LandToSky on 12/2/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "UserProfileVC.h"

#import "ProfileHeaderView.h"
#import "UIView+GSKLayoutHelper.h"

#import "pUpcomingTitleCell.h"
#import "pUpcomingContentCell.h"
#import "pEyeCandyTitleCell.h"
#import "pEyeCandyContentCell.h"
#import "EventImageCVCell.h"

#import "DetailVC.h"
#import "EventsVC.h"
#import "FollowRootVC.h"
#import "allImagesVC.h"
#import "PhotoVideoShowVC.h"
#import "ProfileCalendarVC.h"
#import <MBProgressHUD.h>
@interface UserProfileVC ()<UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate,UICollectionViewDataSource,GSKStretchyHeaderViewStretchDelegate>
{
    IBOutlet UITableView *mainTV;
    ProfileHeaderView *headerView;
    
    NSInteger upComingTVCellNum;
    NSInteger eyeCandyCVCellNum;
    NSMutableArray *icePics;
    //Top View
    __weak IBOutlet UIImageView *addUserImg;
    __weak IBOutlet UIButton *addUserBtn;
    
    
}

@end

@implementation UserProfileVC
@synthesize activityIndex,otherUserID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    headerView = [[[NSBundle mainBundle] loadNibNamed:@"ProfileHeaderView" owner:self options:nil] firstObject];
    headerView.stretchDelegate = self;
    headerView.minimumContentHeight = 85;
    headerView.maximumContentHeight = 230;
    [mainTV addSubview:headerView];
    [mainTV setBackgroundColor:[UIColor colorWithHex:@"#f7f7f7" alpha:1.0f]];
    [MBProgressHUD showHUDAddedTo:headerView animated:YES];
    // Header View Action
    [headerView.eventsBtn addTarget:self action:@selector(onShowEventsVC:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.followingBtn addTarget:self action:@selector(onShowFollowingVC:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.followersBtn addTarget:self action:@selector(onShowFollowersVC:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)initData
{

    upComingTVCellNum = 3;
    eyeCandyCVCellNum = 7;
    
}

- (void)viewDidLayoutSubviews
{
    if (upComingTVCellNum == 0 && eyeCandyCVCellNum == 0){
        CGFloat bottomHeight = mainTV.frame.size.height - 90 - 130 - 145 +5;
        UIView *bottomView = [self.view viewWithTag:1001];
        [commonUtils resizeFrame:bottomView withWidth:bottomView.frame.size.width withHeight:bottomHeight];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if ([[commonUtils.userData valueForKey:@"id"] intValue] == [otherUserID intValue]){
        addUserImg.hidden = true;
        addUserBtn.hidden = true;
        
    }else {
        
    }

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    
    
    [self getUserDetails:otherUserID];
}

#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return commonUtils.userUpcomingArray.count+3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Height index is %ld",(long)indexPath.row);

    
    CGFloat height = 250.0f;
    
    if (indexPath.row == 0) {
        height = (upComingTVCellNum == 0)? 130 : 60;
        
    } else if ( indexPath.row-1 < commonUtils.userUpcomingArray.count ) {
        height= 78;
    }
    else if ( indexPath.row == commonUtils.userUpcomingArray.count+1 ) {
        height= 75;
    }
    
    

    
    return height ;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    UITableViewCell *cell;
    NSLog(@"array here is %@",commonUtils.userUpcomingArray);
    pUpcomingTitleCell* upComingTitleCell;
    pUpcomingContentCell *upComingContentCell;
    pEyeCandyTitleCell *eyeCandyTitleCell;
    pEyeCandyContentCell *eyeCandyContentCell;
 
    if (indexPath.row == 0) {
        if (upComingTVCellNum == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"NoUpcomingEventTVCell"];
            
        } else {
            upComingTitleCell = [tableView dequeueReusableCellWithIdentifier:@"pUpcomingTitleCell"];
            if (upComingTitleCell == nil) {
                upComingTitleCell = [[pUpcomingTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pUpcomingTitleCell"];
                
            }
            cell = upComingTitleCell;
        }
        
    } else if ( indexPath.row-1 < commonUtils.userUpcomingArray.count ) {
        upComingContentCell = [tableView dequeueReusableCellWithIdentifier:@"pUpcomingContentCell"];
        if (upComingContentCell == nil) {
            upComingContentCell = [[pUpcomingContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pUpcomingContentCell"];
        }
        int cellRow = indexPath.row - 1;
        NSLog(@"cell row is %d",cellRow);
        upComingContentCell.showDetailBtn.tag = cellRow;
        [upComingContentCell.showDetailBtn addTarget:self action:@selector(onShowEventDetailVC:) forControlEvents:UIControlEventTouchUpInside];
        [upComingContentCell.showUserProfileBtn addTarget:self action:@selector(onShowUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
        upComingContentCell.titleLbl.text = commonUtils.userUpcomingArray[cellRow][@"title"];
        upComingContentCell.addressLbl.text = commonUtils.userUpcomingArray[cellRow][@"location"];
        upComingContentCell.userNameLbl.text = [NSString stringWithFormat:@"via %@",commonUtils.userUpcomingArray[cellRow][@"get_user"][@"first_name"]];
        NSString *imageUrl = [NSString stringWithFormat:@"%@",commonUtils.userUpcomingArray[cellRow][@"get_user"][@"photo"]];
        [upComingContentCell.userIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                 options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];

        NSDate *today = [NSDate date];
        
        
        NSDate *todayDate = [self toLocalTime:today];
//        NSLog(@"today date is %@",todayDate);
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        
        NSDate * endDateFromApi = [startTimeformatter dateFromString:commonUtils.userUpcomingArray[cellRow][@"server_end_time"]];
        // your date
        
//        NSLog(@"server time %@",endDateFromApi);
//        NSLog(@"server time string %@",[startTimeformatter stringFromDate:endDateFromApi]);
        
        NSComparisonResult result;
        //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
        
        result = [todayDate compare:endDateFromApi]; // comparing two dates
//        NSLog(@"array date is %@",commonUtils.userUpcomingArray[cellRow][@"server_end_time"]);
        if(result==NSOrderedDescending)
        {
            NSLog(@"today is less");
            upComingContentCell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"until %@",commonUtils.userUpcomingArray[cellRow][@"event_end_time"]];
            
        }
        
        else if(result==NSOrderedAscending){
            NSLog(@"server date  is less");
            upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
            upComingContentCell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:today]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"ends %@.%@th",endMonth,endDate];
            
        }
        
        
        else{
            upComingContentCell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"until %@",commonUtils.userUpcomingArray[cellRow][@"event_end_time"]];
            NSLog(@"Both dates are same");
        }

        cell = upComingContentCell;
        
    } else if (indexPath.row == 1+commonUtils.userUpcomingArray.count){
        
        
        if (eyeCandyCVCellNum == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"NoPhotoVideoTVCell"];
        } else {
            eyeCandyTitleCell = [tableView dequeueReusableCellWithIdentifier:@"pEyeCandyTitleCell"];
            if (eyeCandyTitleCell == nil) {
                eyeCandyTitleCell = [[pEyeCandyTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pEyeCandyTitleCell"];
               
            }
            
            cell = eyeCandyTitleCell;
        }
        eyeCandyTitleCell.eyeCandyViewAllBtn.tag = indexPath.row;
        [eyeCandyTitleCell.eyeCandyViewAllBtn addTarget:self action:@selector(showAllImages:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        
        eyeCandyContentCell = [tableView dequeueReusableCellWithIdentifier:@"pEyeCandyContentCell"];

        
        if (eyeCandyContentCell == nil) {
            eyeCandyContentCell = [[pEyeCandyContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pEyeCandyContentCell"];
        }
        eyeCandyContentCell.eyeCandyCv.delegate = self;
        eyeCandyContentCell.eyeCandyCv.dataSource = self;
        [eyeCandyContentCell.eyeCandyCv reloadData];
        cell = eyeCandyContentCell;

    }
   
    return cell;
}


#pragma mark - On Other VC
- (void)onShowUserProfileVC:(UIButton*)sender
{
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowFollowingVC:(UIButton *)sender
{
    FollowRootVC *vc = (FollowRootVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"FollowRootVC"];
    
    
//      commonUtils.commutilUserID = commonUtils.userUpcomingArray[activityIndex][@"user_id"];
    
    commonUtils.commutilUserID =  otherUserID ;
    vc.pageIndex = Following;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowFollowersVC:(UIButton *)sender
{
    FollowRootVC *vc = (FollowRootVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"FollowRootVC"];
//       commonUtils.commutilUserID = commonUtils.userUpcomingArray[activityIndex][@"user_id"];
    commonUtils.commutilUserID = otherUserID ;
    
    vc.pageIndex = Followers;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowEventsVC:(UIButton *)sender
{
    EventsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsVC"];
    commonUtils.commutilUserID = otherUserID;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowEventDetailVC:(UIButton*) sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.comingFrom = @"userprofile";
    vc.selectedTag = sender.tag;
    vc.iceDetails = commonUtils.userUpcomingArray[sender.tag];
//    vc.upComingArray = comm;
    [self.navigationController pushViewController:vc animated:YES];    
}


#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return icePics.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
   
    CGSize mElementSize = CGSizeMake(collectionView.frame.size.width/3 - 1, collectionView.frame.size.width/3-1);
    return mElementSize;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
    
        NSString *imageUrl;
        NSString *type = [NSString stringWithFormat:@"%@",icePics[indexPath.row][@"type"]];
        if ([type isEqualToString:@"image"]) {
            
            
            cell.videoPlayBtn.hidden = YES;
            
            
            imageUrl = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,icePics[indexPath.row][@"image"]];
        }
        else{
            cell.videoPlayBtn.hidden = NO;
            cell.videoPlayBtn.tag = indexPath.row;
            [cell.videoPlayBtn addTarget:self action:@selector(playvideomethod:) forControlEvents:UIControlEventTouchUpInside];

        
            
            imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,icePics[indexPath.row][@"poster"]];
            
        }
        [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"image0"]
                                 options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    
    
    
    return cell;
    
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
    vc.currentPageNum = indexPath.row;
    commonUtils.userIcePics = icePics;
    vc.comingFrom = @"ownProfile";
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - Action
- (IBAction)onAddUser:(id)sender
{
    static BOOL isAdded = NO;
    isAdded = !isAdded;
    if (isAdded) {
        [addUserImg setImage:[UIImage imageNamed:@"follow-check"]];
        [self FollowUnfollowAPI:otherUserID AddOrRemove:@"add_follower"];
    } else {
        [addUserImg setImage:[UIImage imageNamed:@"follow-plus"]];
        [self FollowUnfollowAPI:otherUserID AddOrRemove:@"un_follower"];
    }
    
}


-(void)FollowUnfollowAPI:(NSString*)followedID AddOrRemove:(NSString*)addOrRemove{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        //there-is-no-connection warning
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:followedID forKey:@"followed_id"];
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *serverUrl = [NSString stringWithFormat:@"%@%@",ServerUrl,addOrRemove];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:serverUrl];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if([statusis isEqualToString:@"success"]){
                                                  [[NSNotificationCenter defaultCenter]
                                                   postNotificationName:@"followingVC"
                                                   object:self];
                                                  //[mainTV reloadData];
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                              });
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              NSLog(@"Server not responding");
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                              });
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
}

#pragma mark - GSKStretchView Delegate

- (void)stretchyHeaderView:(ProfileHeaderView *)realHeaderView didChangeStretchFactor:(CGFloat)stretchFactor
{

    CGFloat limitedStretchFactor = MIN(1, stretchFactor);
    
    // User Profile ImageView Size Adjust
    CGSize minImageSize = CGSizeMake(55, 55);
    CGSize maxImageSize = CGSizeMake(125, 125);
    
    realHeaderView.userProfileIv.size = CGSizeInterpolate(limitedStretchFactor, minImageSize, maxImageSize);
    
    // Bottom (Following/Followers/Events) View Adjust
    CGSize minBottomViewSize = CGSizeMake(realHeaderView.size.width-55, 55);
    CGSize maxBottomViewSize = CGSizeMake(realHeaderView.size.width, 70);
    CGPoint minBottomViewOrigin = CGPointMake(55, 0);
    CGPoint maxBottomViewOrigin = CGPointMake(0, 125);
    
    realHeaderView.bottomView.size = CGSizeInterpolate(limitedStretchFactor, minBottomViewSize, maxBottomViewSize);
    realHeaderView.bottomView.left = CGFloatInterpolate(limitedStretchFactor, minBottomViewOrigin.x, maxBottomViewOrigin.x);
    realHeaderView.bottomView.top = CGFloatInterpolate(limitedStretchFactor, minBottomViewOrigin.y, maxBottomViewOrigin.y);
    
    // User Name View
    [realHeaderView.userNameContainerView setAlpha:limitedStretchFactor];
    
//    if (stretchFactor < 1) {
//        [realHeaderView.userNameContainerView setHidden:YES];
//    } else {
//        [realHeaderView.userNameContainerView setHidden:NO];
//    }

    
}
-(void)getUserDetails:(NSString*)userID{
    
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable) {
            //my web-dependent code
            [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
        }
        else {
            [commonUtils showActivityIndicatorColored:headerView];
            NSString *timezone = [commonUtils getTimeZone];
            NSString *urlString = [NSString stringWithFormat:@"http://139.162.37.73/iceapp/api/v1/get_profile/%@?time_zone=%@",userID,timezone];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            
            // Create a mutable copy of the immutable request and add more headers
            NSMutableURLRequest *mutableRequest = [request mutableCopy];
            [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            
            // Now set our request variable with an (immutable) copy of the altered request
            request = [mutableRequest copy];
            
            // Log the output to make sure our new headers are there
            NSLog(@"%@", request.allHTTPHeaderFields);
            
            
            NSURLResponse *response;
            
            NSError *error = nil;
            
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            if(error!=nil)
            {
                NSLog(@"web service error:%@",error);
            }
            else
            {
                if(receivedData !=nil)
                {
                    NSError *Jerror = nil;
                    
                    NSDictionary* json =[NSJSONSerialization
                                         JSONObjectWithData:receivedData
                                         options:kNilOptions
                                         error:&Jerror];
                    //   NSLog(@"user data is %@",json);
                    if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                        
                        commonUtils.userUpcomingArray = [[NSMutableArray alloc] init];
                        NSMutableDictionary *successDic = json[@"successData"];
                        //NSLog(@"%@",commonUtils.userUpcomingArray[activityIndex][@"get_user"]);
                      
                        NSLog(@"successDic ====> %@",successDic);
                       commonUtils.hideHud;
                        headerView.followersLbl.text = [NSString stringWithFormat:@"%@",successDic[@"follower_count"]];
                        headerView.followingLbl.text = [NSString stringWithFormat:@"%@",successDic[@"follow_count"]];
                        headerView.eventLbl.text =  [NSString stringWithFormat:@"%@",successDic[@"activities_count"]];
                        commonUtils.userUpcomingArray = [successDic[@"up_coming_events"]mutableCopy];
                        
                        BOOL is_following = [successDic[@"is_following"]boolValue];
                        if (is_following) {
                            [addUserImg setImage:[UIImage imageNamed:@"follow-check"]];
                           // [self FollowUnfollowAPI:otherUserID AddOrRemove:@"add_follower"];
                        } else {
                            [addUserImg setImage:[UIImage imageNamed:@"follow-plus"]];
                            //[self FollowUnfollowAPI:otherUserID AddOrRemove:@"un_follower"];
                        }

                        [MBProgressHUD hideHUDForView:headerView animated:YES];
                        NSString *imageUrl = [NSString stringWithFormat:@"%@",successDic[@"user"][@"photo"]];
                        [headerView.userProfileIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                                                    placeholderImage:[UIImage imageNamed:@"user_avatar"]
                                                             options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
                    
                        headerView.userNameLbl.text = [[NSString stringWithFormat:@"@%@",successDic[@"user"][@"username"]]lowercaseString];
                        if (![successDic[@"user"][@"ice_breaker"]isKindOfClass:[NSNull class]]) {
                              headerView.userDescLbl.text = successDic[@"user"][@"ice_breaker"];
                        }
                      
                        _userFirstNameLbl.text = successDic[@"user"][@"first_name"];
                        NSMutableArray *eventsImages = [NSMutableArray new];
                        eventsImages = [successDic[@"ice_pics"]mutableCopy];
                        
                        //NSLog(@"userupcoming commutil array is %@",commonUtils.userUpcomingArray);
                        
                        icePics =  [[NSMutableArray alloc]init];
                        icePics =  [successDic[@"user_image"]mutableCopy];
                        for (int i = 0; i<eventsImages.count; i++) {
                            [icePics addObject:eventsImages[i]];
                        }
                        
                        pEyeCandyContentCell *eyeCandyContentCell;
                        [eyeCandyContentCell.eyeCandyCv reloadData];
                        [mainTV reloadData];
                    }
                    if(Jerror!=nil)
                    {
                         [MBProgressHUD hideHUDForView:headerView animated:YES];
                        // NSLog(@"json error:%@",Jerror);
                    }
                }
            }
        
        }
        
    }
-(void)showAllImages:(UIButton*)sender{
    allImagesVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"allImagesVC"];
    VC.iceImages = icePics;
    [self.navigationController pushViewController:VC animated:YES];
}
-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    //    [converter setDateFormat:format];
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    //    NSDate *convertedDate = [converter dateFromString:PassedDate];
    //    NSString *convertedDateString = [converter stringFromDate:convertedDate];
    //    return convertedDateString;
    
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    // Get reference to the destination view controller
    ProfileCalendarVC *vc = [segue destinationViewController];
       // Pass any objects to the view controller here, like...
    vc.otherUserId = otherUserID;
    
    
}
-(void)playvideomethod:(UIButton*)sender{
    NSURL *videoUrl;
    if (icePics[sender.tag][@"ice_id"]) {
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,icePics[sender.tag][@"image"]]];
    }
    else{
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserVideoBaseURL,icePics[sender.tag][@"image"]]];
    }
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}

@end
