//
//  FollowRootVC.h
//  ICE
//
//  Created by LandToSky on 1/9/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FollowRootVC : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *firstNameLbl;
@property (nonatomic, assign) FollowPageIndex pageIndex;
@property (nonatomic,strong) NSString *userId,*firstName;

@end
