//
//  FollowRootVC.m
//  ICE
//
//  Created by LandToSky on 1/9/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "FollowRootVC.h"
#import "FollowingVC.h"
#import "FollowersVC.h"

@interface FollowRootVC ()<UIScrollViewDelegate>
{
    FollowingVC *followingVC;
    FollowersVC *followersVC;
    
    // TabBar
    __weak IBOutlet UILabel *followingLbl;
    __weak IBOutlet UILabel *followerLbl;
    IBOutletCollection(UIButton) NSMutableArray *tabBarBtns;
    IBOutlet UILabel *underLbl;
    
    // ScrollView
    IBOutlet UIScrollView *tabScrollView;
    IBOutletCollection(UIView) NSMutableArray *pageView;
}

@end

@implementation FollowRootVC
@synthesize userId,firstName;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
   
}

- (void)initUI
{
    tabScrollView.delegate = self;
    
    /* Adjust Frame Layout */
    CGRect frame = CGRectZero;
    frame = tabScrollView.frame;
    for (int i = 0; i < 2; i++)
    {
        frame.origin.x = tabScrollView.frame.size.width * i;
        frame.origin.y = 0;
        [(UIView*)pageView[i] setFrame:frame];
    }
    
    [tabScrollView setContentSize:CGSizeMake(tabScrollView.frame.size.width*2, tabScrollView.frame.size.height)];
    [tabScrollView setPagingEnabled:YES];
    
    // If pageIndex is setting, scroll to correspond page
    [tabScrollView setContentOffset:CGPointMake(tabScrollView.frame.size.width * _pageIndex, 0)];
    _firstNameLbl.text = firstName;
}

- (void)initData
{
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"segue-following"]) {
         followingVC = segue.destinationViewController;
        followingVC.userID = userId;
       
    } else if ([segueName isEqualToString:@"segue-followers"]){
         followersVC = segue.destinationViewController;
        followersVC.userID = userId;
       
    }
}

#pragma mark - onTabBar Button
- (IBAction)onTabBarButton:(UIButton*)sender
{
    NSInteger index = [tabBarBtns indexOfObject:sender];
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [tabScrollView setContentOffset:CGPointMake(tabScrollView.frame.size.width * index, 0)];
                         
                     }];
}
#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView != tabScrollView) return;
    
    CGRect frame = underLbl.frame;
    frame.origin.x = scrollView.contentOffset.x / 2;
    underLbl.frame = frame;
    
    /* Get Current Page Number */
    CGFloat width = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    
    if (page == 0) {
        [followingLbl setTextColor:appController.darkFontColor];
        [followerLbl setTextColor:appController.lightFontColor];
        
    } else if (page == 1){
        [followingLbl setTextColor:appController.lightFontColor];
        [followerLbl setTextColor:appController.darkFontColor];
        
    }

    [scrollView setScrollEnabled:YES];
}

@end
