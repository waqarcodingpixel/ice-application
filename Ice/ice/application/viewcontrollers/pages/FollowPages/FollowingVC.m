//
//  FollowingVC.m
//  ICE
//
//  Created by LandToSky on 1/9/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "FollowingVC.h"
#import "FollowTVCell.h"

@interface FollowingVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *mainTV;
    NSMutableArray *followingArray;
}

@end

@implementation FollowingVC
@synthesize userID;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self initData];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self getFollowing];
}
- (void) initUI
{
    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) initData
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"followingVC"
                                               object:nil];
    
  
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return followingArray.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowTVCell *cell = (FollowTVCell*) [tableView dequeueReusableCellWithIdentifier:@"FollowTVCell"];
    if (cell == nil) {
        cell = [[FollowTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FollowTVCell"];
    }
    cell.userNameLbl.text = [NSString stringWithFormat:@"%@",followingArray[indexPath.row][@"get_following"][@"first_name"]];
    NSString *imageUrl = [NSString stringWithFormat:@"%@",followingArray[indexPath.row][@"get_following"][@"photo"]];
    [cell.userIV sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                    placeholderImage:[UIImage imageNamed:@"user_avatar"]
                             options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    NSMutableArray *followerArray = [[NSMutableArray alloc]initWithArray:followingArray[indexPath.row][@"get_following"][@"follow"]];
    NSMutableArray *followingArrayFromApi = [[NSMutableArray alloc]initWithArray:followingArray[indexPath.row][@"get_following"][@"following"]];
    NSString *iceCount = [NSString stringWithFormat:@"%@",followingArray[indexPath.row][@"get_ice_following_count"]];
    cell.followersCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)followerArray.count];
    cell.followingCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)followingArrayFromApi.count];
    cell.eventsCountLbl.text = iceCount;
    
    NSLog(@" User ID ====> %@",[commonUtils.userData valueForKey:@"id"]);
    NSLog(@" followingArray ====> %@",followingArray[indexPath.row]);
    NSLog(@" userID ====> %@",commonUtils.commutilUserID);
    
    if ([[commonUtils.userData valueForKey:@"id"] intValue] == [commonUtils.commutilUserID intValue]){
        cell.userAddImg.hidden = false;
        cell.userAddBtn.hidden = false;
        
        cell.userAddBtn.tag = indexPath.row;
        [cell.userAddBtn addTarget:self action:@selector(unfollowUser:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }else {
        cell.userAddImg.hidden = true;
        cell.userAddBtn.hidden = true;
        
        
    }
    
    return cell;
}
-(void)getFollowing{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        [commonUtils showHud:self.view];
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"http://139.162.37.73/iceapp/api/v1/get_follow_following/%@?time_zone=%@",commonUtils.commutilUserID,timezone];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        NSLog(@"urlString for following ===> %@",urlString);
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                   
                    NSMutableDictionary *successDic = [[NSMutableDictionary alloc]init];
                    followingArray = [[NSMutableArray alloc]init];
                    successDic = [json[@"successData"]mutableCopy];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [commonUtils hideHud];
                        followingArray = [successDic[@"following"]mutableCopy];
                        [mainTV reloadData];
                    });
                   
                    
          
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
    }
    
}
-(void)unfollowUser:(UIButton*)sender{
    
    
    NSLog(@"user ID %@",userID);
    
    
    NSString *otherUserID = [NSString stringWithFormat:@"%@",followingArray[sender.tag][@"get_following"][@"id"]];
    NSLog(@"user id %@",otherUserID);
    NSLog(@"user id %@",otherUserID);
    
    [self unfolloApi:otherUserID];
    [followingArray removeObjectAtIndex:sender.tag];
    [mainTV reloadData];
}
-(void)unfolloApi:(NSString*)followedID{
    
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable) {
            //my web-dependent code
            [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
        }
        else {
            //there-is-no-connection warning
            
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
            
            [_params setObject:followedID forKey:@"followed_id"];
  
            
            
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
            
            NSString *serverUrl = [NSString stringWithFormat:@"%@%@",ServerUrl,@"un_follower"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:serverUrl];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            
            
            
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  //NSLog(@"Server Response %@",response);
                                                  NSLog(@"dictionary %@",dictionary);
                                                  NSString *message = [dictionary valueForKey:@"errorMessage"];
                                                  
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  if([statusis isEqualToString:@"success"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"followerVC"
                                                       object:self];

                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                      });
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  NSLog(@"no data returned");
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                                  });
                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                  NSLog(@"Server not responding");
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                             
                                                  });
                                                  //couldn't download
                                                  
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
        }
        
    }
- (void) reloadTable:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"followingVC"]){
          NSLog (@"Successfully received the test notification!");
        [self getFollowing];
    }
    
}
@end
