//
//  MyProfileEditVC.h
//  ICE
//
//  Created by LandToSky on 12/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
#import <EHPlainAlert/EHPlainAlert.h>
@interface MyProfileEditVC : BaseViewController<UIAlertViewDelegate>
- (IBAction)updateProfileBtn:(id)sender;
@property IBOutlet UIImageView *userProfileIV;
@property IBOutlet SZTextView *iceBreakerTv;
@property IBOutlet UITextField *profileNameTF, *userNameTF, *emailTF, *phoneTF;
@property NSString *profileName,*userName,*email,*phone,*iceBreaker;
@property UIImage *profileImage;
- (IBAction)backBtn:(id)sender;
- (IBAction)changePswdBtn:(id)sender;
@property (nonatomic)BOOL isProfileImageChanged;
@property (weak, nonatomic) IBOutlet UIButton *updateProfileOutlet;
@property (weak, nonatomic) IBOutlet UIView *profileNameView;
@property (weak, nonatomic) IBOutlet UIView *userNameView;
@property (weak, nonatomic) IBOutlet UIView *iceBreakerView;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;

@property (weak, nonatomic) IBOutlet UIImageView *profileCheckIV;



@end
