//
//  MyProfileVC.m
//  ICE
//
//  Created by LandToSky on 12/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "MyProfileVC.h"


#import "ProfileHeaderView.h"
#import "UIView+GSKLayoutHelper.h"
#import "UIViewController+KNSemiModal.h"

#import "pUpcomingTitleCell.h"
#import "pUpcomingContentCell.h"
#import "pEyeCandyTitleCell.h"
#import "pEyeCandyContentCell.h"
#import "EventImageCVCell.h"

#import "DetailVC.h"
#import "UserProfileVC.h"
#import "EventsVC.h"
#import "FollowRootVC.h"
#import "ICEVC.h"
#import "MyProfileEditVC.h"
#import "testViewController.h"
#import "allImagesVC.h"
#import "PhotoVideoShowVC.h";
@interface MyProfileVC ()<UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate,UICollectionViewDataSource,GSKStretchyHeaderViewStretchDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    IBOutlet UITableView *mainTV;
    
    
    NSInteger upComingTVCellNum;
    NSInteger eyeCandyCVCellNum;
    
    
    //    profile image upload
    IBOutlet UIImageView *userProfileIV;
    BOOL noCamera, isProfileImageChanged;
    UIImage *changedImage;
    MyProfileEditVC *editVc;
    NSMutableArray *icePics;
    NSMutableArray *imagesArray;
    PHImageManager *manager;
    NSMutableArray *images ;
   
    // assets contains PHAsset objects.
    UIImage *image2;
    
    int counter;
}


@property (nonatomic, strong) IBOutlet UIView *photoPickContainerView;



@end

@implementation MyProfileVC
@synthesize userName,profileName,profileImage,iceBreaker,headerView,fullName,userImage,iceDesc;
- (void)viewDidLoad {
    [super viewDidLoad];
  
    counter = 0;
        editVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileEditVC"];
   //[self loadDataOfEditProfile];
     [self initUI];
      [self initData];
    icePics = [NSMutableArray new];
    imagesArray = [NSMutableArray new];
   
    
}

- (void)initUI
{
   
   
    headerView = [[[NSBundle mainBundle] loadNibNamed:@"ProfileHeaderView" owner:self options:nil] firstObject];
  
            // UI UPDATE 3
    
        
  
    headerView.stretchDelegate = self;
    headerView.minimumContentHeight = 85;
    headerView.maximumContentHeight = 230;
    headerView.userNameLbl.text = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
        [mainTV addSubview:headerView];
    [mainTV setBackgroundColor:[UIColor colorWithHex:@"#f7f7f7" alpha:1.0f]];
    
    
    // Header View Action    
    [headerView.eventsBtn addTarget:self action:@selector(onShowEventsVC:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.followingBtn addTarget:self action:@selector(onShowFollowingVC:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.followersBtn addTarget:self action:@selector(onShowFollowersVC:) forControlEvents:UIControlEventTouchUpInside];
    _fullNameLbl.text = fullName;
    headerView.userProfileIv.image = userImage;
    headerView.userDescLbl.text = iceDesc;
    //  [self updateUserDetails];
      

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self getUserDetails:[NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]]];
   
    
    // [self updateUserDetails];
}
- (void)initData
{
    
    noCamera = NO;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        noCamera = YES;
    }
    isProfileImageChanged = NO;
    changedImage = nil;
    
    
   
 
    
   }

- (void)viewDidLayoutSubviews
{
    if (upComingTVCellNum == 0 && eyeCandyCVCellNum == 0){
        CGFloat bottomHeight = mainTV.frame.size.height - 90 - 210 - 238 +5;
        UIView *bottomView = [self.view viewWithTag:1001];
        [commonUtils resizeFrame:bottomView withWidth:bottomView.frame.size.width withHeight:bottomHeight];
    }

}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
 [self loadDataOfEditProfile];
        //[self updateUserDetails];


}

#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    static NSInteger count = 0;
    count = 2+upComingTVCellNum+ ((eyeCandyCVCellNum>0)?1:0);
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 0.0f;
    
    if (indexPath.row == 0) {
        height = (upComingTVCellNum == 0)? 210 : 60;
        
    } else if (0<indexPath.row && indexPath.row < 1+upComingTVCellNum ) {
        height = 78;
        
    } else if (indexPath.row == 1+upComingTVCellNum){
        height = (eyeCandyCVCellNum == 0) ? 238 : 75;
        
    } else if (indexPath.row == 2+upComingTVCellNum){
        if (IS_IPHONE_5) {
              height = (eyeCandyCVCellNum < 4) ? 125 : 220;
        }
        if (IS_IPHONE_6) {
            height = (eyeCandyCVCellNum < 4) ? 125 : 250;
        }
        if (IS_IPHONE_6P) {
             height = (eyeCandyCVCellNum < 4) ? 125 : 290;
        }
    }
    
    return height ;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    pUpcomingTitleCell* upComingTitleCell;
    pUpcomingContentCell *upComingContentCell;
    pEyeCandyTitleCell *eyeCandyTitleCell;
    pEyeCandyContentCell *eyeCandyContentCell;
    
    
    if (indexPath.row == 0) {
        if (upComingTVCellNum == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"NoUpcomingEventTVCell"];
            
        } else {
            upComingTitleCell = [tableView dequeueReusableCellWithIdentifier:@"pUpcomingTitleCell"];
            if (upComingTitleCell == nil) {
                upComingTitleCell = [[pUpcomingTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pUpcomingTitleCell"];
            }
            [upComingTitleCell.upComingAppearanceViewAllBtn addTarget:self action:@selector(onShowRightMenuVC:) forControlEvents:UIControlEventTouchUpInside];
            cell = upComingTitleCell;
            
        }
        
    } else if (0<indexPath.row && indexPath.row < 1+upComingTVCellNum ) {
        upComingContentCell = [tableView dequeueReusableCellWithIdentifier:@"pUpcomingContentCell"];
        if (upComingContentCell == nil) {
            upComingContentCell = [[pUpcomingContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pUpcomingContentCell"];
        }
         int cellRow = indexPath.row - 1;
        upComingContentCell.showDetailBtn.tag = cellRow;
        [upComingContentCell.showDetailBtn addTarget:self action:@selector(onShowEventDetailVC:) forControlEvents:UIControlEventTouchUpInside];
        [upComingContentCell.showUserProfileBtn addTarget:self action:@selector(onShowUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
        
       // [upComingContentCell.showDetailBtn addTarget:self action:@selector(onShowEventDetailVC:) forControlEvents:UIControlEventTouchUpInside];
        [upComingContentCell.showUserProfileBtn addTarget:self action:@selector(onShowUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
        upComingContentCell.titleLbl.text = commonUtils.userUpcomingArray[cellRow][@"title"];
        upComingContentCell.addressLbl.text = commonUtils.userUpcomingArray[cellRow][@"location"];
        upComingContentCell.userNameLbl.text = [NSString stringWithFormat:@"via %@",commonUtils.userUpcomingArray[cellRow][@"get_user"][@"first_name"]];
        NSString *imageUrl = [NSString stringWithFormat:@"%@",commonUtils.userUpcomingArray[cellRow][@"get_user"][@"photo"]];
        [upComingContentCell.userIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                                      placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                               options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
//        upComingContentCell.weekdayLbl.text = commonUtils.userUpcomingArray[cellRow][@"event_day"];
//        upComingContentCell.dateLbl.text = commonUtils.userUpcomingArray[cellRow][@"event_day_start"];
//        upComingContentCell.monthLbl.text = commonUtils.userUpcomingArray[cellRow][@"event_month"];
//        NSString *time = [NSString stringWithFormat:@"%@-%@PST",commonUtils.userUpcomingArray[cellRow][@"event_start_time"],commonUtils.userUpcomingArray[cellRow][@"event_end_time"]];
//        
//        upComingContentCell.timeLbl.text = time;
        NSDate *today = [NSDate date];
        
        
        NSDate *todayDate = [self toLocalTime:today];
//        NSLog(@"today date is %@",todayDate);
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        
        NSDate * endDateFromApi = [startTimeformatter dateFromString:commonUtils.userUpcomingArray[cellRow][@"server_end_time"]];
        // your date
        
//        NSLog(@"server time %@",endDateFromApi);
//        NSLog(@"server time string %@",[startTimeformatter stringFromDate:endDateFromApi]);
        
        NSComparisonResult result;
        //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
        
        result = [todayDate compare:endDateFromApi]; // comparing two dates
//        NSLog(@"array date is %@",commonUtils.userUpcomingArray[cellRow][@"server_end_time"]);
        if(result==NSOrderedDescending)
        {
            NSLog(@"today is less");
            upComingContentCell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"until %@",commonUtils.userUpcomingArray[cellRow][@"event_end_time"]];
            
        }
        
        else if(result==NSOrderedAscending){
            NSLog(@"server date  is less");
            upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
            upComingContentCell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:today]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"ends %@.%@th",endMonth,endDate];
            
        }
        
        
        else{
           upComingContentCell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"until %@",commonUtils.userUpcomingArray[cellRow][@"event_end_time"]];
            NSLog(@"Both dates are same");
        }

        cell = upComingContentCell;
        
    } else if (indexPath.row == 1+upComingTVCellNum){
        if (eyeCandyCVCellNum == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"NoPhotoVideoTVCell"];
        } else {
            eyeCandyTitleCell = [tableView dequeueReusableCellWithIdentifier:@"pEyeCandyTitleCell"];
            if (eyeCandyTitleCell == nil) {
                eyeCandyTitleCell = [[pEyeCandyTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pEyeCandyTitleCell"];
            }
            cell = eyeCandyTitleCell;
        }
        
        eyeCandyTitleCell.eyeCandyViewAllBtn.tag = indexPath.row;
        [eyeCandyTitleCell.eyeCandyViewAllBtn addTarget:self action:@selector(showAllImages:) forControlEvents:UIControlEventTouchUpInside];

    } else if (indexPath.row == 2+upComingTVCellNum){
        eyeCandyContentCell = [tableView dequeueReusableCellWithIdentifier:@"pEyeCandyContentCell"];
        if (eyeCandyContentCell == nil) {
            eyeCandyContentCell = [[pEyeCandyContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pEyeCandyContentCell"];
        }
    
        eyeCandyContentCell.eyeCandyCv.delegate = self;
        eyeCandyContentCell.eyeCandyCv.dataSource = self;
        cell = eyeCandyContentCell;
        
        
    }
    [eyeCandyContentCell.eyeCandyCv reloadData];
    return cell;
}


#pragma mark - On Other VC
- (void)onShowUserProfileVC:(UIButton*)sender
{
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowEventDetailVC:(UIButton*) sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.iceDetails = commonUtils.userUpcomingArray[sender.tag];
    vc.comingFrom = @"userprofile";
    vc.selectedTag = sender.tag;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowFollowingVC:(UIButton *)sender
{
    FollowRootVC *vc = (FollowRootVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"FollowRootVC"];
    vc.pageIndex = Following;
    vc.firstName = commonUtils.userData[@"first_name"];
    commonUtils.commutilUserID = [NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowFollowersVC:(UIButton *)sender
{
    FollowRootVC *vc = (FollowRootVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"FollowRootVC"];
    vc.pageIndex = Followers;
     vc.firstName = commonUtils.userData[@"first_name"];
 commonUtils.commutilUserID = [NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowEventsVC:(UIButton *)sender
{
    EventsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsVC"];
    commonUtils.commutilUserID = [NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowRightMenuVC:(UIButton*) sender
{
    [self.sideMenuController showRightViewAnimated:sender];
}

- (IBAction)onShowICEVC:(UIButton*) sender
{
    SidePanelVC *sidePanelVC = (SidePanelVC*)self.sideMenuController;
    
    // Goto ICEVC
    if (!sidePanelVC.ICENavigation) {
        
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ICEVC"];
        sidePanelVC.ICENavigation = [[UINavigationController alloc] initWithRootViewController:vc];
        [sidePanelVC.ICENavigation setNavigationBarHidden:YES];
    }
    
    [sidePanelVC setRootViewController:sidePanelVC.ICENavigation];
    [sidePanelVC hideLeftViewAnimated:sender];


}


#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return icePics.count+1;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize mElementSize = CGSizeMake(collectionView.frame.size.width/3 - 1, collectionView.frame.size.width/3-1);
    return mElementSize;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
    NSLog(@"indexpath is %ld",(long)indexPath.row);
    if (indexPath.row == 0) {
        cell.eventIv.image = [UIImage imageNamed:@"add-image"];
        cell.videoPlayBtn.hidden = YES;
    }
    else{
        if ([icePics[indexPath.row-1]isKindOfClass:[UIImage class]]) {
            
          cell.eventIv.image = icePics[indexPath.row-1];
            cell.videoPlayBtn.hidden = YES;
        }
        else{
            NSString *imageUrl;
            NSString *type = [NSString stringWithFormat:@"%@",icePics[indexPath.row-1][@"type"]];
            if ([type isEqualToString:@"image"]) {
                
            
                    cell.videoPlayBtn.hidden = YES;
               

                imageUrl = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,icePics[indexPath.row-1][@"image"]];
            }
            else{
              
                     cell.videoPlayBtn.hidden = NO;
                cell.videoPlayBtn.tag = indexPath.row-1;
                [cell.videoPlayBtn addTarget:self action:@selector(playvideomethod:) forControlEvents:UIControlEventTouchUpInside];
                imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,icePics[indexPath.row-1][@"poster"]];

            }
            [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                            placeholderImage:[UIImage imageNamed:@"image0"]
                                     options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        }

    }

    
    return cell;
    
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        if(self.isLoadingBase) return;
        
        [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                        KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                        KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                        KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                        }];

    }
    else {
        PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
        vc.currentPageNum = indexPath.row-1;
        commonUtils.userIcePics = icePics;
        vc.comingFrom = @"ownProfile";
        [self.navigationController pushViewController:vc animated:YES];
    }
 
    
}
#pragma mark - GSKStretchView Delegate

#pragma mark - Stretch Delegate
- (void)stretchyHeaderView:(ProfileHeaderView *)realHeaderView didChangeStretchFactor:(CGFloat)stretchFactor
{
    
    CGFloat limitedStretchFactor = MIN(1, stretchFactor);
    
    // User Profile ImageView Size Adjust
    CGSize minImageSize = CGSizeMake(55, 55);
    CGSize maxImageSize = CGSizeMake(125, 125);
    
    realHeaderView.userProfileIv.size = CGSizeInterpolate(limitedStretchFactor, minImageSize, maxImageSize);
    
    // Bottom (Following/Followers/Events) View Adjust
    CGSize minBottomViewSize = CGSizeMake(realHeaderView.size.width-55, 55);
    CGSize maxBottomViewSize = CGSizeMake(realHeaderView.size.width, 70);
    CGPoint minBottomViewOrigin = CGPointMake(55, 0);
    CGPoint maxBottomViewOrigin = CGPointMake(0, 125);
    
    realHeaderView.bottomView.size = CGSizeInterpolate(limitedStretchFactor, minBottomViewSize, maxBottomViewSize);
    realHeaderView.bottomView.left = CGFloatInterpolate(limitedStretchFactor, minBottomViewOrigin.x, maxBottomViewOrigin.x);
    realHeaderView.bottomView.top = CGFloatInterpolate(limitedStretchFactor, minBottomViewOrigin.y, maxBottomViewOrigin.y);
    
    // User Name View
    [realHeaderView.userNameContainerView setAlpha:limitedStretchFactor];
}

#pragma mark - Profile Photo Change

- (IBAction)onProfilePhotoChange:(id)sender {
    if(self.isLoadingBase) return;

    [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                    KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                    KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                    KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                    }];
}
- (IBAction)onClickGallery:(id)sender {
    if(self.isLoadingBase) return;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.tintColor = [UIColor blackColor];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    [self presentViewController:picker animated:YES completion:NULL];
}
- (IBAction)onClickCamera:(id)sender {
    if(self.isLoadingBase) return;
    
    if(noCamera) {
        [commonUtils showVAlertSimple:@"Warning" body:@"Your device has no camera" duration:1.0f];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    changedImage = info[UIImagePickerControllerEditedImage];
    imagesArray = [NSMutableArray new];
    if (changedImage) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:changedImage,@"image",@"image",@"IsImage", nil];
        [imagesArray addObject:dic];
        
        
        [mainTV reloadData];
        
        isProfileImageChanged = YES;
        [self dismissSemiModalView];
        
        
    }
    else{
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSData *data = [NSData dataWithContentsOfURL:videoURL];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:videoURL forKey:@"image"];
        [dic setObject:data forKey:@"videodata"];
        [dic setValue:@"video" forKey:@"IsImage"];
        [dic setValue:@"camera" forKey:@"pickedType"];
        [imagesArray addObject:dic];
        
        [mainTV reloadData];
        
        [self dismissSemiModalView];
        
    }
    [picker dismissViewControllerAnimated:NO completion:^{
        
        
    }];
    [self uploadMyPic];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:NO
                               completion:^{
                                   
                                   /* ScrollView Inset Error
                                   [commonUtils moveView:self.sidePanelController.centerPanelContainer
                                               withMoveX:0
                                               withMoveY:0];
                                   
                                   [commonUtils resizeFrame:self.sidePanelController.centerPanelContainer
                                                  withWidth:SCREEN_WIDTH
                                                 withHeight:SCREEN_HEIGHT];
                                    */
                               }];
    
    [self dismissSemiModalView];
    
}

-(void)updateUserDetails {
    
    
    if (![[commonUtils.userData valueForKey:@"photo"] isKindOfClass:[NSNull class]]&&![[commonUtils.userData valueForKey:@"photo"]isEqualToString:@""]) {
        NSString *imageUrlString = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"photo"]];
        NSURL *url = [NSURL URLWithString:imageUrlString];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        [headerView.userProfileIv setImage:[UIImage imageWithData:data]];
    
       
    }
    else {
       headerView.userProfileIv.image = [UIImage imageNamed:@"user-avatar"];
       
        
    }
    _fullNameLbl.text = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
    if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
        headerView.userDescLbl.text =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
    }
    else {
        headerView.userDescLbl.text = @"";
    }

}

    
    
   
  
//

    
   
- (IBAction)editBtn:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![self.navigationController.topViewController isKindOfClass:MyProfileEditVC.class])
        {
            editVc.isProfileImageChanged = NO;
            [self.navigationController pushViewController:editVc animated:YES];
        }
    });
    
//    testViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"testVc"];
//    [self.navigationController pushViewController:vc animated:YES];
}


-(void)loadDataOfEditProfile{

    
    
 
        
        editVc.profileImage  = headerView.userProfileIv.image;
    
    editVc.userName = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
    
    editVc.profileName= [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
    if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
        editVc.iceBreaker =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
    }
    else {
        editVc.iceBreaker = @"";
    }
    if (![[commonUtils.userData valueForKey:@"email"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"email"] != nil ) {
        editVc.email =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"email"]];
    }
    else {
        editVc.email = @"";
    }
    if (![[commonUtils.userData valueForKey:@"phone"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"phone"] != nil ) {
        editVc.phone =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"phone"]];
    }
    else {
        editVc.phone = @"";
    }
}
-(void)getUserDetails:(NSString*)userID{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"http://139.162.37.73/iceapp/api/v1/get_profile/%@?time_zone=%@",userID,timezone];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    commonUtils.userUpcomingArray = [[NSMutableArray alloc] init];
                    NSMutableDictionary *successDic = json[@"successData"];
                    //NSLog(@"%@",commonUtils.userUpcomingArray[activityIndex][@"get_user"]);
                    
                    
                    headerView.followersLbl.text = [NSString stringWithFormat:@"%@",successDic[@"follower_count"]];
                    headerView.followingLbl.text = [NSString stringWithFormat:@"%@",successDic[@"follow_count"]];
                    headerView.eventLbl.text =  [NSString stringWithFormat:@"%@",successDic[@"activities_count"]];
                    NSString *totalUserIced =  [NSString stringWithFormat:@"%@",successDic[@"ice"]];
                    NSString *totalUserAdded =  [NSString stringWithFormat:@"%@",successDic[@"added"]];
                    commonUtils.userTotalIced = [totalUserIced integerValue];
                    commonUtils.userTotalAdded = [totalUserAdded integerValue];
                    commonUtils.userUpcomingArray = [successDic[@"up_coming_events"]mutableCopy];
                    upComingTVCellNum = commonUtils.userUpcomingArray.count;
                   //   NSLog(@"commutil array is %@",commonUtils.userUpcomingArray);
                    
                    NSMutableArray *eventsImages = [NSMutableArray new];
                    eventsImages = [successDic[@"ice_pics"]mutableCopy];
                    
                    //NSLog(@"userupcoming commutil array is %@",commonUtils.userUpcomingArray);
                
                        icePics =  [[NSMutableArray alloc]init];
                        icePics =  [successDic[@"user_image"]mutableCopy];
                    for (int i = 0; i<eventsImages.count; i++) {
                        [icePics addObject:eventsImages[i]];
                    }
                  
                   
                     eyeCandyCVCellNum = icePics.count;
                    pEyeCandyContentCell *eyeCandyContentCell;
                    [eyeCandyContentCell.eyeCandyCv reloadData];
                    [mainTV reloadData];
                }
                if(Jerror!=nil)
                {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
    }
    
}
-(void)showAllImages:(UIButton*)sender {
    allImagesVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"allImagesVC"];
    VC.iceImages = icePics;
//    VC.iceImages = true;
    [self.navigationController pushViewController:VC animated:YES];
}
-(void)uploadMyPic{
    
  
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
       
              //    NSLog(@"reminder string %@",reminderString);
      
       
        if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
            //       NSArray *invitedarray = @[@6,@7];
            
            NSError *error;
            dispatch_async(dispatch_get_main_queue(), ^{
                  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            });
          
       
            
            // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
            
            //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
            

            
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
            
            NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"upload_image"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:Url];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // add image data
            
            for (int i=0; i<imagesArray.count; i++) {
                
                
                
                
                NSString *strValue = (NSString *)imagesArray[i][@"IsImage"] ;
                
                
                if ([strValue isEqualToString:@"image"]) {
                    
                    
                    NSString *fileName = [NSString stringWithFormat:@"image[%d]",i];
                    NSData *imageData = UIImageJPEGRepresentation(imagesArray[i][@"image"], 0.5);
                    if (imageData) {
                        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:imageData];
                        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    
                }
                else{
                    // NSURL *assetURL = [NSURL fileURLWithPath:imagesArray[i]];
                    NSData *postData = imagesArray[i][@"videodata"];
                    NSString *videoFileName = [NSString stringWithFormat:@"video[%d]",i];
                    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"file.mov\"\r\n", videoFileName] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"Content-Type: video/mov\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:postData];
                    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    UIImage *img = [[UIImage alloc]init];
                    if ([imagesArray[i][@"pickedType"]isEqualToString:@"galery"]) {
                        
                        
                        NSURL *assetURL = [NSURL fileURLWithPath:imagesArray[i][@"image"]];
                        AVURLAsset *asset = [AVURLAsset assetWithURL:assetURL];
                        //
                        
                        
                        
                        //
                        
                        
                        //AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[indexPath.row] options:nil];
                        
                        
                        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                        generate.appliesPreferredTrackTransform = YES;
                        NSError *err = NULL;
                        CMTime time = CMTimeMake(1, 60);
                        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
                        
                        img = [[UIImage alloc] initWithCGImage:imgRef];
              
                    }
                    else{
                        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[i][@"image"] options:nil];
                        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                        generate.appliesPreferredTrackTransform = YES;
                        NSError *err = NULL;
                        CMTime time = CMTimeMake(1, 60);
                        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
                        
                       img = [[UIImage alloc] initWithCGImage:imgRef];
                     
                        
                    }

                    // NSLog(@"image is %@",img);
                    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
                    NSString *thumbnailName = [NSString stringWithFormat:@"thumb[%d]",i];
                    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", thumbnailName] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:imageData];
                    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    
                    
                    
                    
                }
            }
        
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  //NSLog(@"Server Response %@",response);
                                                  NSString* myString;
                                                  myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                                  // NSLog(@"string is %@",myString);
                                                  NSLog(@"dictionary %@",dictionary);
                                                  NSString *message = [dictionary valueForKey:@"errorMessage"];
                                                  
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  if([statusis isEqualToString:@"success"]){
                                                      
                                                    
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          
                                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                          NSMutableDictionary *images = [[NSMutableDictionary alloc]initWithDictionary:[dictionary[@"successData"]mutableCopy]];
                                                          icePics = [NSMutableArray new];
                                                            icePics = images[@"images"];
                                                          eyeCandyCVCellNum = icePics.count;
                                                          [mainTV reloadData];
                                                         [self getUserDetails:[NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]]];
                                                          
                                                          
                                                          
                                                      });
                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                      
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                      });
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  NSLog(@"no data returned");
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                      
                                                    
                                                  });

                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                      
                                                      
                                                  });
                                                  NSLog(@"%@", err.localizedDescription);
                                                  //couldn't download
                                                  
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
            
            
            
        }
        
        
        
        
        else {
            [commonUtils showAlert:@"Error!" withMessage:@"Unable To Get Your Location.Please Allow The App To Get Your Location"];
        }
    }
    
}
-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    //    [converter setDateFormat:format];
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    //    NSDate *convertedDate = [converter dateFromString:PassedDate];
    //    NSString *convertedDateString = [converter stringFromDate:convertedDate];
    //    return convertedDateString;
    
}
- (IBAction)launchGMImagePicker:(id)sender
{
    
    [self dismissSemiModalView];
    if(self.isLoadingBase) return;
    GMImagePickerController *picker = [[GMImagePickerController alloc] init];
    picker.delegate = self;
    picker.title = @"";
    
    picker.customDoneButtonTitle = @"Done";
    picker.customCancelButtonTitle = @"Cancel";
    picker.customNavigationBarPrompt = @"Take a new photo or select an existing one!";
    
    picker.colsInPortrait = 3;
    picker.colsInLandscape = 5;
    picker.minimumInteritemSpacing = 2.0;
    
    //    picker.allowsMultipleSelection = NO;
    //    picker.confirmSingleSelection = YES;
    //    picker.confirmSingleSelectionPrompt = @"Do you want to select the image you have chosen?";
    
    //    picker.showCameraButton = YES;
    //    picker.autoSelectCameraImages = YES;
    
    picker.modalPresentationStyle = UIModalPresentationPopover;
    
    //    picker.mediaTypes = @[@(PHAssetMediaTypeImage)];
    
    //    picker.pickerBackgroundColor = [UIColor blackColor];
    //    picker.pickerTextColor = [UIColor whiteColor];
    //    picker.toolbarBarTintColor = [UIColor darkGrayColor];
    //    picker.toolbarTextColor = [UIColor whiteColor];
    //    picker.toolbarTintColor = [UIColor redColor];
    //    picker.navigationBarBackgroundColor = [UIColor blackColor];
    //    picker.navigationBarTextColor = [UIColor whiteColor];
    //    picker.navigationBarTintColor = [UIColor redColor];
    //    picker.pickerFontName = @"Verdana";
    //    picker.pickerBoldFontName = @"Verdana-Bold";
    //    picker.pickerFontNormalSize = 14.f;
    //    picker.pickerFontHeaderSize = 17.0f;
    //    picker.pickerStatusBarStyle = UIStatusBarStyleLightContent;
    //    picker.useCustomFontForNavigationBar = YES;
    
    UIPopoverPresentationController *popPC = picker.popoverPresentationController;
    popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;
    //    popPC.sourceView = _gmImagePickerButton;
    //    popPC.sourceRect = _gmImagePickerButton.bounds;
    //    popPC.backgroundColor = [UIColor blackColor];
    
    //[self showViewController:picker sender:nil];
    [self presentViewController:picker animated:YES completion:NULL];
}



#pragma mark - GMImagePickerControllerDelegate

- (void)assetsPickerController:(GMImagePickerController *)picker didFinishPickingAssets:(NSArray *)assetArray
{
    imagesArray = [NSMutableArray new];
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    //NSLog(@"assets array is %@",assetArray);
    //    PHAsset *asset = assetArray[0];
    //    NSLog(@"asset type is %ld",(long)asset.mediaType);
    //    NSLog(@"assets array first object %@",asset);
    //    NSLog(@"GMImagePicker: User ended picking assets. Number of selected items is: %lu", (unsigned long)assetArray.count);
    
   manager = [PHImageManager defaultManager];
   images = [NSMutableArray arrayWithCapacity:[assetArray count]];
   
    // assets contains PHAsset objects.
   
    counter = 0;
    [self recursiveFun :assetArray];
    
}
-(void)recursiveFun :(NSArray*)assetArray{
  
    
    PHAsset *asset =  assetArray[counter];
    if (asset.mediaType == 1) {
        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:nil
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            //   NSLog(@"info is %@",info);
                            image2 = image;
                            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:image2,@"image",@"image",@"IsImage", nil];
                            [imagesArray addObject:dic];
                            if (counter ==assetArray.count-1) {
                                //NSLog(@"images array is %@",imagesArray);
                                [self uploadMyPic];
                            }
                            else{
                                counter+=1;
                                [self recursiveFun:assetArray];
                            }
                            [self reloadCollectionViewAndChangeLayout];
                        }];
        
    }
    else if(asset.mediaType == 2){
        [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset *avAsset, AVAudioMix *audioMix, NSDictionary *info) {
            NSURL *url = (NSURL *)[[(AVURLAsset *)avAsset URL] fileReferenceURL];
            
            NSLog(@"url is %@",url);
            NSLog(@"url = %@", [url absoluteString]);
            NSLog(@"url = %@", [url relativePath]);
            AVURLAsset* myAsset = (AVURLAsset*)avAsset;
            NSData * data = [NSData dataWithContentsOfFile:myAsset.URL.relativePath];
            if (data) {
                //   NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[url relativePath],@"image",data,@"videodata",@"video",@"IsImage", nil];
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:[url relativePath] forKey:@"image"];
                [dic setObject:data forKey:@"videodata"];
                [dic setValue:@"video" forKey:@"IsImage"];
                [dic setValue:@"galery" forKey:@"pickedType"];
                [dic setObject:url forKey:@"playurl"];
                [imagesArray addObject:dic];
                if (counter==assetArray.count-1) {
                   // NSLog(@"images array is %@",imagesArray);
                    [self uploadMyPic];
                }
                else{
                    counter+=1;
                    [self recursiveFun:assetArray];
                }

                //  NSLog(@"data here is %lu",(unsigned long)imagesArray.count);
            }
            //                                [imagesArray addObject:[url relativePath] ];
            [self reloadCollectionViewAndChangeLayout];
        }];
        [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset* avasset, AVAudioMix* audioMix, NSDictionary* info){
            AVURLAsset* myAsset = (AVURLAsset*)avasset;
            NSData * data = [NSData dataWithContentsOfFile:myAsset.URL.relativePath];
            if (data) {
                
            }
        }];
        
    }

}
//Optional implementation:
-(void)assetsPickerControllerDidCancel:(GMImagePickerController *)picker
{
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"GMImagePicker: User pressed cancel button");
}
-(void)reloadCollectionViewAndChangeLayout{
    pEyeCandyContentCell *eyeCandyContentCell;
    [eyeCandyContentCell.eyeCandyCv reloadData];
    [mainTV reloadData];

    
}
-(void)playvideomethod:(UIButton*)sender{
    NSURL *videoUrl;
    if (icePics[sender.tag][@"ice_id"]) {
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,icePics[sender.tag][@"image"]]];
    }
    else{
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserVideoBaseURL,icePics[sender.tag][@"image"]]];
    }
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}


@end
