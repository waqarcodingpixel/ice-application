//
//  MyProfileVC.h
//  ICE
//
//  Created by LandToSky on 12/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileHeaderView.h"
#import "GMImagePickerController.h"
@interface MyProfileVC : BaseViewController


@property (weak, nonatomic) IBOutlet UILabel *fullNameLbl;
@property UIImage *profileImage;
@property NSString *profileName,*userName,*iceBreaker;
@property (weak,nonatomic) ProfileHeaderView *headerView;
@property (nonatomic,strong) NSString *fullName,*iceDesc;
@property (nonatomic,strong) UIImage *userImage;

@end
