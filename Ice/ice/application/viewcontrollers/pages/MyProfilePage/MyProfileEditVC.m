//
//  MyProfileEditVC.m
//  ICE
//
//  Created by LandToSky on 12/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "MyProfileEditVC.h"
#import "UIViewController+KNSemiModal.h"
#import "LeftMenuVC.h"
#import "MyProfileVC.h"
#import "ProfileHeaderView.h"
#import "HomeVC.h"
#import <IQKeyboardManager.h>
#import "changePswdVC.h"
 //https://github.com/glaszig/SZTextView  UITextView Placeholder

@interface MyProfileEditVC ()<UIScrollViewDelegate, UITextFieldDelegate,UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *contentView;
    UITextField *currentTextField;
   
    
    //    profile image upload
    
    BOOL isEditing, noCamera ;
    UIImage *changedImage;
      LeftMenuVC * menuVC;
    MyProfileVC *myprofileVc;
    UIAlertView *PushbackAlert;
    /* Ice Breaker TextView */
    

}

@property (nonatomic, strong) IBOutlet UIView *photoPickContainerView;

@end


@implementation MyProfileEditVC
@synthesize userNameTF,userName,email,profileName,profileImage,phone,iceBreaker,isProfileImageChanged;
- (void)viewDidLoad {
    [super viewDidLoad];
    
         [self initUI];
    [self initData];
    [self resetAllBorderColors];
}

- (void)initUI
{
//    if (commonUtils.cominfFromFB) {
//        _emailTF.enabled = NO;
//        _profileNameTF.enabled = NO;
//        userNameTF.enabled = NO;
//    }
//    else {
//        _emailTF.enabled = NO;
//        userNameTF.enabled = NO;
//       _profileNameTF.enabled = YES;
//    }

    dispatch_async(dispatch_get_main_queue(), ^(void){
        //Run UI Updates
   
        _profileNameTF.delegate =
    userNameTF.delegate =
    _emailTF.delegate =
    _phoneTF.delegate = self;
      scrollView.delegate = self;
    UIView *cameraView = (UIView*) [self.view viewWithTag:101];
    [commonUtils setRoundedRectBorderView:cameraView withBorderWidth:1.0f withBorderColor:RGBA(255, 255, 255, 0.09) withBorderRadius:0.0f];
    
    /* Change UITextField PlaceHolder Color */
    [commonUtils changeUITextFiledPlaceHolderColor:_profileNameTF withPlaceHolderText:@"Profile name" withColor:[UIColor lightGrayColor]];
    [commonUtils changeUITextFiledPlaceHolderColor:userNameTF withPlaceHolderText:@"User name" withColor:[UIColor lightGrayColor]];
    [commonUtils changeUITextFiledPlaceHolderColor:_emailTF withPlaceHolderText:@"E mail" withColor:[UIColor lightGrayColor]];
    [commonUtils changeUITextFiledPlaceHolderColor:_phoneTF withPlaceHolderText:@"Phone Number" withColor:[UIColor lightGrayColor]];
    
    /* Ice Breaker TextView */
    _iceBreakerTv.delegate = self;
     // [scrollView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen)]];
        
   
    
  
    
        CGFloat inset = 1.0;
        // ios 7
        if ([UITextView instancesRespondToSelector:@selector(setTextContainerInset:)]) {
            _iceBreakerTv.textContainerInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        } else {
            self.iceBreakerTv.contentInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        }
       
   
        userNameTF.text = userName;
        _emailTF.text = email;
        _phoneTF.text = phone;
        _userProfileIV.image = profileImage;
        _profileNameTF.text = profileName;
        _iceBreakerTv.text = iceBreaker;
    
     });
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    scrollView.userInteractionEnabled = YES;
    //scrollView.exclusiveTouch = YES;
    scrollView.delaysContentTouches = NO;
  //  scrollView.contentSize = contentRect.size;
    CGSize scrollableSize = CGSizeMake(0, contentRect.size.height);
    [scrollView setContentSize:scrollableSize];
 
   
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
     _updateProfileOutlet.enabled = NO;
    _profileCheckIV.alpha = 0.2;
    [self resetAllBorderColors];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    dispatch_async (dispatch_get_main_queue(), ^
                    {
                       
                                          });
}

- (void)initData
{
    isEditing = NO;
    noCamera = NO;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        noCamera = YES;
    }
    isProfileImageChanged = NO;
    changedImage = nil;
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _phoneTF.inputAccessoryView = numberToolbar;
    menuVC = (LeftMenuVC*) self.sideMenuController.leftViewController;
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    myprofileVc = self.navigationController.viewControllers[numberOfViewControllers-2];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}


#pragma mark - Profile Photo Change

- (IBAction)onProfilePhotoChange:(id)sender {
    if(self.isLoadingBase) return;
    
    if(isEditing) {
        [currentTextField resignFirstResponder];
        isEditing = NO;
    }
    [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                    KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                    KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                    KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                    }];
}
- (IBAction)onClickGallery:(id)sender {
    if(self.isLoadingBase) return;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.tintColor = [UIColor blackColor];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}
- (IBAction)onClickCamera:(id)sender {
    if(self.isLoadingBase) return;
    
    if(noCamera) {
        [commonUtils showVAlertSimple:@"Warning" body:@"Your device has no camera" duration:1.0f];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    changedImage = info[UIImagePickerControllerEditedImage];
    [self.userProfileIV setContentMode:UIViewContentModeScaleAspectFill];
    [self.userProfileIV setImage:changedImage];
    
    isProfileImageChanged = YES;
    [self dismissSemiModalView];
    [picker dismissViewControllerAnimated:NO
                               completion:^{
        /* ScrollView Inset Error
        [commonUtils moveView:self.sidePanelController.centerPanelContainer
                    withMoveX:0
                    withMoveY:0];
        
        [commonUtils resizeFrame:self.sidePanelController.centerPanelContainer
                       withWidth:SCREEN_WIDTH
                      withHeight:SCREEN_HEIGHT];
         */
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:NO
                               completion:^{
                                   
        /* ScrollView Inset Error
        [commonUtils moveView:self.sidePanelController.centerPanelContainer
                    withMoveX:0
                    withMoveY:0];
        
        [commonUtils resizeFrame:self.sidePanelController.centerPanelContainer
                       withWidth:SCREEN_WIDTH
                      withHeight:SCREEN_HEIGHT];
         */
    }];

    [self dismissSemiModalView];
    
}


#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(self.isLoadingBase) return NO;
    currentTextField = textField;
    isEditing = YES;
    
//    float offset = 0;
//    
//    if(currentTextField == self.profileNameTF) {
//        offset = 0;
//    } else if(currentTextField == self.userNameTF) {
//        offset = 0;
//    } else if(currentTextField == self.emailTF) {
//        offset = 150;
//    } else if(currentTextField == self.phoneTF) {
//        offset = 170;
//    }
//    
//    [scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.isLoadingBase) return NO;
   // [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    isEditing = NO;
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField.text.length>0) {
        textField.superview.layer.borderColor = [UIColor clearColor].CGColor;
        textField.superview.layer.borderWidth = 1.0;
    }
    [self enablingUpdateProfileBtn];
    return YES;
}

#pragma mark - TextView Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(self.isLoadingBase) return NO;
   // [scrollView setContentOffset:CGPointMake(0, 130) animated:YES];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if(self.isLoadingBase) return NO;
    
    if ([text isEqualToString:@"\n"]) {
       // [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        isEditing = NO;
        return [textView resignFirstResponder];
    }
    
    // Limit line of UITextView is 2
    NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    CGFloat textWidth = CGRectGetWidth(UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset));
    textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
    
    CGSize boundingRect = [self sizeOfString:newText constrainedToWidth:textWidth font:textView.font];
    NSInteger numberOfLines = boundingRect.height / textView.font.lineHeight;
    
    return numberOfLines <= 2;
    
}

-(CGSize)sizeOfString:(NSString *)string constrainedToWidth:(double)width font:(UIFont *)font
{
    return  [string boundingRectWithSize:CGSizeMake(width, DBL_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size;
}

#pragma mark - ScrollView Tap
- (void) onTappedScreen {
    if (self.isLoadingBase) return;
    [self.view endEditing:YES];
   // [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
-(void)updateProfile:(NSMutableDictionary *)userData {
    [self resetAllBorderColors];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
      
    }
    else {
        //there-is-no-connection warning
        [currentTextField resignFirstResponder];
        [commonUtils showHud:self.view];
    
            
    
       
            
            // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
            
            //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
            
        [_params setObject:userData[@"first_name"] forKey:@"full_name"];
        [_params setObject:userData[@"ice_breaker"] forKey:@"ice_breaker"];
        [_params setObject:userData[@"phone"] forKey:@"phone"];
        NSString *str = userNameTF.text;
        NSString *newStr;
        if([str hasPrefix:@"@"]) {
           newStr = [str substringFromIndex:1];
        }
        else{
            newStr = userNameTF.text;
        }
        NSLog(@"new str is %@",newStr);
        [_params setObject:[NSString stringWithFormat:@"%@",_emailTF.text] forKey:@"email"];
         [_params setObject:[NSString stringWithFormat:@"%@",newStr] forKey:@"username"];
      
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
            NSString* FileParamConstant = @"pic";
            NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"update_profile"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:Url];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // add image data
            NSData *imageData = UIImageJPEGRepresentation(commonUtils.userImage, 0.5);
            if (imageData) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:imageData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  //NSLog(@"Server Response %@",response);
                                                  NSLog(@"dictionary %@",dictionary);
                                           
                                                  
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  if([statusis isEqualToString:@"success"]){
                                                      NSDictionary *successDic = [dictionary valueForKey:@"successData"];
                                                      
                                                      
                                                      
                                                    //  NSLog(@"Old Data ====> %@", commonUtils.userData);
                                                      
                                                      commonUtils.userData = successDic;
                                                      [self updateEditProfile];
                                                       [self updateMyProfileScreen];
                                                      
                                                    //NSLog(@"New Data ====> %@", commonUtils.userData);
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^(void){
                                                          //Run UI Updates
                                                          [commonUtils hideHud];
                                                          [self alertForGoingBack:@"Profile Updated Successfully"];
                                                          menuVC.userName.text = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
                                                          userNameTF.text = menuVC.userName.text;
                                                          myprofileVc.headerView.userNameLbl.text = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
                                                          myprofileVc.fullNameLbl.text = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
                                                          if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
                                                              myprofileVc.headerView.userDescLbl.text =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
                                                          }
                                                          else {
                                                              myprofileVc.headerView.userDescLbl.text = @"";
                                                          }
                                                        email = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"email"]];
                                                            [self updateSideMenu];
                                                          [currentTextField resignFirstResponder];
                                                          isProfileImageChanged = NO;
                                                          
                                                      });
                                                   
                                                  
//                                                       [self updateMyProfileScreen];
//                                                        [self updateEditProfile];
                                                     
                                                     
                                                  
                                                      
                                                    
                                                      
                                                       //   [self initUI];
                                                          
                                                    
                                                     
                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^(void){
                                                          [commonUtils hideHud];
                                                          [commonUtils showAlert:@"Error!" withMessage:[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"errorMessage"]]];
                                                      });
                                                    
                                                     // [self updateProfile];
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  dispatch_async(dispatch_get_main_queue(), ^(void){
                                                      [commonUtils hideHud];
                                                     
                                                  });
                                                  NSLog(@"no data returned");
                   
                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                  dispatch_async(dispatch_get_main_queue(), ^(void){
                                                      [commonUtils hideHud];
                                                      
                                                  });
                                                  NSLog(@"Server not responding");
                                                  //couldn't download
                                                  
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
        
        
    }
    
    
}
- (IBAction)updateProfileBtn:(id)sender {
    [currentTextField resignFirstResponder];
    NSString *previoususername = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
    NSString *previousfullname =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
    NSString *previousiceBreaker;
    NSString *preiouvsEmail =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"email"]];
    NSString *previousPhone;
    
    
    
    
    
    
    
    if (![[commonUtils.userData valueForKey:@"phone"] isKindOfClass:[NSNull class]]&&[commonUtils.userData valueForKey:@"phone"]!=nil) {
        previousPhone  =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"phone"]];
    }
    else {
        previousPhone = @"";
    }
    if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]]&&[commonUtils.userData valueForKey:@"ice_breaker"]!=nil) {
        previousiceBreaker  =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
    }
    else {
        previousiceBreaker = @"";
    }
    
    
    
    
    
    
    
    NSLog(@"profile image changes %d",(int)isProfileImageChanged);
    if (isProfileImageChanged) {
        NSLog(@"profile image changed");
    }
    
    if (![userNameTF.text isEqualToString:previoususername]) {
        NSLog(@"User name is change");
    }
    
    
    
    if (![_profileNameTF.text isEqualToString:previousfullname]) {
        NSLog(@"=%@=",_profileNameTF.text);
        NSLog(@"=%@=",previousfullname);
        NSLog(@"profile name changed");
    }
    
    
    if (![_iceBreakerTv.text isEqualToString:previousiceBreaker]) {
        NSLog(@"=%@=",_iceBreakerTv.text);
        NSLog(@"=%@=",previousiceBreaker);
        NSLog(@"ICE breaker text change");
    }
    
    
    if (![_emailTF.text isEqualToString:email]) {
        NSLog(@"Email is change");
    }
    
    
    
    if (![_phoneTF.text isEqualToString:previousPhone]) {
        NSLog(@"Phone no change");
    }
    
    if (![[userNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previoususername]||![[_profileNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previousfullname]||![[_iceBreakerTv.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previousiceBreaker]||![[_emailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] isEqualToString:preiouvsEmail]||![_phoneTF.text isEqualToString:previousPhone]||isProfileImageChanged) {
        
        if (self.userNameTF.text.length>0 && self.emailTF.text.length>0 && _profileNameTF.text.length>0 && (_phoneTF.text.length==14 || _phoneTF.text.length == 0)) {
            if ([commonUtils validateEmail:self.emailTF.text]) {
                
                
                NSMutableDictionary *userDataMutable = [commonUtils.userData mutableCopy];
                if (self.profileNameTF.text.length>0) {
                    [userDataMutable setObject:self.profileNameTF.text forKey:@"first_name"];
                }
                else {
                    [userDataMutable setObject:@"" forKey:@"first_name"];
                }
                if (self.phoneTF.text.length>0) {
                    [userDataMutable setObject:self.phoneTF.text forKey:@"phone"];
                }
                else {
                    [userDataMutable setObject:@"" forKey:@"phone"];
                }
                if (_iceBreakerTv.text.length>0) {
                    [userDataMutable setObject:_iceBreakerTv.text forKey:@"ice_breaker"];
                }
                else {
                    [userDataMutable setObject:@"" forKey:@"ice_breaker"];
                }
                
                
                
                commonUtils.userImage =_userProfileIV.image;
                CGImageRef cgref = [commonUtils.userImage CGImage];
                CIImage *cim = [commonUtils.userImage CIImage];
                if (cim == nil && cgref == NULL)
                {
                    NSLog(@"no underlying data");
                    commonUtils.userImage = [UIImage imageNamed:@"user-avatar"];
                }
                else {
                    commonUtils.userImage = _userProfileIV.image;
                }
                
                //   [userDataMutable setObject:_emailTF.text forKey:@"email"];
                // [userDataMutable setObject:userNameTF.text forKey:@"username"];
                
                //        commonUtils.userData = userDataMutable;
                
                
                //  NSLog(@"Commutils data is %@",commonUtils.userData);
                // [self updateMyProfileScreen];
                //  [self updateEditProfile];
                // [self updateSideMenu];
                
                
                [self updateProfile:userDataMutable];
            }
            else {
                [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Email is not valid" type:ViewAlertPanic];
                _emailView.layer.borderColor = [UIColor redColor].CGColor;
                _emailView.layer.borderWidth = 1.0;
              
            }
        }
        else if(userNameTF.text.length == 0){
              [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Enter User Name" type:ViewAlertPanic];
            _userNameView.layer.borderColor = [UIColor redColor].CGColor;
            _userNameView.layer.borderWidth = 1.0;
        
        }
        else if(_emailTF.text.length == 0){
              [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Enter Email" type:ViewAlertPanic];
            _emailView.layer.borderColor = [UIColor redColor].CGColor;
            _emailView.layer.borderWidth = 1.0;
   
        }
        else if(_profileNameTF.text.length == 0){
              [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Enter Profile Name" type:ViewAlertPanic];
            _profileNameView.layer.borderColor = [UIColor redColor].CGColor;
            _profileNameView.layer.borderWidth = 1.0;
      
        }
        else if (_phoneTF.text.length<14){
              [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Enter Valid Phone Number" type:ViewAlertPanic];
            _phoneView.layer.borderColor = [UIColor redColor].CGColor;
            _phoneView.layer.borderWidth = 1.0;
         
        }
        else {
            NSLog(@"phone length is %lu",_phoneTF.text.length);
            [commonUtils showAlert:@"Error!" withMessage:@"Please Enter Valid Information"];
        }

    }
    
    else {
     //   NSLog(@"%@",isProfileImageChanged);
        [currentTextField resignFirstResponder];
        [self.navigationController popViewControllerAnimated:YES];
    }
   }
-(void)updateSideMenu {
  
   
  

    
   
    menuVC.userImage.image = commonUtils.userImage;
        
    CGSize sizeOfImage = CGSizeMake(50, 50);
    menuVC.userImage.image = [self imageByCroppingImage:menuVC.userImage.image toSize:sizeOfImage];
    
   // menuVC.userName.text = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"username"]];
    
    menuVC.userFN.text= [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
   
  
    
}
-(void)updateMyProfileScreen


    {
       

       
        
        

            myprofileVc.headerView.userProfileIv.image = commonUtils.userImage;
            
            
        
        
        
  
        
    }

-(void)updateEditProfile{

        profileImage = commonUtils.userImage;
      
        
        
    
    if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
       iceBreaker=  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
    }
    else {
        iceBreaker = @"";
    }
   profileName  = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
    phone = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"phone"]];
    

    
}
-(void)cancelNumberPad{
    [_phoneTF resignFirstResponder];
    //_phoneTF.text = @"";
}

-(void)doneWithNumberPad{
  
    [_phoneTF resignFirstResponder];
}
-(void)showAlertView {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Any unsaved changes will be lost" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == PushbackAlert) {
        if (buttonIndex == 0) {
            [currentTextField resignFirstResponder];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else {
        if (buttonIndex == 1)
            
        {
            
           
            
                          
         
            
            _userProfileIV.image = menuVC.userImage.image;
            if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
                _iceBreakerTv.text=  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
            }
            else {
                _iceBreakerTv.text = @"";
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                _profileNameTF.text  = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
               // NSString *phonenumber = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"phone"]];
                if (![[commonUtils.userData valueForKey:@"phone"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"phone"] != nil) {
                    _phoneTF.text = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"phone"]];
                }
                else{
                    _phoneTF.text =@"";
                }
                userNameTF.text = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
                _emailTF.text = [commonUtils.userData valueForKey:@"email"];
                [currentTextField resignFirstResponder];
                [self.navigationController popViewControllerAnimated:YES];
            
            });
            
           
         
            //Code for OK button
        }
   
    }
    
}

- (IBAction)backBtn:(id)sender {
    dispatch_async (dispatch_get_main_queue(), ^
                    {
                        [currentTextField resignFirstResponder];    
                    });
   
    NSString *previoususername = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
    NSString *previousfullname =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
    NSString *previousiceBreaker;
    NSString *preiouvsEmail =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"email"]];
    NSString *previousPhone;
    
    
    
    
    
    
    
    if (![[commonUtils.userData valueForKey:@"phone"] isKindOfClass:[NSNull class]]&&[commonUtils.userData valueForKey:@"phone"]!=nil) {
         previousPhone  =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"phone"]];
    }
    else {
        previousPhone = @"";
    }
    if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]]&&[commonUtils.userData valueForKey:@"ice_breaker"]!=nil) {
        previousiceBreaker  =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
    }
    else {
        previousiceBreaker = @"";
    }

   
   
  

    

     NSLog(@"profile image changes %d",(int)isProfileImageChanged);
        if (isProfileImageChanged) {
            NSLog(@"profile image changed");
        }
    
        if (![userNameTF.text isEqualToString:previoususername]) {
             NSLog(@"User name is change");
        }
    
    
    
        if (![_profileNameTF.text isEqualToString:previousfullname]) {
            NSLog(@"=%@=",_profileNameTF.text);
            NSLog(@"=%@=",previousfullname);
             NSLog(@"profile name changed");
        }
    
    
        if (![_iceBreakerTv.text isEqualToString:previousiceBreaker]) {
            NSLog(@"=%@=",_iceBreakerTv.text);
             NSLog(@"=%@=",previousiceBreaker);
             NSLog(@"ICE breaker text change");
        }
    
    
        if (![_emailTF.text isEqualToString:email]) {
            NSLog(@"Email is change");
        }
    
    
    
        if (![_phoneTF.text isEqualToString:previousPhone]) {
            NSLog(@"Phone no change");
        }
    
    if (![[userNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previoususername]||![[_profileNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previousfullname]||![[_iceBreakerTv.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previousiceBreaker]||![[_emailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] isEqualToString:preiouvsEmail]||![_phoneTF.text isEqualToString:previousPhone]||isProfileImageChanged) {
        [currentTextField resignFirstResponder];
        [self performSelector:@selector(showAlertView) withObject:nil afterDelay:0.6];
       // [self alertForInformationLost];
    }
    
    else {
        [currentTextField resignFirstResponder];
     
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)changePswdBtn:(id)sender {
    changePswdVC *Vc = [self.storyboard instantiateViewControllerWithIdentifier:@"changedPswdVC"];
    [self.navigationController pushViewController:Vc animated:YES];
}
-(void)alertForGoingBack:(NSString*)message{
    PushbackAlert = [[UIAlertView alloc]initWithTitle:@"Success" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [PushbackAlert show];
    
}
#pragma Phonenumber formatting
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _phoneTF) {
        
    
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    // if it's the phone number textfield format it.
    
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    
    
  return YES;
    }
    else {
        return YES;
    }
}
-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}
- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}
-(void)enablingUpdateProfileBtn{
    NSString *previoususername = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
    NSString *previousfullname =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
    NSString *previousiceBreaker;
    NSString *preiouvsEmail =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"email"]];
    NSString *previousPhone;
    
    
    
    
    
    
    
    if (![[commonUtils.userData valueForKey:@"phone"] isKindOfClass:[NSNull class]]&&[commonUtils.userData valueForKey:@"phone"]!=nil) {
        previousPhone  =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"phone"]];
    }
    else {
        previousPhone = @"";
    }
    if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]]&&[commonUtils.userData valueForKey:@"ice_breaker"]!=nil) {
        previousiceBreaker  =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
    }
    else {
        previousiceBreaker = @"";
    }
    
    
    
    
    
    
    
    NSLog(@"profile image changes %d",(int)isProfileImageChanged);
    if (isProfileImageChanged) {
        NSLog(@"profile image changed");
    }
    
    if (![userNameTF.text isEqualToString:previoususername]) {
        NSLog(@"User name is change");
    }
    
    
    
    if (![_profileNameTF.text isEqualToString:previousfullname]) {
        NSLog(@"=%@=",_profileNameTF.text);
        NSLog(@"=%@=",previousfullname);
        NSLog(@"profile name changed");
    }
    
    
    if (![_iceBreakerTv.text isEqualToString:previousiceBreaker]) {
        NSLog(@"=%@=",_iceBreakerTv.text);
        NSLog(@"=%@=",previousiceBreaker);
        NSLog(@"ICE breaker text change");
    }
    
    
    if (![_emailTF.text isEqualToString:email]) {
        NSLog(@"Email is change");
    }
    
    
    
    if (![_phoneTF.text isEqualToString:previousPhone]) {
        NSLog(@"Phone no change");
    }
    
    if (![[userNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previoususername]||![[_profileNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previousfullname]||![[_iceBreakerTv.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:previousiceBreaker]||![[_emailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] isEqualToString:preiouvsEmail]||![_phoneTF.text isEqualToString:previousPhone]||isProfileImageChanged) {
        _updateProfileOutlet.enabled = YES;
        _profileCheckIV.alpha = 1.0;
        
    }
}
-(void)resetAllBorderColors{

    _userNameView.layer.borderColor = [UIColor clearColor].CGColor;
    _userNameView.layer.borderWidth = 1.0;
    _emailView.layer.borderColor = [UIColor clearColor].CGColor;
    _emailView.layer.borderWidth = 1.0;
    _profileNameView.layer.borderColor = [UIColor clearColor].CGColor;
    _profileNameView.layer.borderWidth = 1.0;
    _phoneView.layer.borderColor = [UIColor clearColor].CGColor;
    _phoneView.layer.borderWidth = 1.0;
}
@end
