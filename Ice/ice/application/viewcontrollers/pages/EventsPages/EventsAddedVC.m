//
//  EventsAddedVC.m
//  ICE
//
//  Created by LandToSky on 12/28/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "EventsAddedVC.h"
#import "EventsAddedTVCell.h"
#import "EventsPrivateTVCell.h"

#import "UserProfileVC.h"
#import "DetailVC.h"

@interface EventsAddedVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *mainTV;
    NSMutableArray *addedIces;
    BOOL isPageRefresing;
    int currentPagingNumber;
}


@end

@implementation EventsAddedVC
@synthesize userID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) initData
{
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    currentPagingNumber = 0;
    [self getAddedIced:currentPagingNumber];
}
#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return addedIces.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventsAddedTVCell *cell = (EventsAddedTVCell*) [tableView dequeueReusableCellWithIdentifier:@"EventsAddedTVCell"];
    if (cell == nil) {
        cell = [[EventsAddedTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EventsAddedTVCell"];
    }
    cell.eventTitleLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"title"]];
    cell.addressLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"location"]];
    cell.eventDayLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_day"]];
    cell.eventDateLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_day_start"]];
    cell.eventMnthLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_month"]];
    cell.timeLbl.text = [NSString stringWithFormat:@"%@-%@ PST",addedIces[indexPath.row][@"event_start_time"],addedIces[indexPath.row][@"event_end_time"]];
    cell.userName.text = [NSString stringWithFormat:@"via %@",addedIces[indexPath.row][@"get_user"][@"first_name"]];
    cell.userProfileBtn.tag = indexPath.row;
    [cell.userProfileBtn addTarget:self action:@selector(onUserProfilePage:) forControlEvents:UIControlEventTouchUpInside];
    cell.eventDetailBtn.tag = indexPath.row;
    [cell.eventDetailBtn addTarget:self action:@selector(onEventDetailPage:) forControlEvents:UIControlEventTouchUpInside];
    NSString *imageUrl = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"get_user"][@"photo"]];
    BOOL isLive = [addedIces[indexPath.row][@"is_live"]boolValue];
    if (isLive) {
        cell.liveLbl.hidden = NO;
    }
    else{
        cell.liveLbl.hidden = YES;
    }

    [cell.userIV sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                    placeholderImage:[UIImage imageNamed:@"user_avatar"]
                             options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    return cell;
}

- (void) onUserProfilePage:(UIButton *)sender
{
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = [NSString stringWithFormat:@"%@",addedIces[sender.tag][@"get_user"][@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onEventDetailPage:(UIButton*)sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.selectedTag = sender.tag;
    vc.comingFrom = @"events";
    vc.eventType = @"events";
    commonUtils.activitiesArray = addedIces;
    vc.iceDetails = addedIces[sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)getAddedIced:(int)currentPage{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        NSString *timezone = [commonUtils getTimeZone];
           NSString *skip = [NSString stringWithFormat:@"%d",currentPage];
     NSString *urlString = [NSString stringWithFormat:@"http://139.162.37.73/iceapp/api/v1/get_activities/%@?time_zone=%@&skip=%@",commonUtils.commutilUserID,timezone,skip];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    NSMutableDictionary *successDic = [[NSMutableDictionary alloc]init];
                    successDic = json[@"successData"];
                 
                      [self didFinishRecordsRequest:successDic[@"added_activities"] forPage:currentPagingNumber];
                    
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(mainTV.contentOffset.y >= (mainTV.contentSize.height - mainTV.bounds.size.height)) {
        
        //NSLog(@" scroll to bottom!");
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            isPageRefresing = YES;
            if (currentPagingNumber<(commonUtils.userTotalAdded/12)) {
                currentPagingNumber = currentPagingNumber +1;
                [self getAddedIced:currentPagingNumber];
            }
            
        }
    }
    
}
-(void)didFinishRecordsRequest:(NSArray *)results forPage:(NSInteger)pageNo{
    if(pageNo == 0){
        addedIces = [results mutableCopy];
    }
    else{
        [addedIces addObjectsFromArray:results];
    }
    isPageRefresing = NO;
    [mainTV reloadData];
}

@end
