//
//  EventsIcedVC.m
//  ICE
//
//  Created by LandToSky on 12/28/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "EventsIcedVC.h"
#import "EventsIcedTVCell.h"
#import "EventsPrivateTVCell.h"
#import "DetailVC.h"

@interface EventsIcedVC () <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *mainTV;
    NSMutableArray *icedActiviteis;
    int currentPageNumber;
    BOOL isPageRefresing;
}

@end

@implementation EventsIcedVC
@synthesize userID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) initData
{
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    currentPageNumber = 0;
    [self getIcedActitvities:currentPageNumber];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
  
}
#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
    int retValue = 0;
    if(icedActiviteis != nil){
        retValue = [icedActiviteis count];
    }
    return retValue;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventsIcedTVCell *cell = (EventsIcedTVCell*) [tableView dequeueReusableCellWithIdentifier:@"EventsIcedTVCell"];
    if (cell == nil) {
        cell = [[EventsIcedTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EventsIcedTVCell"];
    }
    cell.eventTitleLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"title"]];
    cell.addressLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"location"]];
    cell.eventDayLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_day"]];
    cell.eventDateLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_day_start"]];
    cell.eventMnthLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_month"]];
    cell.timeLbl.text = [NSString stringWithFormat:@"%@-%@ PST",icedActiviteis[indexPath.row][@"event_start_time"],icedActiviteis[indexPath.row][@"event_end_time"]];
    BOOL isLive = [icedActiviteis[indexPath.row][@"is_live"]boolValue];
    if (isLive) {
        cell.liveLbl.hidden = NO;
    }
    else{
        cell.liveLbl.hidden = YES;
    }
    cell.eventDetailBtn.tag = indexPath.row;
    [cell.eventDetailBtn addTarget:self action:@selector(onEventDetailPage:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(mainTV.contentOffset.y >= (mainTV.contentSize.height - mainTV.bounds.size.height)) {
        
        //NSLog(@" scroll to bottom!");
       if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            isPageRefresing = YES;
           if (currentPageNumber<(commonUtils.userTotalIced/12)) {
               currentPageNumber = currentPageNumber +1;
               [self getIcedActitvities:currentPageNumber];
           }
          
        }
    }
    
}
-(void)didFinishRecordsRequest:(NSArray *)results forPage:(NSInteger)pageNo{
    if(pageNo == 0){
        icedActiviteis = [results mutableCopy];
    }
    else{
        [icedActiviteis addObjectsFromArray:results];
    }
    isPageRefresing = NO;
    [mainTV reloadData];
}
- (void)onEventDetailPage:(UIButton*)sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.selectedTag = sender.tag;
    vc.comingFrom = @"events";
    vc.eventType = @"events";
    commonUtils.activitiesArray = icedActiviteis;
    vc.iceDetails = icedActiviteis[sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)getIcedActitvities:(int)currentPage{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        [commonUtils showHud:self.view];
        NSString *skip = [NSString stringWithFormat:@"%d",currentPage];
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"http://139.162.37.73/iceapp/api/v1/get_activities/%@?time_zone=%@&skip=%@",commonUtils.commutilUserID,timezone,skip];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                 
                    NSMutableDictionary *successDic = [[NSMutableDictionary alloc]init];
                    successDic = json[@"successData"];
//                    icedActiviteis = successDic[@"iced_activities"];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [commonUtils hideHud];
                        [self didFinishRecordsRequest:successDic[@"iced_activities"] forPage:currentPageNumber];
                       // [mainTV reloadData];
                    });
                
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
    }
    
}


@end
