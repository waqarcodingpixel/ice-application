//
//  ICEVC.m
//  ICE
//
//  Created by LandToSky on 1/12/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "ICEVC.h"

#import "EventImageCVCell.h"
#import "UIViewController+KNSemiModal.h"
#import "ZFTokenField.h"

#import "CustomIOSAlertView.h"
#import "SaveGroupAlertView.h"
#import "IcedDoneAlertView.h"
#import "PickerViewController.h"
#import "HomeVC.h"
#import "InviteFriendTVCell.h"
#import "InviteGroupsTVCell.h"
#import "locationPickerViewController.h"
#import "NSDate+Compare.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>
#import "imagesCell.h"
#import <Social/Social.h>
#import <IQKeyboardManager.h>
#import <SafariServices/SafariServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "GMImagePickerController.h"
#import "AppDelegate.h"
@interface ICEVC ()<UICollectionViewDelegate, UICollectionViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate,ZFTokenFieldDataSource, ZFTokenFieldDelegate, PickerViewControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource,FBSDKSharingDelegate,UIGestureRecognizerDelegate,GMImagePickerControllerDelegate,UIAlertViewDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *contentView;
    UITextField *currentTextField;
    
    // Top View
    BOOL isPrivate;
    IBOutlet UITextField *iceTitleTxt;
    IBOutlet UIImageView *iceCheckImg;
    IBOutlet UIButton *iceCheckBtn;
    IBOutlet UIImageView *imgViewMenu;
    
    // Event Photo & Video CollectionView
    IBOutlet UICollectionView *photoCv;
    NSMutableArray *eventDatas;
    
    // Starts and Ends Date Label;
    IBOutlet UILabel *startsDateLbl;
    IBOutlet UILabel *startsTimeLbl;
    IBOutlet UILabel *endsDateLbl;
    IBOutlet UILabel *endsTimeLbl;
    
    // Add More Photo
    BOOL isEditing, noCamera, isProfileImageChanged;
    UIImage *changedImage;
    IBOutlet UIView *addImageView;
    IBOutlet UITextField *inviteFriendTxt;
    IBOutlet UITableView *inviteFriendsTV;
    
    
    // Invite Friend Tag List View
    IBOutlet UIView *inviteFriendsView;
    CGFloat increaseInviteTokenViewH;
    IBOutlet ZFTokenField *tokenField;
    NSMutableArray *tokens;
    BOOL isGuestCanInvite,addApiCalled;

    
    // Detail
    IBOutlet UIView *detailView;
    BOOL isDetailEditing,isStartDate,isEndDate;
    IBOutlet UIView *detailContainerView;
    CGFloat keyboardHeight;
    IBOutlet UITextView *detailTxt;
    NSMutableArray *imagesArray;
    // Reminder
    IBOutlet UIView *reminderView;
    
    // Save Group As AlertView
    CustomIOSAlertView *saveGroupAlertView;
    
    // Iced Done Alert View
    CustomIOSAlertView *icedDoneAlertView;
    NSMutableArray *usersArray,*usersMutableCopy,*userGroupsMutableCopy;
   
    NSString *pickedAddress;
    NSString *userID;
    NSDateFormatter *dateformatter;
    NSString *iceStartTime ,*iceEndTime;
    CLLocationCoordinate2D *emptyCord;
    BOOL m_postingInProgress;
    NSMutableArray *btncheckedArray,*imagesArrayCopy,*invitedGroups,*userGroups,*inviteGroupIDs,*inviteUserIDs,*videosArrayData;
    UIImage *imageToUpload;
    SaveGroupAlertView *saveGroupAlert;
   
    BOOL isUsersShown;
    
    
}
@property (nonatomic, strong) IBOutlet UIView *photoPickContainerView;

@end

@implementation ICEVC
@synthesize pickerElements,hourPicker;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
    self.pendingAlertViews = [NSMutableArray new];
}

- (void)initUI
{
    
    [self.view bringSubviewToFront:inviteFriendsView];
    [scrollView setContentSize:contentView.frame.size];
    [commonUtils setRoundedRectBorderView:[self.view viewWithTag:101] withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#f8f8f8" alpha:1.0f] withBorderRadius:0.0f];
    [commonUtils setRoundedRectBorderView:[self.view viewWithTag:102] withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#f8f8f8" alpha:1.0f] withBorderRadius:0.0f];
    [commonUtils setRoundedRectBorderView:[self.view viewWithTag:103] withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#f8f8f8" alpha:1.0f] withBorderRadius:0.0f];
    [commonUtils setRoundedRectBorderButton:[self.view viewWithTag:104] withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#c6a6dd" alpha:1.0f] withBorderRadius:0.0f];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    
    iceTitleTxt.delegate = self;
    
    tokenField.dataSource = self;
    tokenField.delegate = self;
    tokenField.textField.enabled = NO;
    _houtMintTF.delegate = self;
    inviteFriendTxt.userInteractionEnabled = NO;
    inviteFriendTxt.delegate = self;
    _reminderTF.delegate = self;
    _imageToChooseView.userInteractionEnabled = YES;
    _imgCollectionView.userInteractionEnabled = YES;
    [inviteFriendsTV setHidden:YES];
    inviteFriendsTV.delegate = self;
    inviteFriendsTV.dataSource = self;
    
    [detailTxt setEditable:NO];
    detailTxt.delegate = self;
    
   // [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];
    
    increaseInviteTokenViewH = 0.0f;
    imagesArray = [[NSMutableArray alloc]init];
    usersArray = [[NSMutableArray alloc]init];
    videosArrayData = [NSMutableArray new];
    usersMutableCopy=userGroupsMutableCopy = [[NSMutableArray alloc]init];
    [inviteFriendTxt addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
    _imgCollectionView.pagingEnabled = YES;
    
    imagesArrayCopy = [[NSMutableArray alloc]init];
    pickerElements = @[@"minutes", @"hours"];
    self.hourPicker = [[UIPickerView alloc] init];
    self.hourPicker.delegate = self;
    self.hourPicker.dataSource = self;
    self.hourPicker.showsSelectionIndicator = YES;
    _houtMintTF.inputView = self.hourPicker;
    
    
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    if (isDetailEditing)
    {
        float y = detailContainerView.frame.origin.y  - ( SCREEN_HEIGHT - keyboardHeight - detailContainerView.frame.size.height - 75);
       // [scrollView setContentOffset:CGPointMake(0, y) animated:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
   
   // _imgCollectionView.backgroundColor = [UIColor redColor];
   
}

- (void)initData
{
    isPrivate = NO;
    isGuestCanInvite = YES;
    
    isEditing = NO;
    noCamera = NO;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        noCamera = YES;
    }
    isProfileImageChanged = NO;
    changedImage = nil;
    
    
 
//    tokens =[@[@"George Shephard", @"James Fletcher", @"Anna Davis"] mutableCopy];
  
    
    isDetailEditing = NO;
    //  [self getUsers];
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _reminderTF.inputAccessoryView = numberToolbar;
    invitedGroups = [[NSMutableArray alloc]init];
    userGroups = [[NSMutableArray alloc]init];
    inviteUserIDs = [[NSMutableArray alloc]init];
    inviteGroupIDs = [[NSMutableArray alloc]init];
    addApiCalled = NO;
     [self getUsers];
    [self getUserGroups];
    
}

- (void)viewWillAppear:(BOOL)animated
{
  
   if (self.isshowMenu) {
       imgViewMenu.image = [UIImage imageNamed:@"arrow-left-black"];
   }else {
       imgViewMenu.image = [UIImage imageNamed:@"menu-icon"];
   }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   
    [self.view endEditing:YES];
    
}

#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == _imgCollectionView) {
        return   imagesArrayCopy.count;
    }
    else {
        if (imagesArray.count>0) {
            return imagesArray.count;
            
        }
        else {
            return 0;
        }

    }
    
    
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _imgCollectionView) {
        imagesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imgToChooseCell" forIndexPath:indexPath];
        cell.imgBtn.tag = indexPath.row;
    
     //    [cell.imgBtn addTarget:self action:@selector(shareToFB:) forControlEvents:UIControlEventTouchUpInside];
   
        
     
        
             cell.imageToChoose.image = imagesArrayCopy[indexPath.row];
      
       
        return cell;
    }
    else {
        EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
        NSLog(@"images array is %d",(int)imagesArray.count);
        
        NSString *strValue = (NSString *)imagesArray[indexPath.row][@"IsImage"] ;
        if ([strValue isEqualToString:@"image"]) {
            cell.eventIv.image = imagesArray[indexPath.row][@"image"];
            cell.videoPlayBtn.hidden = YES;
        }
        else{
          //  NSLog(@"images array is %@",imagesArray);
           // NSLog(@"index is %@",imagesArray[indexPath.row][@"image"]);
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if ([imagesArray[indexPath.row][@"pickedType"]isEqualToString:@"galery"]) {
                    
                
                 NSURL *assetURL = [NSURL fileURLWithPath:imagesArray[indexPath.row][@"image"]];
                 AVURLAsset *asset = [AVURLAsset assetWithURL:assetURL];
           //
          
                
                
           //
           
           
            //AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[indexPath.row] options:nil];
                
                
            AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
            generate.appliesPreferredTrackTransform = YES;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 60);
            CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
            
            UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
            cell.eventIv.image = img;
            cell.videoPlayBtn.hidden = NO;
                }
                else{
                    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[indexPath.row][@"image"] options:nil];
                    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                    generate.appliesPreferredTrackTransform = YES;
                    NSError *err = NULL;
                    CMTime time = CMTimeMake(1, 60);
                    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
                    
                    UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
                    cell.eventIv.image = img;
                    cell.videoPlayBtn.hidden = NO;

                }
             });

        }
        cell.closeBtn.tag = indexPath.row;
        cell.videoPlayBtn.tag = indexPath.row;
        [cell.closeBtn addTarget:self action:@selector(removeImage:) forControlEvents:UIControlEventTouchUpInside];
        [cell.videoPlayBtn addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        //[cell.eventIv setImage:[UIImage imageNamed:[NSString stringWithFormat:@"image%lu", indexPath.item %5]]];
        
        return cell;
    }
  

    
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"did select");
    if (collectionView == _imgCollectionView) {
        imageToUpload = imagesArrayCopy[indexPath.row];
        _imageToChooseView.hidden = YES;

        
        // build an activity view controller
    
           }
   
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2; // This is the minimum inter item spacing, can be more
}

#pragma mark Button Action
- (IBAction)onPrivatePublic:(id)sender
{
    
    isPrivate = !isPrivate;
    UILabel *lbl = (UILabel*)[self.view viewWithTag:1001];
    UIImageView *imageView = (UIImageView*) [self.view viewWithTag:1002];
    if (isPrivate) {
        [lbl setText:@"PRIVATE"];
        [imageView setImage:[UIImage imageNamed:@"private"]];
    } else {
        [lbl setText:@"PUBLIC"];
        [imageView setImage:[UIImage imageNamed:@"public"]];
    }
  
}

- (IBAction)onInviteFriendAdd:(id)sender
{
    if (isUsersShown) {
        isUsersShown = NO;
        [self.view endEditing:YES];
        
        [inviteFriendTxt setText:@""];
        inviteFriendTxt.userInteractionEnabled = NO;
        [inviteFriendsTV setHidden:YES];
        
        //After added new token, adjust frame
        [self adjustFrameAfterAddingInviteFriend];
    }
    else{
        isUsersShown = YES;
        inviteFriendTxt.userInteractionEnabled = YES;
        [inviteFriendTxt becomeFirstResponder];
        [scrollView setContentOffset:CGPointMake(0, 348) animated:YES];
    }
 
}

- (IBAction)onGuestCanInvite:(id)sender
{
    isGuestCanInvite = !isGuestCanInvite;
    UILabel *lbl = (UILabel*)[self.view viewWithTag:1003];
    UIImageView *imageView = (UIImageView*) [self.view viewWithTag:1004];
    if (isGuestCanInvite) {
        [lbl setText:@"Guest can invite friends"];
        [imageView setImage:[UIImage imageNamed:@"guest-invite-on"]];
    } else {
        [lbl setText:@"Guest can't invite friends"];
        [imageView setImage:[UIImage imageNamed:@"guest-invite-off"]];
    }
}


- (IBAction)onLocation:(id)sender
{
//    NSString *mapURLStr = [NSString stringWithFormat: @"http://maps.apple.com/?q=%@",@"Limmattalstrasse 170, 8049 Zürich"];
//    
//    mapURLStr = [mapURLStr stringByReplacingOccurrencesOfString:@" " withString:@"+"];
//    NSURL *url = [NSURL URLWithString:[mapURLStr stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
//    if ([[UIApplication sharedApplication] canOpenURL:url]){
//        [[UIApplication sharedApplication] openURL:url];
//    }
    locationPickerViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"locPicVc"];
    [self.navigationController pushViewController:vc animated:YES];
    
}


#pragma mark - Invite Friend Token View Delegate
- (void)tokenDeleteButtonPressed:(UIButton *)tokenButton
{
    //Land
    //    NSUInteger index = [tokenField indexOfTokenView:tokenButton.superview];
    NSUInteger index = tokenButton.tag;
    if (index != NSNotFound) {
        if (tokens[index][@"name"]) {
            for (int i=0; i<inviteGroupIDs.count; i++) {
                if ([NSString stringWithFormat:@"%@",tokens[index][@"id"]]==[NSString stringWithFormat:@"%@",inviteGroupIDs[i]]) {
                    [inviteGroupIDs removeObjectAtIndex:i];
                }
            }
        }
        else {
            for (int i=0; i<inviteUserIDs.count; i++) {
                if ([NSString stringWithFormat:@"%@",tokens[index][@"id"]]==[NSString stringWithFormat:@"%@",inviteUserIDs[i]]) {
                    [inviteUserIDs removeObjectAtIndex:i];
                }
            }
        }
  
        [tokens removeObjectAtIndex:index];
        [tokenField reloadData];
        [self adjustFrameAfterAddingInviteFriend];
    }
}

- (CGFloat)lineHeightForTokenInField:(ZFTokenField *)tokenField
{
    return 25;
}

- (NSUInteger)numberOfTokenInField:(ZFTokenField *)tokenField
{
    return tokens.count;
}

- (UIView *)tokenField:(ZFTokenField *)tokenField viewForTokenAtIndex:(NSUInteger)index
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TokenView" owner:nil options:nil];
    UIView *view = nibContents[0];
    UIImageView *imageView = (UIImageView*)[view viewWithTag:1];
    UILabel *label = (UILabel *)[view viewWithTag:2];
    UIButton *button = (UIButton *)[view viewWithTag:3];
    if ([tokens[index]valueForKey:@"photo"]) {
     NSString *imageUrlString = [NSString stringWithFormat:@"%@",[tokens[index]valueForKey:@"photo"]];
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrlString]
                     placeholderImage:[UIImage imageNamed:@"user-avatar"]
                              options:SDWebImageCacheMemoryOnly];
    }
    else {
        imageView.image = [UIImage imageNamed:@"invite-groups"];
    }
    if ([tokens[index] valueForKey:@"first_name"]) {
         label.text = [tokens[index] valueForKey:@"first_name"];
    }
    else {
        label.text =  label.text = [tokens[index] valueForKey:@"name"];
    }
    
    
    
    
   
    
    button.tag = index;
    [button addTarget:self action:@selector(tokenDeleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
   
    //    CGSize size = [label sizeThatFits:CGSizeMake(1000, 25)];
    CGFloat width = [self widthForText:label.text font:[UIFont systemFontOfSize:13] withHeight:25.0f];
    view.frame = CGRectMake(0, 0, width + 60, 25);
    return view;
}

#pragma mark - ZFTokenField Delegate

- (CGFloat)tokenMarginInTokenInField:(ZFTokenField *)tokenField
{
    return 5;
}

- (void)tokenField:(ZFTokenField *)tokenField1 didReturnWithText:(NSString *)text
{
    [tokens addObject:text];
    [tokenField1 reloadData];
}

- (void)tokenField:(ZFTokenField *)tokenField didRemoveTokenAtIndex:(NSUInteger)index
{
    [tokens removeObjectAtIndex:index];
}

- (BOOL)tokenFieldShouldEndEditing:(ZFTokenField *)textField
{
    return NO;
}

- (CGFloat)widthForText:(NSString*)text font:(UIFont*)font withHeight:(CGFloat)height {
    
    CGSize constraint = CGSizeMake(2000.0f, height);
    CGSize size;
    
    CGSize boundingBox = [text boundingRectWithSize:constraint
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.width;
}

#pragma mark - Invite Friend TableView Delegata
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return usersArray.count;
    else if (section == 1)
        return userGroups.count;
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0)
        return @"Section 1";
    else
        return @"Section 2";
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    [view setBackgroundColor:[UIColor colorWithHex:@"#f9f6fc" alpha:1.0f]];
    
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, tableView.frame.size.width-5, 15)];
    [label setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    [label setTextColor:[UIColor colorWithHex:@"#c6a6dd" alpha:1.0f]];
    
    NSString *sectionTitleStr;
    if (section == 0) {
        sectionTitleStr = @"PEOPLE:";
    } else {
        sectionTitleStr = @"GROUPS:";
    }
    
    [label setText:sectionTitleStr];
    [view addSubview:label];
    return view;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        InviteFriendTVCell *cell0 = (InviteFriendTVCell*) [tableView dequeueReusableCellWithIdentifier:@"InviteFriendTVCell"];
        
        cell0.selectFriendBtn.tag = indexPath.row;
        cell0.inviteFriendNameLbl.text = [usersArray[indexPath.row]valueForKey:@"first_name"];
       
       
            NSString *imageUrlString = [NSString stringWithFormat:@"%@",[usersArray[indexPath.row]valueForKey:@"photo"]];
   
            
        
     
            
               [cell0.inviteFriendImage sd_setImageWithURL:[NSURL URLWithString:imageUrlString]
                            placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                     options:SDWebImageCacheMemoryOnly];
               
    
            
        [cell0.selectFriendBtn addTarget:self action:@selector(onSelectInviteFriend:) forControlEvents:UIControlEventTouchUpInside];
        cell = cell0;
    } else if (indexPath.section == 1){
        InviteGroupsTVCell* cell1 = (InviteGroupsTVCell*) [tableView dequeueReusableCellWithIdentifier:@"InviteGroupsTVCell"];
        cell1.groupName.text = userGroups[indexPath.row][@"name"];
        cell1.selectGroupBtn.tag = indexPath.row;
        cell1.groupCount.text = [NSString stringWithFormat:@"%@",userGroups[indexPath.row][@"group_count_count"]];
        [cell1.selectGroupBtn addTarget:self action:@selector(onSelectInviteGroup:) forControlEvents:UIControlEventTouchUpInside];
        cell = cell1;
    }
    
    return cell;
}


- (void)onSelectInviteFriend:(UIButton*)sender
{
    [inviteUserIDs addObject:usersArray[sender.tag][@"id"]];
    [tokens addObject:usersArray[sender.tag]];
    [tokenField reloadData];
    
    [self.view endEditing:YES];
    [inviteFriendTxt setText:@""];
    inviteFriendTxt.userInteractionEnabled = NO;
    [inviteFriendsTV setHidden:YES];
    
     //After added new token, adjust frame
    [self adjustFrameAfterAddingInviteFriend];
}

- (void)onSelectInviteGroup:(UIButton*)sender
{
    [inviteGroupIDs addObject:userGroups[sender.tag][@"id"]];
    [tokens addObject:userGroups[sender.tag]];
    [tokenField reloadData];
    
    [self.view endEditing:YES];
    [inviteFriendTxt setText:@""];
    inviteFriendTxt.userInteractionEnabled = NO;
    [inviteFriendsTV setHidden:YES];
    
    //After added new token, adjust frame
    [self adjustFrameAfterAddingInviteFriend];
}


- (void)adjustFrameAfterAddingInviteFriend
{
    //After added new token, adjust frame
    increaseInviteTokenViewH = tokenField.frame.size.height - 62.0f;
    // - increase InviteFriendView Height
    [commonUtils resizeFrame:inviteFriendsView withWidth:SCREEN_WIDTH withHeight:(tokenField.frame.size.height + 133.0f)];
    
    // - move DetailView
    [commonUtils moveView:detailView withMoveX:0.0f withMoveY:543.0f + increaseInviteTokenViewH];
    
    // - move ReminderView
    [commonUtils moveView:reminderView withMoveX:0.0f withMoveY:718.0f + increaseInviteTokenViewH];
    
    // - resize ContentView
    [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:849.0f + increaseInviteTokenViewH];
    
    [scrollView setContentSize:contentView.frame.size];
    
    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
   [scrollView setContentOffset:bottomOffset animated:YES];
    
}

#pragma mark - Profile Photo Change
- (IBAction)onProfilePhotoChange:(id)sender {
    if(self.isLoadingBase) return;
    
    if(isEditing) {
        [currentTextField resignFirstResponder];
        isEditing = NO;
    }
    [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                    KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                    KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                    KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                    }];
}
- (IBAction)onClickGallery:(id)sender {
    if(self.isLoadingBase) return;
    
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.navigationBar.tintColor = [UIColor blackColor];
//    
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
//    [self presentViewController:picker animated:YES completion:NULL];
    
    
    
    
    
    
    
   
}
- (IBAction)onClickCamera:(id)sender {
    if(self.isLoadingBase) return;
    
    if(noCamera) {
        [commonUtils showVAlertSimple:@"Warning" body:@"Your device has no camera" duration:1.0f];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
     picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    changedImage = info[UIImagePickerControllerEditedImage];

    if (changedImage) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:changedImage,@"image",@"image",@"IsImage", nil];
        [imagesArray addObject:dic];


        [photoCv reloadData];
    
        isProfileImageChanged = YES;
        [self dismissSemiModalView];
        

    }
    else{
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSData *data = [NSData dataWithContentsOfURL:videoURL];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:videoURL forKey:@"image"];
        [dic setObject:data forKey:@"videodata"];
        [dic setValue:@"video" forKey:@"IsImage"];
        [dic setValue:@"camera" forKey:@"pickedType"];
        [imagesArray addObject:dic];

        [photoCv reloadData];
   
        [self dismissSemiModalView];

    }
    [picker dismissViewControllerAnimated:NO completion:^{
        
        
    }];

        [UIView animateWithDuration:1.0f animations:^{
            [commonUtils resizeFrame:addImageView withWidth:87.0f withHeight:addImageView.frame.size.height];
            [commonUtils moveView:addImageView withMoveX:SCREEN_WIDTH - 87 withMoveY:addImageView.frame.origin.y
             ];
        }];
  
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        
        /* ScrollView Inset Error
        [commonUtils moveView:self.sidePanelController.centerPanelContainer
                    withMoveX:0
                    withMoveY:0];
        
        [commonUtils resizeFrame:self.sidePanelController.centerPanelContainer
                       withWidth:SCREEN_WIDTH
                      withHeight:SCREEN_HEIGHT];
         */
    }];

    [self dismissSemiModalView];
}

#pragma mark - AlertView - Save Group As
- (IBAction)onSaveGroup:(id)sender
{
    if (inviteUserIDs.count>1) {
        saveGroupAlertView = [[CustomIOSAlertView alloc] init];
        if (IS_IPHONE_5) {
            saveGroupAlertView.frame = CGRectMake(saveGroupAlertView.frame.origin.x, saveGroupAlertView.frame.origin.y, 280, saveGroupAlertView.frame.size.height);
        }
        // Add some custom content to the alert view
        [saveGroupAlertView setContainerView:[self createSaveGroupCustomView]];
    
        [saveGroupAlertView setUseMotionEffects:true];
        [saveGroupAlertView setCloseOnTouchUpOutside:YES];
        [saveGroupAlertView show];
         NSLog(@"width is %f",saveGroupAlertView.frame.size.width);

    }
    else{
        [commonUtils showAlert:@"Error!" withMessage:@"Please Add More Members To Create Group"];
    }
    // Here we need to pass a full frame
   }

- (UIView*) createSaveGroupCustomView{
     saveGroupAlert= [[[NSBundle mainBundle] loadNibNamed:@"SaveGroupAlertView" owner:self options:nil] objectAtIndex:0];

    [saveGroupAlert.cancelBtn addTarget:self action:@selector(onSaveGroupCancel:) forControlEvents:UIControlEventTouchUpInside];
    [saveGroupAlert.saveBtn addTarget:self action:@selector(onSaveGroupSaveBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    return saveGroupAlert;
}

- (void)onSaveGroupCancel:(UIButton *)sender
{
    NSLog(@"Tap Cancel");
        [saveGroupAlertView close];
}

- (void)onSaveGroupSaveBtn:(UIButton *)sender
{
    NSLog(@"Tap Save");
    if (saveGroupAlert.groupNameTxt.text.length>0) {
         [saveGroupAlertView close];
        [self saveGroup:saveGroupAlert.groupNameTxt.text];
    }
    else{
        [commonUtils showAlert:@"Warning!" withMessage:@"Please Provide Group Name"];
    }

   
}

#pragma mark - AlertView - Iced Done
- (IBAction)onIced:(id)sender
{
    if (!addApiCalled) {
        
    
     if (CLLocationCoordinate2DIsValid(commonUtils.pickedLocationCord)&&!(commonUtils.pickedLocationCord.latitude==0)&&!(commonUtils.pickedLocationCord.longitude==0)) {
     
    userID = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"id"]];
    if (startsDateLbl.text.length>0&&endsDateLbl.text.length>0&&imagesArray.count>0&&userID.length>0&&iceTitleTxt.text.length>0&&detailTxt.text.length>0&&tokens.count>0&&_reminderTF.text.length>0) {
        NSString *startTimeDateString = [NSString stringWithFormat:@"%@ %@",startsDateLbl.text,startsTimeLbl.text];
        NSString *endTimeDateString =  [NSString stringWithFormat:@"%@ %@",endsDateLbl.text,endsTimeLbl.text];
        NSDateFormatter *dateFormat = [NSDateFormatter new];
        [dateFormat setDateFormat:@"MMM d, yyyy h:mm a"];
        
        NSDate *mydate = [dateFormat dateFromString:startTimeDateString];
        //NSLog(@"mydate %@ " ,mydate);
        
        
        [dateFormat setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        
       // NSLog(@"mydate %@ " ,[dateFormat stringFromDate:mydate]);
       iceStartTime = [dateFormat stringFromDate:mydate];
        [dateFormat setDateFormat:@"MMM d, yyyy h:mm a"];
        mydate = [dateFormat dateFromString:endTimeDateString];
       [dateFormat setDateFormat:@"yyyy-M-dd HH:mm:ss"];
 
       iceEndTime = [dateFormat stringFromDate:mydate];
    
         
         
         
         
         
        
        
        
        
        [self getAddressFromGoogle];
        
   
        
    }
    else if(iceTitleTxt.text.length == 0){
        [commonUtils showAlert:@"Error" withMessage:@"Please give title of ICE"];
    }
    else if (startsDateLbl.text.length==0)  {
        [commonUtils showAlert:@"Error" withMessage:@"Please select ICE start time"];
    }
    else if (endsDateLbl.text.length==0){
         [commonUtils showAlert:@"Error" withMessage:@"Please select ICE end time"];
    }
    else if (imagesArray.count==0){
        [commonUtils showAlert:@"Error" withMessage:@"Please add images of ICE"];
    }
    else if(tokens.count ==0){
        [commonUtils showAlert:@"Error" withMessage:@"Please add people to invite"];
    }
    else if(detailTxt.text.length==0){
         [commonUtils showAlert:@"Error" withMessage:@"Please give description for ICE"];
    }
 
    else if (_reminderTF.text.length==0){
         [commonUtils showAlert:@"Error" withMessage:@"Please add reminder time for ICE"];
    }
         }
     else{
         [commonUtils showAlert:@"Error!" withMessage:@"Please select location first"];
     }
        
    }
}

- (UIView*) createIcedDoneCustomView{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AppDelegate.sharedAppDelegate.customAlertView = [[[NSBundle mainBundle] loadNibNamed:@"IcedDoneAlertView" owner:self options:nil] objectAtIndex:0];
    
    appDelegate.imagesArrayCopy = imagesArrayCopy;
   
    [appDelegate.customAlertView.doneBtn addTarget:appDelegate action:@selector(iceDone:) forControlEvents:UIControlEventTouchUpInside];
//    [AppDelegate.sharedAppDelegate.customAlertView.doneBtn addTarget:AppDelegate.sharedAppDelegate action:@selector() forControlEvents:UIControlEventTouchUpInside];
  
    return AppDelegate.sharedAppDelegate.customAlertView;
}

- (void)onIcedDone:(UIButton *)sender
{
    [AppDelegate.sharedAppDelegate.iceDoneAlert close];

    
  //  NSLog(@"images array copy is %@",imagesArrayCopy);
    NSMutableArray *imagesArrayCopy2 = [NSMutableArray new];

    for (int i= 0; i<imagesArrayCopy.count; i++) {
        if ([imagesArrayCopy[i][@"image"]isKindOfClass:[UIImage class]]) {
           [imagesArrayCopy2 addObject:imagesArrayCopy[i][@"image"]];
        }
        else{
            NSData *postData = imagesArrayCopy[i][@"videodata"];
           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSLog(@"paths is %@",paths);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"myMove%d.mp4",i+1]];
            
            [postData writeToFile:path atomically:YES];
            NSURL *moveUrl = [NSURL fileURLWithPath:path];
            [imagesArrayCopy2 addObject:moveUrl];
            NSLog(@"images array copy 2 is %@",imagesArrayCopy2);
        }
     
    }
    if ([commonUtils.sharingOptions[@"facebook"]isEqualToString:@"yes"]) {
        UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:imagesArrayCopy2 applicationActivities:nil];
        controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            // When completed flag is YES, user performed specific activity
            NSLog(@"pending alerts is %@",_pendingAlertViews);
         if (self.pendingAlertViews.count > 0) {
             CustomIOSAlertView *alert = self.pendingAlertViews.firstObject;
             [self.pendingAlertViews removeObjectAtIndex:0];
             self.visibleAlertView = alert;
             [alert show];
         
         }
         else{
             self.visibleAlertView = nil;
//             HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
//             [homeVC onShowTabView:2];
//             [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
//             [self.sideMenuController hideLeftViewAnimated:sender];
//             NSLog(@"%@", commonUtils.sharingOptions);
         }
            

        };
        // and present it
  
        [self presentViewController:controller animated:YES completion:^{
            // executes after the user selects something
        }];
        NSLog(@"share fb");
    }
    else{
//        HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
//        [homeVC onShowTabView:2];
//        [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
//        [self.sideMenuController hideLeftViewAnimated:sender];

    }
    

}

#pragma mark - onStarts
- (IBAction)onStartsCalendar:(id)sender
{    isStartDate = YES;
    PickerViewController *pickerViewController = [[PickerViewController alloc] initFromNib];
    
    pickerViewController.delegate = self;
    [pickerViewController setPageIndex:StartsCalendar];
    [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];


}

- (IBAction)onEndsCalendar:(id)sender
{
    isEndDate = YES;
    PickerViewController *pickerViewController = [[PickerViewController alloc] initFromNib];
    pickerViewController.delegate = self;
    [pickerViewController setPageIndex:EndsCalendar];
    [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];
}

- (void)didSelectStartsMonthDayYear:(NSString *)startsMonthDayYearStr
                  withStartsHourMin:(NSString*)startsHourMinStr
               withEndsMonthDayYear:(NSString*)endsMonthDayYearStr
                    withEndsHourMin:(NSString*)endsHourMinStr
                     withStartsDate:(NSDate*)startstDate
                       withEndsDate:(NSDate*) endsDate
{
    if (isStartDate) {
          [startsDateLbl setText:startsMonthDayYearStr];
         [startsTimeLbl setText:startsHourMinStr];
        isStartDate=NO;
    }
   
   

    
    // Check StartDate isLater than EndsDate
    if ([startstDate isLaterThan:endsDate ]) { // add Strike to EndDateLbl
        [iceCheckBtn setUserInteractionEnabled:NO];
        [iceCheckImg setAlpha:0.4f];
        
        [endsDateLbl setAttributedText:[commonUtils strikeString:endsMonthDayYearStr]];
        [endsTimeLbl setAttributedText:[commonUtils strikeString:endsHourMinStr]];
        
    } else {
        [iceCheckBtn setUserInteractionEnabled:YES];
        [iceCheckImg setAlpha:1.0f];
        if (isEndDate) {
            isEndDate = NO;
            [endsDateLbl setText:endsMonthDayYearStr];
            [endsTimeLbl setText:endsHourMinStr];
        }
     
    }
    
}

- (IBAction)onDetailEdit:(id)sender
{
    
    [detailTxt setEditable:YES];
    [detailTxt becomeFirstResponder];
}

#pragma mark - TextFiled Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    currentTextField = textField;
    if(self.isLoadingBase) return NO;
    if (textField == inviteFriendTxt) {
        
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == inviteFriendTxt && textField.text.length>0) {
        
        [inviteFriendsTV setHidden:NO];
    }
    
    if (textField == iceTitleTxt) {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 24;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(self.isLoadingBase) return NO;
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == iceTitleTxt) return;
    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
 //   [scrollView setContentOffset:bottomOffset animated:YES];
}


#pragma mark - TextView Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(self.isLoadingBase) return NO;
    isDetailEditing = YES;
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if(self.isLoadingBase) return NO;
    
    if ([text isEqualToString:@"\n"]) {
        
        isEditing = NO;
        [textView setEditable:NO];
        isDetailEditing = NO;
        return [textView resignFirstResponder];
    }
    
    return YES;
}
-(void)textFieldDidChange:(id)sender{
    if ([inviteFriendTxt.text length]>0) {
        [usersArray removeAllObjects];
        [userGroups removeAllObjects];
        for (int i=0; i<usersMutableCopy.count; i++) {
            // NSRange *range = ;
            NSString *object = [usersMutableCopy[i]valueForKey:@"first_name"];
            
            if([object rangeOfString:inviteFriendTxt.text options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                [usersArray addObject:usersMutableCopy[i]];
                
            }
        }
        for (int i=0; i<userGroupsMutableCopy.count; i++) {
            // NSRange *range = ;
            NSString *object = [userGroupsMutableCopy[i]valueForKey:@"name"];
            
            if([object rangeOfString:inviteFriendTxt.text options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                [userGroups addObject:userGroupsMutableCopy[i]];
                
            }
        }

        
        
        
        [inviteFriendsTV reloadData];
        
    }
    else {
        usersArray=userGroups = [[NSMutableArray alloc]init];
        usersArray = [usersMutableCopy mutableCopy];
        userGroups = [userGroupsMutableCopy mutableCopy];
        [inviteFriendsTV reloadData];
        
    }

}
- (void)textViewDidEndEditing:(UITextView *)textView{
    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
   // [scrollView setContentOffset:bottomOffset animated:YES];
}

#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;

    
    CGPoint point = [sender locationInView:self.view];
    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
    if ([viewTouched isKindOfClass:[inviteFriendsTV class]]) {
          // Do nothing;
    }
  
    else {
         // respond to touch action
        [self.view endEditing:YES];
        [inviteFriendTxt setText:@""];
        inviteFriendTxt.userInteractionEnabled = NO;
        [inviteFriendsTV setHidden:YES];
        
    }
    
}

-(void)addIce:(NSMutableArray*)dataArray{
    
    addApiCalled = YES;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        NSString *reminderString = _reminderTF.text;
        if ([_houtMintTF.text isEqualToString:@"hours"]) {
            int reminderTime = [reminderString integerValue];
            reminderTime = reminderTime*60;
            reminderString = [NSString stringWithFormat:@"%d",reminderTime];
        }
    //    NSLog(@"reminder string %@",reminderString);
    NSNumber *isPrivateNumber = [NSNumber numberWithBool:!isPrivate];
        NSString *isprivateString = [NSString stringWithFormat:@"%@",isPrivateNumber];
      
    NSNumber *guestCanInvite = [NSNumber numberWithBool:isGuestCanInvite];
    if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
//       NSArray *invitedarray = @[@6,@7];
      
        NSError *error;
        NSData *invitedGroupData =  [NSJSONSerialization dataWithJSONObject:inviteGroupIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *invitedGroupString = [[NSString alloc] initWithData:invitedGroupData encoding:NSUTF8StringEncoding];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inviteUserIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
          
//        [commonUtils showHud:self.view];
    
            NSString *currentLatitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.latitude];
            NSString *currentLongtitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.longitude];

            NSString *deviceId = @"abcdefghijklmnopqrstuvwxyzabcdefghi";
            NSString *address =  [commonUtils getUserDefault:@"location"];
            
            NSString *timeZoneOffset = [commonUtils getTimeZone];
            NSLog(@"device id is %@ address is %@ timezone seconds are %@",deviceId,address,timeZoneOffset);
            
            // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
            
            //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
            
        [_params setObject:iceStartTime forKey:@"start_date"];
            [_params setObject:iceEndTime forKey:@"end_date"];
            [_params setObject:userID forKey:@"user_id"];
            [_params setObject:iceTitleTxt.text forKey:@"title"];
            [_params setObject:detailTxt.text forKey:@"description"];
            [_params setObject:pickedAddress forKey:@"location"];
            [_params setObject:currentLatitude forKey:@"lat"];
             [_params setObject:invitedGroupString forKey:@"group_ids"];
            [_params setObject:currentLongtitude forKey:@"lng"];
            [_params setObject:isprivateString forKey:@"is_public"];
            [_params setObject:guestCanInvite forKey:@"can_invite"];
        [_params setObject:jsonString forKey:@"invited_ids"];
        [_params setObject:timeZoneOffset forKey:@"timezone"];
         [_params setObject:reminderString forKey:@"reminder_time"];
          [self clearAddIceData];
        
        NSLog(@"param ===> %@",_params);
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    
            NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"add_ice"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:Url];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
             [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // add image data
        for (int i=0; i<dataArray.count; i++) {
            
            
            
            
            NSString *strValue = (NSString *)dataArray[i][@"IsImage"] ;
     

            if ([strValue isEqualToString:@"image"]) {
                
            
            NSString *fileName = [NSString stringWithFormat:@"images[%d]",i];
            NSData *imageData = UIImageJPEGRepresentation(dataArray[i][@"image"], 0.5);
            if (imageData) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:imageData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            }
           
                }
            else{
               // NSURL *assetURL = [NSURL fileURLWithPath:imagesArray[i]];
                NSData *postData = dataArray[i][@"videodata"];
                NSString *videoFileName = [NSString stringWithFormat:@"videos[%d]",i];
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"file.mov\"\r\n", videoFileName] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: video/mov\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:postData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
             
                   UIImage *img = [self getThumbnailFromArray:dataArray index:i];
               // NSLog(@"image is %@",img);
                NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
                                NSString *thumbnailName = [NSString stringWithFormat:@"thumbs[%d]",i];
                                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", thumbnailName] dataUsingEncoding:NSUTF8StringEncoding]];
                                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                                [body appendData:imageData];
                                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];

                

                

                
            }
        }

            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  //NSLog(@"Server Response %@",response);
                                                  NSString* myString;
                                                  myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                                 // NSLog(@"string is %@",myString);
                                                  NSLog(@"dictionary %@",dictionary);
                                                  NSString *message = [dictionary valueForKey:@"errorMessage"];
                                                 
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  if([statusis isEqualToString:@"success"]){
                                                 
                                                      NSString *successMessage = [dictionary valueForKey:@"successMessage"];
                                                  
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [commonUtils hideHud];
                                                             imagesArrayCopy = dataArray;
                                                          // Here we need to pass a full frame
                                                        AppDelegate.sharedAppDelegate.iceDoneAlert   = [[CustomIOSAlertView alloc] init];
                                                      
                                                          // Add some custom content to the alert view
                                                          [AppDelegate.sharedAppDelegate.iceDoneAlert setContainerView:[self createIcedDoneCustomView]];
                                                          
                                                          [AppDelegate.sharedAppDelegate.iceDoneAlert setUseMotionEffects:true];
                                                          [AppDelegate.sharedAppDelegate.iceDoneAlert setCloseOnTouchUpOutside:YES];
                                                          if (AppDelegate.sharedAppDelegate.visibleAlertView) {
                                                              [commonUtils.pendingAlerts addObject:AppDelegate.sharedAppDelegate.iceDoneAlert];
                                                              
                                                          }
                                                          else{
                                                             AppDelegate.sharedAppDelegate.visibleAlertView = AppDelegate.sharedAppDelegate.iceDoneAlert;
                                                              [AppDelegate.sharedAppDelegate.iceDoneAlert show];
                                                              
                                                          }
                                                          
                                                         
                                                     
                                                    
                                                 
                                                      
                                                    
                                                
                                                       //   [commonUtils showAlert:@"Success" withMessage:successMessage];
                                                          
                                                          
                                                          
                                                      });
                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [commonUtils hideHud];
                                                          addApiCalled = NO;
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                      });
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  NSLog(@"no data returned");
                                                  [commonUtils hideHud];
                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                
                                                  [commonUtils hideHud];
                                                  NSLog(@"%@", err.localizedDescription);
                                                  //couldn't download
                                                  
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
        
              });
        
    }
    
    
    
    
    else {
        [commonUtils showAlert:@"Error!" withMessage:@"Unable To Get Your Location.Please Allow The App To Get Your Location"];
    }
    }
    
}
-(void)getUsers{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://139.162.37.73/iceapp/api/v1/get_users"]];
    
    // Create a mutable copy of the immutable request and add more headers
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
    NSLog(@"%@", request.allHTTPHeaderFields);
  
    
    NSURLResponse *response;
    
    NSError *error = nil;
    
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(error!=nil)
    {
        NSLog(@"web service error:%@",error);
    }
    else
    {
        if(receivedData !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:receivedData
                                 options:kNilOptions
                                 error:&Jerror];
         //   NSLog(@"user data is %@",json);
            if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                usersArray = [[json valueForKey:@"successData"] mutableCopy];
                tokens = [NSMutableArray array];
                [tokenField reloadData];
                usersMutableCopy = usersArray.mutableCopy;
            }
            if(Jerror!=nil)
            {
               // NSLog(@"json error:%@",Jerror);
            }
        }
    }
    }
    
}
#pragma Play video func
-(void)playVideo:(UIButton*)sender{
    NSURL *assetURL;
    if ([imagesArray[sender.tag][@"pickedType"]isEqualToString:@"galery"]) {
        assetURL =  [NSURL fileURLWithPath:imagesArray[sender.tag][@"image"]];
    }
    else{
        assetURL = imagesArray[sender.tag][@"image"];
    }
    NSLog(@"url is %@",assetURL);
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:assetURL];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];

}
#pragma Remove Image from CollectionView
- (void) removeImage:(UIButton *) sender {
    [imagesArray removeObjectAtIndex:sender.tag];
    [photoCv reloadData];
    [_imgCollectionView reloadData];
    NSLog(@"Tag : %ld", (long)sender.tag);
    
}

-(void)cancelNumberPad{
    [_reminderTF resignFirstResponder];
    //_phoneTF.text = @"";
}

-(void)doneWithNumberPad{
    
    [_reminderTF resignFirstResponder];
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Clicked! %d, %d", (int)buttonIndex, (int)[alertView tag]);
   
}
- (IBAction)cancelImageUploadBtn:(id)sender {
    _imageToChooseView.hidden = YES;
}

-(void)getUserGroups {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://139.162.37.73/iceapp/api/v1/get_groups"]];
    
    // Create a mutable copy of the immutable request and add more headers
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
    NSLog(@"%@", request.allHTTPHeaderFields);
    
    
    NSURLResponse *response;
    
    NSError *error = nil;
    
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(error!=nil)
    {
        NSLog(@"web service error:%@",error);
    }
    else
    {
        if(receivedData !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:receivedData
                                 options:kNilOptions
                                 error:&Jerror];
           // NSLog(@"user data is %@",json);
            if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                userGroups = [json[@"successData"]mutableCopy];
                userGroupsMutableCopy = [userGroups mutableCopy];
                [inviteFriendsTV reloadData];
                //[tokenField reloadData];
            }
            if(Jerror!=nil)
            {
                NSLog(@"json error:%@",Jerror);
            }
        }
    }
    
}
-(void)saveGroup:(NSString *)groupName{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }

    else {
        NSError *error;
        NSData *invitedGroupData =  [NSJSONSerialization dataWithJSONObject:inviteGroupIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *invitedGroupString = [[NSString alloc] initWithData:invitedGroupData encoding:NSUTF8StringEncoding];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inviteUserIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [commonUtils showHud:self.view];
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];

        [_params setObject:invitedGroupString forKey:@"group_ids"];

        [_params setObject:jsonString forKey:@"user_ids"];
        [_params setObject:groupName forKey:@"name"];
    
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
     
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"create_group"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
   
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              // NSLog(@"string is %@",myString);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if([statusis isEqualToString:@"success"]){
                                                  
                                                  NSString *successMessage = [dictionary valueForKey:@"successMessage"];
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [commonUtils hideHud];
                                                      [commonUtils showAlert:@"Success" withMessage:successMessage];
                                                      [self getUserGroups];
                                                      // Here we need to pass a full frame
                                                      
                                                      
                                                      
                                                      
                                                  });
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [commonUtils hideHud];
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              [commonUtils hideHud];
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              [commonUtils hideHud];
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
        
    }


    
#pragma picker methods
#pragma mark - UIPickerViewDataSource

// #3
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView == hourPicker) {
        return 1;
    }
    
    return 0;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == hourPicker) {
        return [pickerElements count];
    }
    
    return 0;
}

#pragma mark - UIPickerViewDelegate

// #5
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView == hourPicker) {
        return pickerElements[row];
    }
    
    return nil;
}

// #6
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView == hourPicker) {
        _houtMintTF.text = pickerElements[row];
    }
}


- (IBAction)onMenuShowforChild:(id)sender{
    if (self.isshowMenu) {
        [self.navigationController popViewControllerAnimated:true];
    }else {
        [self.sideMenuController showLeftViewAnimated:sender];
    }
}


- (IBAction)pickerBtn:(id)sender {
   
        [_houtMintTF becomeFirstResponder];
    
}
- (NSData *)generatePostDataForData:(NSData *)uploadData
{
    // Generate the post header:
    NSString *post = [NSString stringWithCString:"--AaB03x\r\nContent-Disposition: form-data; name=\"upload[file]\"; filename=\"somefile\"\r\nContent-Type: video/mov\r\nContent-Transfer-Encoding: binary\r\n\r\n" encoding:NSASCIIStringEncoding];
    
    // Get the post header int ASCII format:
    NSData *postHeaderData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    // Generate the mutable data variable:
    NSMutableData *postData = [[NSMutableData alloc] initWithLength:[postHeaderData length] ];
    [postData setData:postHeaderData];
    
    // Add the image:
    [postData appendData: uploadData];
    
    // Add the closing boundry:
    [postData appendData: [@"\r\n--AaB03x--" dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    
    // Return the post data:
    return postData;
}
-(void)uploadVideo{
    NSString *reminderString = _reminderTF.text;
    if ([_houtMintTF.text isEqualToString:@"hours"]) {
        int reminderTime = [reminderString integerValue];
        reminderTime = reminderTime*60;
        reminderString = [NSString stringWithFormat:@"%d",reminderTime];
    }
    //    NSLog(@"reminder string %@",reminderString);
    NSNumber *isPrivateNumber = [NSNumber numberWithBool:!isPrivate];
    NSString *isprivateString = [NSString stringWithFormat:@"%@",isPrivateNumber];
    
    NSNumber *guestCanInvite = [NSNumber numberWithBool:isGuestCanInvite];
    if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
        //       NSArray *invitedarray = @[@6,@7];
        
        NSError *error;
        NSData *invitedGroupData =  [NSJSONSerialization dataWithJSONObject:inviteGroupIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *invitedGroupString = [[NSString alloc] initWithData:invitedGroupData encoding:NSUTF8StringEncoding];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inviteUserIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [commonUtils showHud:self.view];
        
        NSString *currentLatitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.latitude];
        NSString *currentLongtitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.longitude];
        
        NSString *deviceId = @"abcdefghijklmnopqrstuvwxyzabcdefghi";
        NSString *address =  [commonUtils getUserDefault:@"location"];
        
        NSString *timeZoneOffset = [commonUtils getTimeZone];
        NSLog(@"device id is %@ address is %@ timezone seconds are %@",deviceId,address,timeZoneOffset);

    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    
    [_params setObject:iceStartTime forKey:@"start_date"];
    [_params setObject:iceEndTime forKey:@"end_date"];
    [_params setObject:userID forKey:@"user_id"];
    [_params setObject:iceTitleTxt.text forKey:@"title"];
    [_params setObject:detailTxt.text forKey:@"description"];
    [_params setObject:pickedAddress forKey:@"location"];
    [_params setObject:currentLatitude forKey:@"lat"];
    [_params setObject:invitedGroupString forKey:@"group_ids"];
    [_params setObject:currentLongtitude forKey:@"lng"];
    [_params setObject:isprivateString forKey:@"is_public"];
    [_params setObject:guestCanInvite forKey:@"can_invite"];
    [_params setObject:jsonString forKey:@"invited_ids"];
    [_params setObject:timeZoneOffset forKey:@"timezone"];
    [_params setObject:reminderString forKey:@"reminder_time"];
}
}


- (IBAction)launchGMImagePicker:(id)sender
{
    
    [self dismissSemiModalView];
      if(self.isLoadingBase) return;
    GMImagePickerController *picker = [[GMImagePickerController alloc] init];
    picker.delegate = self;
    picker.title = @"";
  
    picker.customDoneButtonTitle = @"Done";
    picker.customCancelButtonTitle = @"Cancel";
    picker.customNavigationBarPrompt = @"Select Photo or Video";
   
    picker.colsInPortrait = 3;
    picker.colsInLandscape = 5;
    picker.minimumInteritemSpacing = 2.0;
    
    //    picker.allowsMultipleSelection = NO;
    //    picker.confirmSingleSelection = YES;
    //    picker.confirmSingleSelectionPrompt = @"Do you want to select the image you have chosen?";
    
    //    picker.showCameraButton = YES;
    //    picker.autoSelectCameraImages = YES;
    
    picker.modalPresentationStyle = UIModalPresentationPopover;
    
    //    picker.mediaTypes = @[@(PHAssetMediaTypeImage)];
    
    //    picker.pickerBackgroundColor = [UIColor blackColor];
    //    picker.pickerTextColor = [UIColor whiteColor];
    //    picker.toolbarBarTintColor = [UIColor darkGrayColor];
    //    picker.toolbarTextColor = [UIColor whiteColor];
    //    picker.toolbarTintColor = [UIColor redColor];
    //    picker.navigationBarBackgroundColor = [UIColor blackColor];
    //    picker.navigationBarTextColor = [UIColor whiteColor];
    //    picker.navigationBarTintColor = [UIColor redColor];
    //    picker.pickerFontName = @"Verdana";
    //    picker.pickerBoldFontName = @"Verdana-Bold";
    //    picker.pickerFontNormalSize = 14.f;
    //    picker.pickerFontHeaderSize = 17.0f;
    //    picker.pickerStatusBarStyle = UIStatusBarStyleLightContent;
    //    picker.useCustomFontForNavigationBar = YES;
    
    UIPopoverPresentationController *popPC = picker.popoverPresentationController;
    popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;
    //    popPC.sourceView = _gmImagePickerButton;
    //    popPC.sourceRect = _gmImagePickerButton.bounds;
    //    popPC.backgroundColor = [UIColor blackColor];
    
    //[self showViewController:picker sender:nil];
    [self presentViewController:picker animated:YES completion:NULL];
}



#pragma mark - GMImagePickerControllerDelegate

- (void)assetsPickerController:(GMImagePickerController *)picker didFinishPickingAssets:(NSArray *)assetArray
{
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    //NSLog(@"assets array is %@",assetArray);
    //    PHAsset *asset = assetArray[0];
    //    NSLog(@"asset type is %ld",(long)asset.mediaType);
    //    NSLog(@"assets array first object %@",asset);
    //    NSLog(@"GMImagePicker: User ended picking assets. Number of selected items is: %lu", (unsigned long)assetArray.count);
    
    PHImageManager *manager = [PHImageManager defaultManager];
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[assetArray count]];
    
    // assets contains PHAsset objects.
    __block UIImage *ima;
    
    for (PHAsset *asset in assetArray) {
        // Do something with the asset
        if (asset.mediaType == 1) {
            [manager requestImageForAsset:asset
                               targetSize:PHImageManagerMaximumSize
                              contentMode:PHImageContentModeDefault
                                  options:nil
                            resultHandler:^void(UIImage *image, NSDictionary *info) {
                                //   NSLog(@"info is %@",info);
                                ima = image;
                                NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:ima,@"image",@"image",@"IsImage", nil];
                                [imagesArray addObject:dic];
                                [self reloadCollectionViewAndChangeLayout];
                            }];
            
        }
        else if(asset.mediaType == 2){
            [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset *avAsset, AVAudioMix *audioMix, NSDictionary *info) {
                NSURL *url = (NSURL *)[[(AVURLAsset *)avAsset URL] fileReferenceURL];
                             
                NSLog(@"url is %@",url);
                NSLog(@"url = %@", [url absoluteString]);
                NSLog(@"url = %@", [url relativePath]);
                AVURLAsset* myAsset = (AVURLAsset*)avAsset;
                NSData * data = [NSData dataWithContentsOfFile:myAsset.URL.relativePath];
                if (data) {
                 //   NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[url relativePath],@"image",data,@"videodata",@"video",@"IsImage", nil];
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    [dic setObject:[url relativePath] forKey:@"image"];
                     [dic setObject:data forKey:@"videodata"];
                     [dic setValue:@"video" forKey:@"IsImage"];
                    [dic setValue:@"galery" forKey:@"pickedType"];
                    [dic setObject:url forKey:@"playurl"];
                    [imagesArray addObject:dic];
                  //  NSLog(@"data here is %lu",(unsigned long)imagesArray.count);
                }
//                                [imagesArray addObject:[url relativePath] ];
                  [self reloadCollectionViewAndChangeLayout];
            }];
            [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset* avasset, AVAudioMix* audioMix, NSDictionary* info){
                AVURLAsset* myAsset = (AVURLAsset*)avasset;
                NSData * data = [NSData dataWithContentsOfFile:myAsset.URL.relativePath];
                if (data) {
                  
                }
            }];
            
        }
        
        
    }
 //   NSLog(@"images array is %@",images);
    
}

//Optional implementation:
-(void)assetsPickerControllerDidCancel:(GMImagePickerController *)picker
{
 [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"GMImagePicker: User pressed cancel button");
}
-(void)reloadCollectionViewAndChangeLayout{
    dispatch_async(dispatch_get_main_queue(), ^(void){
 
    [photoCv reloadData];
    [UIView animateWithDuration:1.0f animations:^{
        [commonUtils resizeFrame:addImageView withWidth:87.0f withHeight:addImageView.frame.size.height];
        [commonUtils moveView:addImageView withMoveX:SCREEN_WIDTH - 87 withMoveY:addImageView.frame.origin.y
         ];
    }];
           });

}
-(void)getAddressFromGoogle{
      [currentTextField resignFirstResponder];
    //NSLog(@"App id is %@",AppUtility.deviceId);
    NSString * authenticate_url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true/false",commonUtils.pickedLocationCord.latitude,commonUtils.pickedLocationCord.longitude];
    NSLog(@"url string : %@",authenticate_url);
    // NSLog(@"client id here is %@",_clienti);
    NSData * webData = [NSData dataWithContentsOfURL:[NSURL URLWithString:authenticate_url]];
    NSLog(@"come here check2");
    if(webData == nil)
    {
        NSLog(@"No data Found");
        pickedAddress = @"";
        
        return;
    }
    else
    {
        
        NSString * responseString = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
        NSLog(@"response string %@",responseString);
        NSError * error;
        NSDictionary *json =
        [NSJSONSerialization JSONObjectWithData: [responseString dataUsingEncoding:NSUTF8StringEncoding]
                                        options: NSJSONReadingMutableContainers
                                          error: &error];
        NSLog(@"%@",json);
        NSString *formattedAddress = [NSString stringWithFormat:@"%@",json[@"results"][0][@"formatted_address"]];
        NSLog(@"address is %@",formattedAddress);

        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            pickedAddress = formattedAddress;
          //  [self addIce:imagesArray];
          [self showSuccessAlertAndGoBack];
        });
        
    }
}
-(void)clearAddIceData{
    dispatch_async(dispatch_get_main_queue(), ^{
 
    startsDateLbl.text = @"";
    endsDateLbl.text = @"";
    startsTimeLbl.text = @"";
    endsTimeLbl.text = @"";
    
    
    tokens = [[NSMutableArray alloc] init];
    [tokenField reloadData];
    _locationLbl.text = @"";
    iceTitleTxt.text = @"";
    _reminderTF.text = @"";
    detailTxt.text = @"";
    imagesArray = [[NSMutableArray alloc] init];
    commonUtils.pickedLocationCord = CLLocationCoordinate2DMake(0, 0);
    [photoCv reloadData];
    [_imgCollectionView reloadData];
    addApiCalled = NO;
    commonUtils.userPickedCord = CGPointZero;
           });
}
-(void)showSuccessAlertAndGoBack{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:@"Images & Videos Are Uploading"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil, nil];
  
    
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == [alertView cancelButtonIndex]){
        if (self.isshowMenu) {
              [self addIce:imagesArray];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
              [self addIce:imagesArray];
                         HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
                         [homeVC onShowTabView:2];
                         [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
                    //     [self.sideMenuController hideLeftViewAnimated:sender];
            

        }
        //cancel clicked ...do your action
    }else{
        //reset clicked
    }
}
-(UIImage*)getThumbnailFromArray:(NSMutableArray*)Array index:(NSInteger)index{
  
        

    if ([Array[index][@"pickedType"]isEqualToString:@"galery"]) {
        
        
        NSURL *assetURL = [NSURL fileURLWithPath:Array[index][@"image"]];
        AVURLAsset *asset = [AVURLAsset assetWithURL:assetURL];
        //
        
        
        
        //
        
        
        //AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[indexPath.row] options:nil];
        
        
        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generate.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 60);
        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
        
        UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
        return img;
  
    }
    else{
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:Array[index][@"image"] options:nil];
        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generate.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 60);
        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
        
       UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
        return img;

        
    }
    

}
@end
