//
//  CommentVC1.m
//  ICE
//
//  Created by LandToSky on 11/25/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "CommentVC1.h"
#import "CommentTVCell.h"
#import "CommentTableVC.h"
#import "CommentTableVC.h"
@interface CommentVC1 ()<UITableViewDelegate, UITableViewDataSource>
{

    IBOutlet UIView *opacityView;
    IBOutlet UIImageView *topBackIv;
}



@end

@implementation CommentVC1
@synthesize commentOF,imageIndex,activityIndex,likesCount,commentsCount,comingFrom;
- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initUI];
    [self initData];
    topBackIv.image = _commentingImage;
}

- (void)initUI
{
    
//    UIView *userCommentView = [self.view viewWithTag:101];
//    userCommentView.layer.shadowColor = [UIColor whiteColor].CGColor;
//    userCommentView.layer.shadowOffset = CGSizeMake(0, -15);
//    userCommentView.layer.shadowOpacity = 0.8;
//    userCommentView.layer.shadowRadius = 5.0f;
    [topBackIv setImage:[self blurredImageWithImage:[UIImage imageNamed:@"image0"]]];
    
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];
    
    
}

- (void)initData
{
    _cmntLbl.text = commentsCount;
    _likesLbl.text = likesCount;
}

- (IBAction)onShowUserComment:(id)sender {
    
    
}

#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;
    [self.navigationController popViewControllerAnimated:YES];
    //    CGPoint point = [sender locationInView:sender.view];
    //    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
    //    if ([viewTouched isKindOfClass:[ZFTokenField class]]) {
    //        // Do nothing;
    //    } else {
    //        // respond to touch action
    //        //        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    //        
    //    }
}

- (UIImage *)blurredImageWithImage:(UIImage *)sourceImage{
    
    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:5.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    /*  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
     *  up exactly to the bounds of our original image */
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *retVal = [UIImage imageWithCGImage:cgImage];
    return retVal;
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
  
    CommentTableVC *vc = [segue destinationViewController];
    

    vc.imageIndex = imageIndex;
    vc.commentOF = @"image";

    vc.activityIndex = activityIndex;
    vc.comingFrom = comingFrom;
    
}

@end
