//
//  CommentVC.h
//  ICE
//
//  Created by LandToSky on 11/23/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IceTVCell.h"
@interface CommentVC : BaseViewController
@property (nonatomic) NSInteger passedTag,sectionIndex;
@property (nonatomic,strong) IceTVCell *cell;
@property (nonatomic,strong) NSString *iceID,*comingfrom;
@property (nonatomic,strong) NSMutableArray *iceDetailsArray;
@end
