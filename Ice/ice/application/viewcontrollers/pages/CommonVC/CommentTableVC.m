//
//  CommentTableViewVC.m
//  ICE
//
//  Created by LandToSky on 11/27/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "CommentTableVC.h"
#import "CommentTVCell.h"
//#import "SZTextView.h" //https://github.com/glaszig/SZTextView  UITextView Placeholder
#import "CSGrowingTextView.h" // https://github.com/cloverstudio/CSGrowingTextView CommentTextView

const CGFloat commentInputViewH = 57.0f;

@interface CommentTableVC ()<UITableViewDelegate, UITableViewDataSource, CSGrowingTextViewDelegate>
{
    IBOutlet UITableView *commentTV;
    
    IBOutlet UIView *commentInputView;
    IBOutlet UIImageView *userIv;
    IBOutlet CSGrowingTextView *commentTextView;
    BOOL isKeyboardShown;
    
    
    float keyboardHeight;
   
    IBOutlet UIView *opacityView;
    BOOL isUserComment;
    NSMutableArray *imageCommentsArray,*imagesArray,*commentsArray;
  
}

@end

@implementation CommentTableVC
@synthesize activityIndex,commentOF,imageIndex,iceCell,comingFrom,iceDetailsArray,iceId,sectionIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self initUI];
    [self initData];
}


- (void)initUI
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    [opacityView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];
    
    [commentTextView.internalTextView setFont:[UIFont fontWithName:@"Lato-Italic" size:13]];
    [commentTextView.internalTextView setTextColor:[UIColor colorWithHex:@"#585858" alpha:1.0f]];
    
    [commentTextView.placeholderLabel setText: @"Tap to comment"];
    [commentTextView.placeholderLabel setFont:[UIFont fontWithName:@"Lato-Italic" size:13]];
    [commentTextView.placeholderLabel setTextColor:[UIColor colorWithHex:@"#ababab" alpha:1.0f]];
    
    [commentTextView setMinimumNumberOfLines:3];
    [commentTextView setMaximumNumberOfLines:5];
    
    [commentTextView setEnablesNewlineCharacter:YES];
    
    commentTextView.delegate = self;
 
    
//    // Make only first letter of sentence is capitalize
//    commentTextView.internalTextView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
}


- (void)initData
{
  
    
    isUserComment = NO;
    isKeyboardShown = NO;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSString *imageUrl = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"photo"]];
    [userIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
              placeholderImage:[UIImage imageNamed:@"user-avatar"]
                       options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    commentsArray = [NSMutableArray new];
  [[IQKeyboardManager sharedManager] setEnable:false];
    if ([commentOF isEqualToString:@"ice"]) {
        commentsArray = [iceDetailsArray mutableCopy];
//        commentsArray =[[NSMutableArray alloc] init];
//        
//        commentsArray = [commonUtils.liveEventsArray[activityIndex][@"get_comments"]mutableCopy];
    
    }
    else if ([commentOF isEqualToString:@"image"]){
      //  NSLog(@"array is %@",commonUtils.liveEventsArray);
        if ([comingFrom isEqualToString:@"userprofile"]) {
            imagesArray = [[NSMutableArray alloc]init];
            imagesArray = [commonUtils.userUpcomingArray[activityIndex][@"get_images"]mutableCopy];
            
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }
        else if([comingFrom isEqualToString:@"tagged"]){
            imagesArray = [[NSMutableArray alloc]init];
            imagesArray = [commonUtils.taggedData[activityIndex][@"get_images"]mutableCopy];
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }
        else if([comingFrom isEqualToString:@"live"]){
            imagesArray = [[NSMutableArray alloc]init];
            imagesArray = [commonUtils.liveEventsArray[activityIndex][@"get_images"]mutableCopy];
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }
        else if([comingFrom isEqualToString:@"upcoming"]){
            imagesArray = [[NSMutableArray alloc]init];
            imagesArray = [commonUtils.upNextEventsArray[activityIndex][@"get_images"]mutableCopy];
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }
        else if([comingFrom isEqualToString:@"comingsoon"]){
            imagesArray = [[NSMutableArray alloc]init];
            imagesArray = [commonUtils.comingSoonArray[activityIndex][@"get_images"]mutableCopy];
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }
        else if([comingFrom isEqualToString:@"calendar"]){
            imagesArray = [[NSMutableArray alloc]init];
            imagesArray = [commonUtils.calendarArray[sectionIndex][activityIndex][@"get_images"]mutableCopy];
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }
        else if([comingFrom isEqualToString:@"events"]){
            imagesArray = [[NSMutableArray alloc]init];
            imagesArray = [commonUtils.activitiesArray[activityIndex][@"get_images"]mutableCopy];
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }
        else if ([comingFrom isEqualToString:@"notification"]){
             imagesArray = [[NSMutableArray alloc]init];
            imagesArray = commonUtils.notificationsArray[activityIndex][@"get_ice"][@"get_images"];
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }
        else if ([comingFrom isEqualToString:@"alert"]){
            imagesArray = [[NSMutableArray alloc]init];
            imagesArray = commonUtils.alertDic[@"get_images"];
            commentsArray = [imagesArray[imageIndex][@"comments"]mutableCopy];
        }


    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppearNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    //NSLog(@"%@",commonUtils.liveEventsArray);
   // [self getDashBoard];
}
- (void)keyboardWillAppearNotification:(NSNotification *)notification {
    keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    isKeyboardShown = (CGRectGetMinY(keyboardFrame) < CGRectGetHeight([[UIScreen mainScreen] bounds]));
    
    [self adjustTableViewFrame:notification];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    [self.view endEditing:YES];
     [[IQKeyboardManager sharedManager] setEnable:true];
}


#pragma mark - UITable View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return commentsArray.count;
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static CommentTVCell *cell = nil;
//    cell.commentLbl.text = commentsArray[indexPath.row][@"commnet"];
//    CGFloat height = [commonUtils heightForText:commentsArray[indexPath.row][@"commnet"] font:[UIFont fontWithName:@"Lato" size:13.0] withinWidth: SCREEN_WIDTH - 77];
//    return height + 37;
//}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CommentTVCell";
    
    CommentTVCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CommentTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.commentLbl.numberOfLines = 0;
    

         cell.userNameLbl.text = [NSString stringWithFormat:@"%@",commentsArray[indexPath.row][@"user"][@"first_name"]];
         NSString *imageUrl = [NSString stringWithFormat:@"%@",commentsArray[indexPath.row][@"user"][@"photo"]];
        [cell.userIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                       placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
  
   
  

    [cell.commentLbl setText:commentsArray[indexPath.row][@"comment"]];
 
   
    if (commentsArray[indexPath.row][@"timeago"]) {
      cell.timeLbl.text = [NSString stringWithFormat:@"%@",commentsArray[indexPath.row][@"timeago"]];
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat commentsHeight = 0;
    
    
    
    
    
    
    
    NSString *comment = commentsArray[indexPath.row][@"comment"];
    
    CGRect commentrect = [comment boundingRectWithSize:CGSizeMake(299, CGFLOAT_MAX)
                          
                                               options:(NSStringDrawingUsesLineFragmentOrigin)
                          
                                            attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Lato-Regular" size:13]}
                          
                                               context:nil];
    
    
    
    commentsHeight += commentrect.size.height;
    
    NSLog(@"comment height is %f",commentsHeight);
    
    if (commentsHeight > 23) {
        
        commentsHeight += 40;
        
    }
    
    else {
        
        commentsHeight = 55;
        
    }
    
    
    
    return commentsHeight;
    
}


- (IBAction)onSendNewComment:(id)sender {
    
    [commentTextView resignFirstResponder];
    isUserComment = YES;
    
//    [commentsArray addObject:commentTextView.internalTextView.text];
//    NSIndexPath *newCommentIndexPath = [NSIndexPath indexPathForRow:commentsArray.count-1 inSection:0];
//    [commentTV beginUpdates];
//    [commentTV insertRowsAtIndexPaths:@[newCommentIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    [commentTV endUpdates];
    if (commentTextView.internalTextView.text.length>0) {
        
    
    NSMutableDictionary *userDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:commonUtils.userData[@"first_name"],@"first_name",commonUtils.userData[@"photo"],@"photo", nil];
     NSMutableDictionary *commentDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:commentTextView.internalTextView.text,@"comment",@"now",@"timeago",userDic,@"user", nil];
    [commentsArray addObject:commentDic];
        NSIndexPath *newCommentIndexPath = [NSIndexPath indexPathForRow:commentsArray.count-1 inSection:0];
        [commentTV beginUpdates];
        [commentTV insertRowsAtIndexPaths:@[newCommentIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [commentTV endUpdates];
    if ([commentOF isEqualToString:@"image"]) {
      
        
        [self commentAPI:[NSString stringWithFormat:@"%@",imagesArray[imageIndex][@"id"]] :commentTextView.internalTextView.text: @"image_id" :@"add_image_comment"];
  

    }
    else {
       
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *likesCount = [f numberFromString:iceCell.commentsCountLbl.text];
            NSInteger likes = [likesCount integerValue];
            likes++;
            iceCell.commentsCountLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
     
  //NSLog(@"ice id is %@",iceId);
        [self commentAPI:iceId :commentTextView.internalTextView.text :@"ice_id" :@"add_ice_comment"];
    }
    commentTextView.internalTextView.text = @"";
    [commonUtils resizeFrame:commentInputView withWidth:commentInputView.frame.size.width withHeight:commentInputViewH];
    CGFloat y = self.view.frame.size.height - commentInputViewH;
    [commonUtils moveView:commentInputView withMoveX:0 withMoveY:y];
   
    [self commentTableViewScrollToBottom];
    }
  
}


- (void)adjustTableViewFrame:(NSNotification *)note {
    
    UIViewAnimationOptions animationCurve = [note.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGFloat animationDuration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat y;
    if (isKeyboardShown) {
        y = self.view.frame.size.height - keyboardHeight - commentInputView.frame.size.height;
    } else {
        y = self.view.frame.size.height - commentInputView.frame.size.height;
    }
    
    
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         [commonUtils moveView:commentInputView withMoveX:0 withMoveY:y];
                     } completion:nil];
}


- (void) commentTableViewScrollToBottom{
    if (commentsArray.count > 0) {
        NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:commentsArray.count-1 inSection:0];
        [commentTV scrollToRowAtIndexPath:lastIndexPath
                               atScrollPosition:UITableViewScrollPositionBottom
                                       animated:YES];
    }
}

#pragma mark - ScrollView Delegate
    
// Undable bottom scroll up
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentSize.height - scrollView.frame.size.height)];
    }
}

#pragma mark - Growing TextView Delegate
- (BOOL) growingTextViewShouldBeginEditing:(CSGrowingTextView *)textView
{
//    [self moveCommentInputViewToUp];
    [self commentTableViewScrollToBottom];
    return YES;
}


- (void)growingTextView:(CSGrowingTextView *)growingTextView willChangeHeight:(CGFloat)height  {
    
    [commonUtils resizeFrame:commentInputView withWidth:commentInputView.frame.size.width withHeight:height];
    CGFloat y;
    if (isKeyboardShown) {
        y = self.view.frame.size.height - keyboardHeight - commentInputView.frame.size.height;
    } else {
        y = self.view.frame.size.height - commentInputView.frame.size.height;
    }

    [commonUtils moveView:commentInputView withMoveX:0 withMoveY:y];
}



#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;
    [self.view endEditing:YES];
//    [self moveCommentInputViewToBottom];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)commentAPI:(NSString*)ID :(NSString *)CommentString :(NSString*)idKey :(NSString*)commentType{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    
    else {
        NSError *error;

       // [commonUtils showHud:self.view];
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:ID forKey:idKey];
        
        [_params setObject:CommentString forKey:@"comment"];
      
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,commentType];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              // NSLog(@"string is %@",myString);
                                         //     NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if([statusis isEqualToString:@"success"]){
                                          
                                                  if ([commentOF isEqualToString:@"image"]) {
                                                      
                                                      if ([comingFrom isEqualToString:@"live"]) {
                                                          NSMutableArray  *liveEventsCopy = [commonUtils.liveEventsArray mutableCopy];
                                                          
                                                          
                                                          NSMutableArray *liveEventsCopy2 = [NSMutableArray new];
                                                          
                                                          for(int index= 0 ; index < liveEventsCopy.count ; index++){
                                                              NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:liveEventsCopy[index]] ;
                                                              
                                                              [liveEventsCopy2 addObject:dataDict];
                                                          }
                                                          
                                                          
                                                          //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                                                          
                                                          
                                                          //
                                                          
                                                          
                                                          NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:liveEventsCopy2[activityIndex][@"get_images"] ];
                                                          //
                                                          
                                                          
                                                          NSMutableArray *newImageArray = [NSMutableArray new];
                                                          
                                                          for(int index= 0 ; index < arrayGetImage.count ; index++){
                                                              NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
                                                              
                                                              [newImageArray addObject:dataDict];
                                                          }
                                                          
                                                          
                                                          [newImageArray[imageIndex] setObject:commentsArray forKey:@"comments"];
                                                          
                                                          
                                                          liveEventsCopy2[activityIndex][@"get_images"] = newImageArray ;
                                                          commonUtils.liveEventsArray = [[NSMutableArray alloc] initWithArray:liveEventsCopy2];
                                                      }
                                                      if ([comingFrom isEqualToString:@"tagged"]) {
                                                          NSMutableArray  *liveEventsCopy = [commonUtils.taggedData mutableCopy];
                                                          
                                                          
                                                          NSMutableArray *liveEventsCopy2 = [NSMutableArray new];
                                                          
                                                          for(int index= 0 ; index < liveEventsCopy.count ; index++){
                                                              NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:liveEventsCopy[index]] ;
                                                              
                                                              [liveEventsCopy2 addObject:dataDict];
                                                          }
                                                          
                                                          
                                                          //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                                                          
                                                          
                                                          //
                                                          
                                                          
                                                          NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:liveEventsCopy2[activityIndex][@"get_images"] ];
                                                          //
                                                          
                                                          
                                                          NSMutableArray *newImageArray = [NSMutableArray new];
                                                          
                                                          for(int index= 0 ; index < arrayGetImage.count ; index++){
                                                              NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
                                                              
                                                              [newImageArray addObject:dataDict];
                                                          }
                                                          
                                                          
                                                          [newImageArray[imageIndex] setObject:commentsArray forKey:@"comments"];
                                                          
                                                          
                                                          liveEventsCopy2[activityIndex][@"get_images"] = newImageArray ;
                                                          commonUtils.taggedData = [[NSMutableArray alloc] initWithArray:liveEventsCopy2];
                                                      }
                                                      else if([comingFrom isEqualToString:@"userprofile"]){
                                                          NSMutableArray  *upcomingArrayCopy = [commonUtils.userUpcomingArray mutableCopy];
                                                          
                                                          
                                                          NSMutableArray *upcomingArrayCopy2 = [NSMutableArray new];
                                                          
                                                          for(int index= 0 ; index < upcomingArrayCopy.count ; index++){
                                                              NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:upcomingArrayCopy[index]] ;
                                                              
                                                              [upcomingArrayCopy2 addObject:dataDict];
                                                          }
                                                          
                                                          
                                                          //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                                                          
                                                          
                                                          //
                                                          
                                                          
                                                          NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:upcomingArrayCopy2[activityIndex][@"get_images"] ];
                                                          //
                                                          
                                                          
                                                          NSMutableArray *newImageArray = [NSMutableArray new];
                                                          
                                                          for(int index= 0 ; index < arrayGetImage.count ; index++){
                                                              NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
                                                              
                                                              [newImageArray addObject:dataDict];
                                                          }
                                                          
                                                          
                                                          [newImageArray[imageIndex] setObject:commentsArray forKey:@"comments"];
                                                          
                                                          
                                                          upcomingArrayCopy2[activityIndex][@"get_images"] = newImageArray ;
                                                          commonUtils.userUpcomingArray = [[NSMutableArray alloc] initWithArray:upcomingArrayCopy2];
                                                      }
                                                    else if ([comingFrom isEqualToString:@"upcoming"]) {
                                                          NSMutableArray  *liveEventsCopy = [commonUtils.upNextEventsArray mutableCopy];
                                                          
                                                          
                                                          NSMutableArray *liveEventsCopy2 = [NSMutableArray new];
                                                          
                                                          for(int index= 0 ; index < liveEventsCopy.count ; index++){
                                                              NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:liveEventsCopy[index]] ;
                                                              
                                                              [liveEventsCopy2 addObject:dataDict];
                                                          }
                                                          
                                                          
                                                          //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                                                          
                                                          
                                                          //
                                                          
                                                          
                                                          NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:liveEventsCopy2[activityIndex][@"get_images"] ];
                                                          //
                                                          
                                                          
                                                          NSMutableArray *newImageArray = [NSMutableArray new];
                                                          
                                                          for(int index= 0 ; index < arrayGetImage.count ; index++){
                                                              NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
                                                              
                                                              [newImageArray addObject:dataDict];
                                                          }
                                                          
                                                          
                                                          [newImageArray[imageIndex] setObject:commentsArray forKey:@"comments"];
                                                          
                                                          
                                                          liveEventsCopy2[activityIndex][@"get_images"] = newImageArray ;
                                                          commonUtils.upNextEventsArray = [[NSMutableArray alloc] initWithArray:liveEventsCopy2];
                                                      }
                                                    else if([comingFrom isEqualToString:@"comingsoon"]){
                                                        NSMutableArray  *liveEventsCopy = [commonUtils.comingSoonArray mutableCopy];
                                                        
                                                        
                                                        NSMutableArray *liveEventsCopy2 = [NSMutableArray new];
                                                        
                                                        for(int index= 0 ; index < liveEventsCopy.count ; index++){
                                                            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:liveEventsCopy[index]] ;
                                                            
                                                            [liveEventsCopy2 addObject:dataDict];
                                                        }
                                                        
                                                        
                                                        //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                                                        
                                                        
                                                        //
                                                        
                                                        
                                                        NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:liveEventsCopy2[activityIndex][@"get_images"] ];
                                                        //
                                                        
                                                        
                                                        NSMutableArray *newImageArray = [NSMutableArray new];
                                                        
                                                        for(int index= 0 ; index < arrayGetImage.count ; index++){
                                                            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
                                                            
                                                            [newImageArray addObject:dataDict];
                                                        }
                                                        
                                                        
                                                        [newImageArray[imageIndex] setObject:commentsArray forKey:@"comments"];
                                                        
                                                        
                                                        liveEventsCopy2[activityIndex][@"get_images"] = newImageArray ;
                                                        commonUtils.comingSoonArray = [[NSMutableArray alloc] initWithArray:liveEventsCopy2];
                                                    }
                                                    else if([comingFrom isEqualToString:@"calendar"]){
                                                        NSMutableArray  *calendarArrayCopy = commonUtils.calendarArray[sectionIndex];
                                                        
                                                        
                                                        NSMutableArray *calendarArrayCopy2 = [NSMutableArray new];
                                                        
                                                        for(int index= 0 ; index < calendarArrayCopy.count ; index++){
                                                            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:calendarArrayCopy[index]] ;
                                                            
                                                            [calendarArrayCopy2 addObject:dataDict];
                                                        }
                                                        
                                                        
                                                        //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                                                        
                                                        
                                                        //
                                                        
                                                        
                                                        NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:calendarArrayCopy2[activityIndex][@"get_images"] ];
                                                        //
                                                        
                                                        
                                                        NSMutableArray *newImageArray = [NSMutableArray new];
                                                        
                                                        for(int index= 0 ; index < arrayGetImage.count ; index++){
                                                            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
                                                            
                                                            [newImageArray addObject:dataDict];
                                                        }
                                                        
                                                        
                                                        [newImageArray[imageIndex] setObject:commentsArray forKey:@"comments"];
                                                        
                                                        
                                                        calendarArrayCopy2[activityIndex][@"get_images"] = newImageArray ;
                                                        commonUtils.calendarArray[sectionIndex] = [[NSMutableArray alloc] initWithArray:calendarArrayCopy2];
                                                    }
                                                    else if([comingFrom isEqualToString:@"events"]){
                                                        NSMutableArray  *activitiesArrayCopy = commonUtils.activitiesArray;
                                                        
                                                        
                                                        NSMutableArray *activtiesArrayCopy2 = [NSMutableArray new];
                                                        
                                                        for(int index= 0 ; index < activitiesArrayCopy.count ; index++){
                                                            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:activitiesArrayCopy[index]] ;
                                                            
                                                            [activtiesArrayCopy2 addObject:dataDict];
                                                        }
                                                        
                                                        
                                                        //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                                                        
                                                        
                                                        //
                                                        
                                                        
                                                        NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:activtiesArrayCopy2[activityIndex][@"get_images"] ];
                                                        //
                                                        
                                                        
                                                        NSMutableArray *newImageArray = [NSMutableArray new];
                                                        
                                                        for(int index= 0 ; index < arrayGetImage.count ; index++){
                                                            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
                                                            
                                                            [newImageArray addObject:dataDict];
                                                        }
                                                        
                                                        
                                                        [newImageArray[imageIndex] setObject:commentsArray forKey:@"comments"];
                                                        
                                                        
                                                        activtiesArrayCopy2[activityIndex][@"get_images"] = newImageArray ;
                                                        commonUtils.activitiesArray = [[NSMutableArray alloc] initWithArray:activtiesArrayCopy2];
                                                    }
                                                    else if([comingFrom isEqualToString:@"notification"]){
                                                     
                                                        NSMutableArray *changedArray = [self changedArray:commonUtils.notificationsArray];
                                                     //   NSLog(@"changed array is %@",changedArray);
                                                        [changedArray[activityIndex][@"get_ice"][@"get_images"][imageIndex]setObject:commentsArray forKey:@"comments"];
                                                        commonUtils.notificationsArray = [[NSMutableArray alloc]initWithArray:changedArray];
                                                   
                                                    }
                                                    else if ([comingFrom isEqualToString:@"alert"]){
                                                        [self changeDicOfAlert];
                                                        
                                                        
                                                    }


                                                  }
                                                 


                                                  
                                                  
                                               
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [commonUtils hideHud];
                                                      
                                                      //[commonUtils showAlert:@"Success" withMessage:successMessage];
                                              
                                                      // Here we need to pass a full frame
                                                      
                                                      
                                                      
                                                      
                                                  });
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      if ([commentOF isEqualToString:@"ice"]) {
                                                          NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                                                          f.numberStyle = NSNumberFormatterDecimalStyle;
                                                          NSNumber *commentsCount = [f numberFromString:iceCell.commentsCountLbl.text];
                                                          NSInteger comments = [commentsCount integerValue];
                                                          comments--;
                                                          iceCell.commentsCountLbl.text = [NSString stringWithFormat:@"%ld",(long)comments];
                                                      }

                                                      [commonUtils hideHud];
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                            //  NSLog(@"no data returned");
                                              //[commonUtils hideHud];
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              [commonUtils hideHud];
                                              //NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
    
}
-(void)getDashBoard{
    
    NSString *serverUrl = [NSString stringWithFormat:@"%@dashboard?time_zone=%@",ServerUrl,commonUtils.getTimeZone];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
    // Create a mutable copy of the immutable request and add more headers
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
   // NSLog(@"%@", request.allHTTPHeaderFields);
    
    
    NSURLResponse *response;
    
    NSError *error = nil;
    
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(error!=nil)
    {
     //   NSLog(@"web service error:%@",error);
    }
    else
    {
        if(receivedData !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:receivedData
                                 options:kNilOptions
                                 error:&Jerror];
            // NSLog(@"user data is %@",json);
            if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                NSDictionary *successDic = [json[@"successData"]mutableCopy];
                commonUtils.liveEventsArray = [successDic[@"live"]mutableCopy];
           
                [commentTV reloadData];
              //  NSLog(@"live array is %@",commonUtils.liveEventsArray);
                
            }
            if(Jerror!=nil)
            {
              //  NSLog(@"json error:%@",Jerror);
            }
        }
    }
    
}
-(NSMutableArray*)changedArray:(NSMutableArray*)arrayToPass{
    NSMutableArray  *notificationsArrayCopy = [NSMutableArray new];
    for ( int i = 0; i<arrayToPass.count; i++) {
        [notificationsArrayCopy addObject:[[NSMutableDictionary alloc]initWithDictionary:arrayToPass[i]]];
    }
    for (int i =0; i<arrayToPass.count; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrayToPass[i][@"get_ice"]];
 
        [notificationsArrayCopy[i]setObject:dic forKey:@"get_ice"];
    }
    for (int i = 0; i<arrayToPass.count; i++) {
        NSMutableArray *imagesArray2 = [[NSMutableArray alloc]initWithArray:arrayToPass[i][@"get_ice"][@"get_images"]];
        NSMutableArray *imagesArray3 = [NSMutableArray new];
        for (int j = 0; j<imagesArray2.count; j++) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:imagesArray2[j]];
            [imagesArray3 addObject:dic];
        }
        
        

        [notificationsArrayCopy[i][@"get_ice"]setObject:imagesArray3 forKey:@"get_images"];
        
    }
   
    return notificationsArrayCopy;
}
-(void)changeDicOfAlert{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:[commonUtils.alertDic mutableCopy]];
    
    NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:dic[@"get_images"] ];
    //
    
    
    NSMutableArray *newImageArray = [NSMutableArray new];
    
    for(int index= 0 ; index < arrayGetImage.count ; index++){
        NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
        
        [newImageArray addObject:dataDict];
    }
    
 [newImageArray[imageIndex] setObject:commentsArray forKey:@"comments"];
    [dic setObject:newImageArray forKey:@"get_images"];
    commonUtils.alertDic = [[NSMutableDictionary alloc]initWithDictionary:dic];
}
@end
