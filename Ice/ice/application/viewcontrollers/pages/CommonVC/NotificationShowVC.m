//
//  NotificationShowVC.m
//  ICE
//
//  Created by LandToSky on 11/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "NotificationShowVC.h"
#import "NotificationTVCell.h"
#import "DetailVC.h"
@interface NotificationShowVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *notiTV;
    NSMutableArray *notiArray,*fullNotificationArray;
    __weak IBOutlet UILabel *notiLbl;
    int currentPageNumber;
    BOOL isPageRefresing;
    
 }


@end

@implementation NotificationShowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    fullNotificationArray = [NSMutableArray new];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];
    [commonUtils setRoundedRectView:notiLbl withCornerRadius:notiLbl.frame.size.height/2];
}

- (void)initData
{
   
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    currentPageNumber = 0;
    [self getNotfications:currentPageNumber];
}
#pragma mark - UITable View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notiArray.count;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"NotificationTVCell";
    
    NotificationTVCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[NotificationTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.titleLbl.text = notiArray[indexPath.row][@"get_ice"][@"title"];
    NSString *imageUrlStr =  [NSString stringWithFormat:@"%@",notiArray[indexPath.row][@"get_ice"][@"get_user"][@"photo"]];
    [cell.userIv sd_setImageWithURL:[NSURL URLWithString:imageUrlStr]
                       placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    cell.userNameLbl.text = [NSString stringWithFormat:@"%@",notiArray[indexPath.row][@"get_ice"][@"get_user"][@"first_name"]];
    BOOL is_live = [notiArray[indexPath.row][@"get_ice"][@"is_live"]boolValue];
    BOOL is_upnext = [notiArray[indexPath.row][@"get_ice"][@"is_upcoming"]boolValue];
    BOOL is_comingsoon = [notiArray[indexPath.row][@"get_ice"][@"is_comming_soon"]boolValue];
    if (is_live) {
        cell.liveLbl.hidden = NO;
        [cell.dateContainerView setBackgroundColor:appController.appBlueColor];
    }
    else if(is_upnext){
        cell.liveLbl.hidden = YES;
        [cell.dateContainerView setBackgroundColor:appController.appGreenColor];
    }
    else{
        cell.liveLbl.hidden = YES;
        [cell.dateContainerView setBackgroundColor:appController.appRedColor];
    }
    NSDate *today = [NSDate date];
    
    
    NSDate *todayDate = [self toLocalTime:today];
    
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    
    NSDate * endDateFromApi = [startTimeformatter dateFromString:notiArray[indexPath.row][@"get_ice"][@"server_end_time"]];
    // your date
    
    
    NSComparisonResult result;
    
    
    result = [todayDate compare:endDateFromApi]; // comparing two dates
    
    if(result==NSOrderedDescending)
    {
        NSLog(@"today is less");
        cell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
        cell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
        cell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
    
        
    }
    
    else if(result==NSOrderedAscending){
        NSLog(@"server date  is less");
        cell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
        cell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
        cell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:today]uppercaseString];
       
        
    }
    
    
    else{
        cell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
        cell.dateLbl.text = [[self getRequiredDate:@"dd" dateToConvert:endDateFromApi]uppercaseString];
        cell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];

        NSLog(@"Both dates are same");
    }

//    [cell.titleLbl setText:notiArray[indexPath.row]];
//    switch (indexPath.row % 3) {
//        case 0:
//            [cell.dateContainerView setBackgroundColor:appController.appBlueColor];
//            break;
//        case 1:
//            [cell.dateContainerView setBackgroundColor:appController.appGreenColor];
//            break;
//        case 2:
//            [cell.dateContainerView setBackgroundColor:appController.appRedColor];
//            break;
//            
//        default:
//            break;
//    }
    [commonUtils setRoundedRectBorderView:cell.liveLbl withBorderWidth:1.0f withBorderColor:appController.appBlueColor withBorderRadius:0.0f];
    cell.showUserProfileVCBtn.tag = indexPath.row;
    cell.showEventDetailVCBtn.tag = indexPath.row;
    [cell.showUserProfileVCBtn addTarget:self action:@selector(onShowUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
    [cell.showEventDetailVCBtn addTarget:self action:@selector(onShowEventDetailVC:) forControlEvents:UIControlEventTouchUpInside];    
    
    return cell;
}

- (void)onShowUserProfileVC:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    commonUtils.notificationsArray = notiArray;
    [self.delegate showUserProfileVC:sender];
}

- (void)onShowEventDetailVC:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

    commonUtils.notificationsArray = notiArray;
      commonUtils.checkincount = notiArray[sender.tag][@"get_checked_in_count"];
    NSLog(@"check in count is %@",commonUtils.checkincount);
    [self.delegate showEventDetailVC:sender];
}



#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;
    [self dismissViewControllerAnimated:NO completion:nil];
    //    CGPoint point = [sender locationInView:sender.view];
    //    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
    //    if ([viewTouched isKindOfClass:[ZFTokenField class]]) {
    //        // Do nothing;
    //    } else {
    //        // respond to touch action
    //        //        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    //        
    //    }
}

-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    
    
}
-(void)getNotfications:(int)currentPage{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        [commonUtils showHud:self.view];
        NSString *skip = [NSString stringWithFormat:@"%d",currentPage];
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"http://139.162.37.73/iceapp/api/v1/get_all_notifications/%@?time_zone=%@",skip,timezone];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(receivedData !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:receivedData
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    
                    NSMutableDictionary *successDic = [[NSMutableDictionary alloc]init];
                    successDic = json[@"successData"];
                    //                    icedActiviteis = successDic[@"iced_activities"];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [commonUtils hideHud];
                        notiLbl.text = [NSString stringWithFormat:@"%@",successDic[@"un_read"]];
                     
                        [self didFinishRecordsRequest:successDic[@"notifications"] forPage:currentPageNumber];
                      
                        // [mainTV reloadData];
                    });
                    
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(notiTV.contentOffset.y >= (notiTV.contentSize.height - notiTV.bounds.size.height)) {
        
        //NSLog(@" scroll to bottom!");
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            NSInteger notficationCount = [notiLbl.text integerValue];
            isPageRefresing = YES;
            if (currentPageNumber<(notficationCount/5)) {
                currentPageNumber = currentPageNumber +1;
                [self getNotfications:currentPageNumber];
            }
            
        }
    }
    
}
-(void)didFinishRecordsRequest:(NSArray *)results forPage:(NSInteger)pageNo{
    if(pageNo == 0){
        fullNotificationArray = [results mutableCopy];
        notiArray = [results mutableCopy];
    }
    else{
        [fullNotificationArray addObject:results];
        [notiArray addObjectsFromArray:results];
    }
    isPageRefresing = NO;
    [notiTV reloadData];
}

@end
