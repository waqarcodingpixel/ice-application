//
//  CommentVC.m
//  ICE
//
//  Created by LandToSky on 11/23/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "CommentVC.h"
#import "CommentTVCell.h"
#import "CommentTableVC.h"
@interface CommentVC ()
{


    IBOutlet UIView *opacityView;
}

@end

@implementation CommentVC
@synthesize passedTag,cell,iceDetailsArray,iceID,comingfrom,sectionIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)initUI
{
//     [self.view setAlpha:0.8];
    [self.view setBackgroundColor:[UIColor clearColor]];
//    UIView *userCommentView = [self.view viewWithTag:101];
//    userCommentView.layer.shadowColor = [UIColor whiteColor].CGColor;
//    userCommentView.layer.shadowOffset = CGSizeMake(0, -15);
//    userCommentView.layer.shadowOpacity = 0.8;
//    userCommentView.layer.shadowRadius = 5.0f;    
    
    
    [opacityView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];

    
}

- (void)initData
{


    
}


- (IBAction)onShowUserComment:(id)sender {
    
    
}



#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"detail"
     object:self];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"live"
     object:self];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"upcoming"
     object:self];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"comingsoon"
     object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
//    CGPoint point = [sender locationInView:sender.view];
//    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
//    if ([viewTouched isKindOfClass:[ZFTokenField class]]) {
//        // Do nothing;
//    } else {
//        // respond to touch action
//        //        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//        
//    }
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
        // Get reference to the destination view controller
        CommentTableVC *vc = [segue destinationViewController];
    
        // Pass any objects to the view controller here, like...
        vc.activityIndex = passedTag;
    vc.commentOF = @"ice";
    vc.iceCell = cell;
    vc.iceDetailsArray = iceDetailsArray;
    vc.iceId = iceID;
    vc.comingFrom = comingfrom;
    
    
}

@end
