//
//  EventCollectionVC.m
//  ICE
//
//  Created by LandToSky on 11/27/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "EventCollectionVC.h"
#import "EventImageCVCell.h"
#import <HWViewPager.h>
#import "PhotoVideoShowVC.h"

@interface EventCollectionVC ()<UICollectionViewDataSource, HWViewPagerDelegate>
{
    
    IBOutlet HWViewPager *eventPhotoCV;
    NSMutableArray *imagesArray;
}

@end

@implementation EventCollectionVC
@synthesize index,comingFrom,eventType,sectionIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    eventPhotoCV.dataSource = self;
    eventPhotoCV.pagerDelegate = self;
}

- (void)initData
{
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if ([comingFrom isEqualToString:@"userprofile"]) {
        //NSLog(@"upcoming commutil %@",commonUtils.userUpcomingArray[index]);
        imagesArray = [NSMutableArray new];
        NSLog(@"commutil array is %@",commonUtils.userUpcomingArray);
      imagesArray = commonUtils.userUpcomingArray[index][@"get_images"];  
    }
    else {
        if ([eventType isEqualToString:@"live"]) {
             imagesArray = commonUtils.liveEventsArray[index][@"get_images"];
        }
        else if([eventType isEqualToString:@"upcoming"]){
             imagesArray = commonUtils.upNextEventsArray[index][@"get_images"];
        }
        else if([eventType isEqualToString:@"comingsoon"]){
             imagesArray = commonUtils.comingSoonArray[index][@"get_images"];
        }
        else if([eventType isEqualToString:@"tagged"]){
            imagesArray = commonUtils.taggedData[index][@"get_images"];
        }
        else if([eventType isEqualToString:@"calendar"]){
            //NSLog(@"section index %ld",(long)sectionIndex);
          //  NSLog(@"index is %ld",(long)index);
            imagesArray = commonUtils.calendarArray[sectionIndex][index][@"get_images"];
        }
        else if([eventType isEqualToString:@"events"]){
            //NSLog(@"section index %ld",(long)sectionIndex);
            //  NSLog(@"index is %ld",(long)index);
            imagesArray = commonUtils.activitiesArray[index][@"get_images"];
        }
        else if ([eventType isEqualToString:@"notification"]){
            imagesArray = commonUtils.notificationsArray[index][@"get_ice"][@"get_images"];
        }
        else if ([eventType isEqualToString:@"alert"]){
            imagesArray = commonUtils.alertDic[@"get_images"];
        }

        
      //  NSLog(@"array is %@",commonUtils.liveEventsArray[index]);
    }
    
}
#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   // if ([comingFrom isEqualToString:@"userprofile"]) {
    //     return upComingArray.count;
    //}
    //else {
return  imagesArray.count;
    //}
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
     NSString *imageUrl;
    cell.videoPlayBtn.tag = indexPath.row;

    [cell.videoPlayBtn addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
    if ([imagesArray[indexPath.row][@"type"]isEqualToString:@"image"]) {
        imageUrl  = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,imagesArray[indexPath.row][@"image"]];
        [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"image0"]
                                 options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        cell.videoPlayBtn.hidden = YES;
        
    }
    else{
        imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,imagesArray[indexPath.row][@"poster"]];
        [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"image0"]
                                 options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        cell.videoPlayBtn.hidden = NO;
    }

    
    
    cell.eventImageSelectBtn.tag = indexPath.item;
    [cell.eventImageSelectBtn addTarget:self action:@selector(onSelectEventImageView:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}
#pragma mark - PlayVideo
- (void)onPlayVideo:(UIButton*) sender{


    NSURL *videoUrl;
    
    videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,imagesArray[sender.tag][@"image"]]];
    
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}

- (void)onSelectEventImageView:(UIButton*) sender
{
    NSLog(@"Selected Event Image View");
    PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
    vc.currentPageNum = sender.tag;
    vc.collectionViewTag = index;
    vc.comingFrom = comingFrom;
    vc.eventType = eventType;
    vc.sectionIndex = sectionIndex;
//    vc.upComingArray = upComingArray;
    
    [self.navigationController pushViewController:vc animated:YES];
    //

}


#pragma mark - Show Comment VC
- (void) onShowCommentVC:(UIButton*) sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentVC"];
    vc.providesPresentationContextTransitionStyle = YES;
    vc.definesPresentationContext = YES;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}

#pragma mark - HWViewPagerDelegate
-(void)pagerDidSelectedPage:(NSInteger)selectedPage{
    NSLog(@"FistViewController, SelectedPage : %d",(int)selectedPage);
}
#pragma mark - VideoPlay
- (void)videoPlay:(UIButton*)sender {
    NSURL *videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,imagesArray[sender.tag][@"image"]]];
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}
@end
