//
//  NotificationShowVC.h
//  ICE
//
//  Created by LandToSky on 11/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
@protocol DismissNotificationVCDelegate <NSObject>

- (void)showUserProfileVC:(UIButton*)sender;
- (void)showEventDetailVC:(UIButton*)sender;

@end


@interface NotificationShowVC : BaseViewController

@property (nonatomic, weak) id<DismissNotificationVCDelegate> delegate;


@end
