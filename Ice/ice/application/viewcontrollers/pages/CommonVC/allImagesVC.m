//
//  allImagesVC.m
//  ICE
//
//  Created by MAC MINI on 04/08/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "allImagesVC.h"
#import "imageCV.h"
#import "PhotoVideoShowVC.h"
@interface allImagesVC ()

@end

@implementation allImagesVC
@synthesize iceImages,isUserImages;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return iceImages.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize mElementSize = CGSizeMake(collectionView.frame.size.width/3 - 1, collectionView.frame.size.width/3-1);
    return mElementSize;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    imageCV * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"allImagesCell" forIndexPath:indexPath];
    NSString *imageUrl;
    if (isUserImages) {
      imageUrl = [NSString stringWithFormat:@"%@",iceImages[indexPath.row][@"image_path"]];
    }
    else{
        NSString *type = [NSString stringWithFormat:@"%@",iceImages[indexPath.row][@"type"]];
        if ([type isEqualToString:@"image"]) {
            cell.videoPlayBtn.hidden = YES;
            imageUrl = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,iceImages[indexPath.row][@"image"]];
        }
        else{
            cell.videoPlayBtn.tag = indexPath.row;
            [cell.videoPlayBtn addTarget:self action:@selector(playvideomethod:) forControlEvents:UIControlEventTouchUpInside];
            cell.videoPlayBtn.hidden = NO;
             imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,iceImages[indexPath.row][@"poster"]];
        }
   
    }
   
    [cell.eventIV sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                    placeholderImage:[UIImage imageNamed:@"image0"]
                             options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    
    
    return cell;
    
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
    vc.currentPageNum = indexPath.row;
    commonUtils.userIcePics = iceImages;
    vc.comingFrom = @"ownProfile";
    [self.navigationController pushViewController:vc animated:YES];

    
}

- (IBAction)doneBtn:(id)sender {
    [_imgToShowView setHidden:YES];
 //   [self.view sendSubviewToBack:_imgToShowView];

}
- (IBAction)backBtn:(id)sender {
    [_imgToShowView setHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
    //   [self.view sendSubviewToBack:_imgToShowView];
    
}
-(void)playvideomethod:(UIButton*)sender{
    NSURL *videoUrl;
    if (iceImages[sender.tag][@"ice_id"]) {
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,iceImages[sender.tag][@"image"]]];
    }
    else{
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserVideoBaseURL,iceImages[sender.tag][@"image"]]];
    }
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}

@end
