//
//  EventCollectionVC.h
//  ICE
//
//  Created by LandToSky on 11/27/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCollectionVC : UIViewController
@property (nonatomic) NSInteger index,sectionIndex;

@property (nonatomic,strong) NSString *comingFrom,*eventType;

@end
