//
//  CommentVC1.h
//  ICE
//
//  Created by LandToSky on 11/25/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CommentVC1 : BaseViewController
@property (nonatomic) NSInteger activityIndex,imageIndex;
@property (nonatomic,strong) NSString *commentOF,*likesCount,*commentsCount,*comingFrom;
@property (weak, nonatomic) IBOutlet UILabel *cmntLbl;
@property (weak, nonatomic) IBOutlet UILabel *likesLbl;
@property (nonatomic,strong) UIImage *commentingImage;


@end
