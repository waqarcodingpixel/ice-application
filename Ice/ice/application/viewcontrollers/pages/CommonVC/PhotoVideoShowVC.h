//
//  PhotoVideoShowVC.h
//  ICE
//
//  Created by LandToSky on 11/25/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ZFTokenField.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
@interface PhotoVideoShowVC : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *commentsLbl;
@property (weak, nonatomic) IBOutlet UILabel *likesLbl;
@property (weak, nonatomic) IBOutlet UIButton *imgLikeBtn;
@property (weak, nonatomic) IBOutlet UIView *taggingView;
@property (weak, nonatomic) IBOutlet ZFTokenField *tokenField;
@property (weak, nonatomic) IBOutlet UITableView *tagTV;
@property (weak, nonatomic) IBOutlet UIView *tokenView;
@property (weak, nonatomic) IBOutlet UIScrollView *tokenSV;

- (IBAction)imgLikeBtn:(id)sender;
@property (nonatomic, assign) NSInteger currentPageNum;
@property (nonatomic, assign) NSInteger collectionViewTag,sectionIndex;
@property (nonatomic,weak) IBOutlet UITextField  *inviteFriendTxt;
@property (nonatomic,strong) NSString *comingFrom;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (strong,nonatomic) NSString *eventType;
- (IBAction)tagBtn:(id)sender;
- (IBAction)videoPlayBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *videoPlayOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *likeIV;
@end
