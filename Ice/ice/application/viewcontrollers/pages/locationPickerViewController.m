//
//  locationPickerViewController.m
//  ICE
//
//  Created by Vengile on 19/07/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "locationPickerViewController.h"
#import <MapKit/MapKit.h>
#import "ICEVC.h"
@interface locationPickerViewController ()

@end

@implementation locationPickerViewController{
    CLLocationCoordinate2D touchMapCoordinate;
    BOOL locationPicked;
    NSString *searchedAddress;
    CLLocationManager *locationManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initGmap];
    _gmapView.delegate = self;
     _locationPickerMap.delegate = self;
    
    _locationPickerMap.showsUserLocation = YES;

    
    CLLocation *previousLoc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude];
    NSLog(@"previous loc is %f",previousLoc.coordinate.latitude);
    if (previousLoc.coordinate.latitude != 0.000000) {
        [self dropPreviousPin];
        locationPicked = YES;
    }
    else{
        locationPicked = NO;
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
    if (!CGPointEqualToPoint(commonUtils.userPickedCord, CGPointZero)){
       
    }
    
}

#pragma mark - Gesture Recognizer

// Gesture recognizer method
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
   // [self removeAllPinsButUserLocation];
   
   
    CGPoint longPressPoint = [gestureRecognizer locationInView:_gmapView];
    CLLocationCoordinate2D coordinate = [_gmapView.projection coordinateForPoint: longPressPoint];
    GMSMarker *pickedLoc = [GMSMarker markerWithPosition:coordinate];
    pickedLoc.title = @"Picked Location";
   // london.icon = [UIImage imageNamed:@"house"];
    pickedLoc.map = _gmapView;
  
}
#pragma mark - Map Methods
- (void)removeAllPinsButUserLocation
{
    id userLocation = _locationPickerMap.userLocation;
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[_locationPickerMap annotations]];
    if ( userLocation != nil ) {
        [pins removeObject:userLocation]; // avoid removing user location off the map
    }
    
    [_locationPickerMap removeAnnotations:pins];
  
    pins = nil;
}
-(void)setInitialMapZoom
{
    NSLog(@"Did update user location");
    MKCoordinateRegion mapRegion;
    mapRegion.center = _locationPickerMap.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [_locationPickerMap setRegion:mapRegion animated: YES];
}

- (IBAction)locationPickedBtn:(id)sender {
    if (!locationPicked) {
        CLLocation *pickedloc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude];
    
    if (CLLocationCoordinate2DIsValid(touchMapCoordinate)&&!(touchMapCoordinate.latitude==0)&&!(touchMapCoordinate.longitude==0)) {
          locationPicked = YES;

        [self getAddressFromGoogle];
    }
        
       
 else if(pickedloc.coordinate.latitude != 0.000000 ) {
     [self locationPickedGoBack];
    }
 else {
       [commonUtils showAlert:@"Error!" withMessage:@"Please select ICE location"];
 }
    }
    else{

        [self getAddressFromGoogle];
     
    }
}
#pragma mapview methods
-(void)getAddressFromCoordinates{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      
                      //  NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      
                      
                      NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
                      ICEVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];
                      dispatch_async(dispatch_get_main_queue(), ^(void){
                          //Run UI Updates
                         vc.locationLbl.text = locatedAt;
                          [commonUtils showAlert:@"Success" withMessage:@"ICE location is set"];
                          [self.navigationController popViewControllerAnimated:YES];
                      });
                     
                  }
                  else {
                      NSLog(@"Could not locate");
                  }
              }
     ];
}
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
   }
-(void)dropPreviousPin{
    CLLocationCoordinate2D coordinate =CLLocationCoordinate2DMake(commonUtils.pickedLocationCord.latitude,commonUtils.pickedLocationCord.longitude);
    GMSMarker *pickedLoc = [GMSMarker markerWithPosition:coordinate];
    pickedLoc.title = @"Picked Location";
    // london.icon = [UIImage imageNamed:@"house"];
    pickedLoc.map = _gmapView;

}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
  CLLocation  *currentLocation = [locations lastObject];

    NSLog(@"gmap loc is %f",_gmapView.myLocation.coordinate.latitude);
    double currentlat = currentLocation.coordinate.latitude;
    double currentlng = currentLocation.coordinate.longitude;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentlat
                                                            longitude:currentlng
                                                                 zoom:15];
    
    [_gmapView animateToCameraPosition:camera];
    [locationManager stopUpdatingLocation];

}

#pragma map search methods
- (IBAction)onLaunchClicked:(id)sender {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}
// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    //NSLog(@"place dic %@",place);
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSLog(@"place lat is %f",place.coordinate.latitude);
    NSLog(@"place lng is %f",place.coordinate.longitude);
    searchedAddress = place.formattedAddress;
    
    CLLocationCoordinate2D center = place.coordinate;
   // center.latitude -= self.mapView.region.span.latitudeDelta * 0.40;
    [self.locationPickerMap setCenterCoordinate:center animated:YES];
    [self zoomInToMyLocation:place.coordinate];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
-(void)zoomInToMyLocation:(CLLocationCoordinate2D)center
{
    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = center.latitude;
    region.center.longitude = center.longitude;
    region.span.longitudeDelta = 0.02f;
    region.span.latitudeDelta = 0.02f;
    [self.locationPickerMap setRegion:region animated:YES];
      [self removeAllPinsButUserLocation];
    MKPointAnnotation *annot = [[MKPointAnnotation alloc] init];
    annot.coordinate = center;
    [self.locationPickerMap addAnnotation:annot];
    commonUtils.pickedLocationCord = center;
    locationPicked = NO;
    

}
-(void)locationPickedGoBack{
    NSString *locatedAt = searchedAddress;
    
    
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    ICEVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];
    dispatch_async(dispatch_get_main_queue(), ^(void){
        //Run UI Updates
        vc.locationLbl.text = locatedAt;
        [commonUtils showAlert:@"Success" withMessage:@"ICE location is set"];
        [self.navigationController popViewControllerAnimated:YES];
         });
}
-(void)getAddressFromGoogle{
 
    //NSLog(@"App id is %@",AppUtility.deviceId);
    NSString * authenticate_url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true/false",commonUtils.pickedLocationCord.latitude,commonUtils.pickedLocationCord.longitude];
    NSLog(@"url string : %@",authenticate_url);
    // NSLog(@"client id here is %@",_clienti);
    NSData * webData = [NSData dataWithContentsOfURL:[NSURL URLWithString:authenticate_url]];
    NSLog(@"come here check2");
    if(webData == nil)
    {
        NSLog(@"No data Found");
    
        
        return;
    }
    else
    {
        
        NSString * responseString = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
        NSLog(@"response string %@",responseString);
        NSError * error;
        NSDictionary *json =
        [NSJSONSerialization JSONObjectWithData: [responseString dataUsingEncoding:NSUTF8StringEncoding]
                                        options: NSJSONReadingMutableContainers
                                          error: &error];
        NSLog(@"%@",json);
        NSString *formattedAddress = [NSString stringWithFormat:@"%@",json[@"results"][0][@"formatted_address"]];
        NSLog(@"address is %@",formattedAddress);
        NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
        ICEVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            vc.locationLbl.text = formattedAddress;
            [commonUtils showAlert:@"Success" withMessage:@"ICE location is set"];
            [self.navigationController popViewControllerAnimated:YES];
        });

    }
}
-(void)initGmap{
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    // Create a GMSCameraPosition that tells the map to display the
    _gmapView.myLocationEnabled = YES;
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 500; // meters
    [locationManager startUpdatingLocation];
  
}
- (void)mapView:(GMSMapView *)mapView
didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [_gmapView clear];
    GMSMarker *pickedLoc = [GMSMarker markerWithPosition:coordinate];
    pickedLoc.title = @"Picked Location";
    // london.icon = [UIImage imageNamed:@"house"];
    pickedLoc.map = _gmapView;
    touchMapCoordinate = coordinate;
    commonUtils.pickedLocationCord = coordinate;

}
@end
