//
//  Place.h
//  iTransitBuddy
//
//  Created by Blue Technology Solutions LLC 09/09/2008.
//  Copyright 2010 Blue Technology Solutions LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Place : NSObject {

}

@property (nonatomic) int eventID;
@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* time;
@property (nonatomic, retain) NSString* address;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@property (nonatomic) int isMain;
@property (nonatomic) NSString *date;
@property (nonatomic) NSString *weekday;
@property (nonatomic) NSString *eventDate;
@property (nonatomic) NSString *eventMnth;
@property (nonatomic) NSString *user_photo_url;
@property (nonatomic) BOOL isLiveEvent;


@end
