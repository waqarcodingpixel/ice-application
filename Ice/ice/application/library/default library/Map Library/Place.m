//
//  Place.m
//  iTransitBuddy
//
//  Created by Blue Technology Solutions LLC 09/09/2008.
//  Copyright 2010 Blue Technology Solutions LLC. All rights reserved.
//

#import "Place.h"


@implementation Place

- (instancetype)init {
    self = [super init];
    
    if (self) {
        // initialize instance variables here
        self.title = @"";
        self.time = @"";
        self.address = @"";
        self.latitude = 0;
        self.longitude = 0;
        self.isMain = 0;
        self.date = @"";
        self.weekday = @"";
        self.user_photo_url = @"";
    }
    
    return self;
}
@end
