//
//  MapCalloutViewEvent.h
//  ICE
//
//  Created by LandToSky on 11/18/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapCalloutViewEvent : UIView

// Date Container View
@property (weak, nonatomic) IBOutlet UIImageView *dateContainerView;
@property (weak, nonatomic) IBOutlet UILabel *weekdayLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *monthLbl;

// User Image View
@property (weak, nonatomic) IBOutlet UIImageView *userIv;
@property (weak, nonatomic) IBOutlet UIButton *userProfileBtn;

// Detail View
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;


@end
