//
//  NSDate+Compare.m
//  ICE
//
//  Created by LandToSky on 1/20/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "NSDate+Compare.h"

@implementation NSDate(Compare)

-(BOOL) isLaterThanOrEqualTo:(NSDate*)date {
    return !([self compare:date] == NSOrderedAscending);
}

-(BOOL) isEarlierThanOrEqualTo:(NSDate*)date {
    return !([self compare:date] == NSOrderedDescending);
}
-(BOOL) isLaterThan:(NSDate*)date {
    return ([self compare:date] == NSOrderedDescending);
    
}
-(BOOL) isEarlierThan:(NSDate*)date {
    return ([self compare:date] == NSOrderedAscending);
}

@end
