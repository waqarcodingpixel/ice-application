//
//  RMCalendarCell.h
//  ICE
//
//  Created by LandToSky on 2/7/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FSCalendar.h"

@interface RMCalendarCell : FSCalendarCell

@property (weak, nonatomic) UIImageView *todayShowIv;
//@property (weak, nonatomic) CAShapeLayer *selectionLayer;

@end
