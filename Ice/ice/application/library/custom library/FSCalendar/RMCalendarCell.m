//
//  RMCalendarCell.m
//  ICE
//
//  Created by LandToSky on 2/7/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "RMCalendarCell.h"
#import "FSCalendarExtensions.h"

@implementation RMCalendarCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Today show Imageview
        UIImageView *todayShowIv = [[UIImageView alloc] init];
        [self.contentView insertSubview:todayShowIv atIndex:0];
        todayShowIv.backgroundColor = [UIColor clearColor];
        todayShowIv.layer.borderWidth = 1.5f;
        todayShowIv.layer.borderColor = appController.appBlueColor.CGColor;
        self.todayShowIv = todayShowIv;
        
        
        // Select Show Layer
        /*
        CAShapeLayer *selectionLayer = [[CAShapeLayer alloc] init];
        selectionLayer.fillColor = RGBA(141, 201, 240, 0.3).CGColor;
        selectionLayer.borderColor = RGBA(141, 201, 240, 1.0).CGColor;
        selectionLayer.borderWidth = 1.0f;
        selectionLayer.actions = @{@"hidden":[NSNull null]};
        [self.contentView.layer insertSublayer:selectionLayer below:self.titleLabel.layer];
        self.selectionLayer = selectionLayer;
        self.selectionLayer.hidden = YES;
        */
        
        self.shapeLayer.hidden = YES;
        self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
//        self.backgroundView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1.0f];
        
    }
    return self;
}



- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.backgroundView.frame = CGRectInset(self.bounds, .5, 0);
    self.todayShowIv.frame = self.bounds;
 //   self.selectionLayer.frame = self.bounds;
//    self.selectionLayer.path = [UIBezierPath bezierPathWithRect:self.selectionLayer.bounds].CGPath;
}

- (void)configureAppearance
{
    [super configureAppearance];
    self.eventIndicator.hidden = self.placeholder; // Hide the event indicator for placeholder cells
    if (self.placeholder) {
        self.backgroundView.backgroundColor = [UIColor clearColor];
    } else {
        self.backgroundView.backgroundColor = [UIColor whiteColor];
    }
}

@end
