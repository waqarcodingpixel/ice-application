//
//  IcedDoneAlertView.m
//  ICE
//
//  Created by LandToSky on 1/14/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "IcedDoneAlertView.h"

@interface IcedDoneAlertView()
{
    
    IBOutletCollection(UIImageView) NSArray *images;
    IBOutletCollection(UIButton) NSArray *buttons;
}
@end

@implementation IcedDoneAlertView
@synthesize btnChecked;
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}


- (void)setup
{
    btnChecked = [[NSMutableArray alloc] initWithCapacity:3];
    for (int i = 0; i < 3; i++) {
        [btnChecked addObject:@(NO)];
    }
    
    for (UIButton* btn in buttons) {
        btn.tag = [buttons indexOfObject:btn];
    }
    commonUtils.sharingOptions = [[NSMutableDictionary alloc] init];
}

- (IBAction)onButtons:(UIButton*)sender
{
    NSInteger i = sender.tag;
    BOOL checked = ![btnChecked[i] boolValue];
    btnChecked[i] = @(checked);
    
    switch (i) {
        case 0:
            if (checked) {
                [commonUtils.sharingOptions setValue:@"yes" forKey:@"facebook"];
               // NSLog(@"%@",commonUtils.sharingOptions);
                [images[i] setImage:[UIImage imageNamed:@"facebook-on"]];
            } else {
                [commonUtils.sharingOptions setValue:@"no" forKey:@"facebook"];
                [images[i] setImage:[UIImage imageNamed:@"facebook-off"]];
            }
            break;
            
        case 1:
            if (checked) {
                [commonUtils.sharingOptions setValue:@"yes" forKey:@"twitter"];
                [images[i] setImage:[UIImage imageNamed:@"twitter-on"]];
            } else {
                [commonUtils.sharingOptions setValue:@"no" forKey:@"twitter"];
                [images[i] setImage:[UIImage imageNamed:@"twitter-off"]];
            }
            break;
            
        case 2:
            if (checked) {
                [commonUtils.sharingOptions setValue:@"yes" forKey:@"google"];
                [images[i] setImage:[UIImage imageNamed:@"google-on"]];
            } else {
                [commonUtils.sharingOptions setValue:@"no" forKey:@"google"];
                [images[i] setImage:[UIImage imageNamed:@"google-off"]];
            }
            break;
            
        default:
            break;
    }
}



@end
