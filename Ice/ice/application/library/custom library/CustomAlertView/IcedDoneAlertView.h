//
//  IcedDoneAlertView.h
//  ICE
//
//  Created by LandToSky on 1/14/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IcedDoneAlertView : UIView

@property (nonatomic, strong) IBOutlet UIButton *doneBtn;
@property (nonatomic,strong) NSMutableArray *btnChecked;
@end
