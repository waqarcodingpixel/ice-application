//
//  AppDelegate.m
//  ICE
//
//  Created by LandToSky on 11/10/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "DetailVC.h"
#import <IQKeyboardManager.h>

@interface AppDelegate ()

@end
float locDistance = -1.0;
CLLocationDistance meters;
@implementation AppDelegate

{
    CLLocation *currentLocation ;
    CLLocation *lastLocationFnded;
   
 
}
@synthesize updateloctimer,imagesArrayCopy;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSDictionary *userdata = [commonUtils getUserData];
    if (userdata) {
        commonUtils.userData = userdata;
        commonUtils.sessionToken = [NSUserDefaults.standardUserDefaults valueForKey:@"session_token"];
    }

  //  [commonUtils initAddressBook]; // Get contact list from iphone
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [self updateLocationManager];
    
    
    
    
//    [[IQKeyboardManager sharedManager] isEnabled] = true;
    
    [[IQKeyboardManager sharedManager] setEnable:true];

    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
    [[IQKeyboardManager sharedManager] setShouldShowTextFieldPlaceholder:false];
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    // Register Push Notification
  
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
      [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    if(launchOptions != nil && [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]) {
        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        appController.apnsMessage = [[userInfo objectForKey:@"aps"] objectForKey:@"info"];
        [commonUtils setUserDefault:@"apns_message_arrived" withFormat:@"1"];
        NSDictionary *aps = userInfo[@"aps"];
        NSDictionary *apsData = aps[@"data"];
        NSString *iceId = [NSString stringWithFormat:@"%@",apsData[@"ice_id"]];
        commonUtils.noti_ice_id = iceId;
        commonUtils.is_Notification = YES;
        if (commonUtils.sessionToken) {
            //        dispatch_async(dispatch_get_main_queue(), ^{
            if (commonUtils.is_Notification) {
               
              
                    
                 //   commonUtils.is_Notification = NO;
                    
                    [self getIceDetail];
                    
                    //Do checking here.
                
            }
            
            
            
            //        });
        }

    }
    
   //    [navigationController performSelector:@selector(pushNewViewFromTableCell:) withObject:view afterDelay:1.0]
    
    // First VC whether exists default user in app
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UINavigationController *rootNav = [storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    
    
//    [self.window setRootViewController:rootNav];
//    [self.window makeKeyAndVisible];
    
    
     [self performSelectorInBackground:@selector(startTenMinutesTimer) withObject:nil];
    [GMSPlacesClient provideAPIKey:@"AIzaSyA2RSi7u6lAFI8H9LC3NhnscRc44lgV5Jg"];
    [GMSServices provideAPIKey:@"AIzaSyA2RSi7u6lAFI8H9LC3NhnscRc44lgV5Jg"];
       return YES;
}
-(void)startTenMinutesTimer
{
    updateloctimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(callingServiceFetchData) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:updateloctimer forMode:NSRunLoopCommonModes];
}
-(void)callingServiceFetchData
{
    [NSThread sleepForTimeInterval:0.3];
    [self performSelectorInBackground:@selector(callService) withObject:nil];
}
-(void) callService

{
    
    [self UpdatingLocation];
    //     [self performSelectorInBackground:@selector(alertSoundPlay) withObject:nil];
    
}
-(void)UpdatingLocation
{
    
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = 10;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    [self.locationManager startUpdatingLocation];
    
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSDictionary *userdata = commonUtils.userData;
    if (userdata) {
        [commonUtils saveUserdata:userdata];
    }
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSDictionary *userdata = commonUtils.userData;
    if (userdata) {
        [commonUtils saveUserdata:userdata];
    }
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - FB Sign up

//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
//    return [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                          openURL:url
//                                                sourceApplication:sourceApplication
//                                                       annotation:annotation
//            ];
//}

#ifdef __IPHONE_9_0 
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary *)options
{
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    return YES;
}
#else 
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url sourceApplication:sourceApplication
                                                annotation:annotation];   return YES;
}
#endif

#pragma mark - Remote Notification
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    NSString* newToken = [[[NSString stringWithFormat:@"%@",deviceToken]
                           stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [commonUtils setUserDefault:@"user_apns_id" withFormat:newToken];
    NSLog(@"My saved token is: %@", [commonUtils getUserDefault:@"user_apns_id"]);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
      NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    appController.apnsMessage = [[NSMutableDictionary alloc] init];
    appController.apnsMessage = [[userInfo objectForKey:@"aps"] objectForKey:@"info"];
    NSLog(@"APNS Info Fetched : %@", userInfo);
    NSLog(@"My Received Message : %@", appController.apnsMessage);
    [commonUtils setUserDefault:@"apns_message_arrived" withFormat:@"1"];
    NSDictionary *aps = userInfo[@"aps"];
    NSDictionary *apsData = aps[@"data"];
    NSString *iceId = [NSString stringWithFormat:@"%@",apsData[@"ice_id"]];
    commonUtils.noti_ice_id = iceId;
    commonUtils.is_Notification = YES;
    if (commonUtils.sessionToken) {
        //        dispatch_async(dispatch_get_main_queue(), ^{
        if (commonUtils.is_Notification) {
            UIApplicationState state = [[UIApplication sharedApplication] applicationState];
            if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
            {
           
                commonUtils.is_Notification = NO;
                
                [self getIceDetail];

                //Do checking here.
            }
                   }
     
        
        
        //        });
    }

    [application setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]];
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    //Do Your Code.................Enjoy!!!!
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
}



#pragma mark - CLLocationManagerDelegate
- (void)updateLocationManager {
    //    if([commonUtils getUserDefault:@"flag_location_query_enabled"] != nil && [[commonUtils getUserDefault:@"flag_location_query_enabled"] isEqualToString:@"1"]) {
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    //[_locationManager setDistanceFilter:804.17f]; // Distance Filter as 0.5 mile (1 mile = 1609.34m)
    //locationManager.distanceFilter=kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingLocation];
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    //    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
    //        [_locationManager requestWhenInUseAuthorization];
    //    }
    
    if(IS_OS_8_OR_LATER) {
        [_locationManager requestAlwaysAuthorization];
    }
    //        [_locationManager startMonitoringSignificantLocationChanges];
    //        [_locationManager startUpdatingLocation];
    //    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
      NSLog(@"didFailWithError: %@", error.localizedDescription);
    [self alertForLocationNotFound];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
   // NSLog(@"didUpdateToLocation: %@", [locations lastObject]);
   currentLocation = [locations lastObject];
    if (currentLocation != nil) {
        BOOL locationChanged = NO;
        if(![commonUtils getUserDefault:@"currentLatitude"] || ![commonUtils getUserDefault:@"currentLongitude"]) {
            locationChanged = YES;
        } else if(![[commonUtils getUserDefault:@"currentLatitude"] isEqualToString:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]] || ![[commonUtils getUserDefault:@"currentLongitude"] isEqualToString:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]]) {
            locationChanged = YES;
        }
        if(locationChanged) {
            [commonUtils setUserDefault:@"currentLatitude" withFormat:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]];
            [commonUtils setUserDefault:@"currentLongitude" withFormat:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]];
            //            [commonUtils setUserDefault:@"barksUpdate" withFormat:@"1"];
        }
    }
    [self getAddressFromCoordinates];
    
    
    NSString * speed = [NSString stringWithFormat:@"%f",currentLocation.speed];
    if([speed doubleValue]<=0)
    {
        speed =@"0";
    }
    NSString *adresis = [NSString stringWithFormat:@"Address not Found"];
    CLGeocoder* geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = placemarks[0];
        if(currentLocation != nil)
        {
            NSString *latitude  =   [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            NSString *longitude =  [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        
            if(lastLocationFnded!=nil)  {
                meters = [currentLocation distanceFromLocation:lastLocationFnded];
                locDistance = meters;
            }
            
          //  NSLog(@"meters %f",meters);
         
            if(commonUtils.sessionToken!=nil)
            {
                lastLocationFnded=currentLocation;
                [self updateLatLngApi:latitude :longitude];
            }
            
            
        };
        
    }];

     //[_locationManager stopUpdatingLocation];
    //    [self updateUserLocation];
}

- (void)updateUserLocation {  //for update user's coordinate automatically
    NSString *msg = [NSString stringWithFormat:@"%@:%@", [commonUtils getUserDefault:@"currentLatitude"], [commonUtils getUserDefault:@"currentLongitude"]];
  //  [commonUtils showAlert:@"Location Updated" withMessage:msg];
}



#pragma mark - Log Out
-(void) logOut
{
    // Remove data from singleton (where all my app data is stored)
    if([[commonUtils getUserDefault:@"logged_out"] isEqualToString:@"1"]) {
        [commonUtils removeUserDefault:@"logged_out"];
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
        [FBSDKAccessToken setCurrentAccessToken:nil];
        
    }
    
    // Show login screen
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    
    [self.window setRootViewController:nav];
    [self.window makeKeyAndVisible];
    
    
}


#pragma mark - AppDelegate shareAppDelegate
+(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
-(void)getAddressFromCoordinates{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      
                    //  NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];

             
                      [commonUtils setUserDefault:@"location" withFormat:[NSString stringWithFormat:@"%@",locatedAt]];
                  }
                  else {
                    //  NSLog(@"Could not locate");
                  }
              }
     ];
}
-(void)alertForLocationNotFound {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"HOLD UP..." message:@"Before you continue, ICE needs access to your location. Turn on location services in your device settings" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go To Settings", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 1) {
        CGFloat systemVersion = [[UIDevice currentDevice].systemVersion floatValue];
        if (systemVersion < 10) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy&path=LOCATION"]];
        }else{
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy&path=LOCATION"]
                                              options:[NSDictionary dictionary]
                                    completionHandler:nil];
        }
    }
    
}
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        AppDelegate *instance = sharedInstance;
        instance.locationManager = [CLLocationManager new];
        instance.locationManager.delegate = instance;
        instance.locationManager.desiredAccuracy = kCLLocationAccuracyBest; // you can use kCLLocationAccuracyHundredMeters to get better battery life
        instance.locationManager.pausesLocationUpdatesAutomatically = NO; // this is important
    });
    
    return sharedInstance;
}
- (void)startUpdatingLocation
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied)
    {
        NSLog(@"Location services are disabled in settings.");
    }
    else
    {
        // for iOS 8
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        // for iOS 9
        if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)])
        {
            [self.locationManager setAllowsBackgroundLocationUpdates:YES];
        }
        
        [self.locationManager startUpdatingLocation];
    }
}
-(void)updateLatLngApi:(NSString*)lat :(NSString*)lng{
    
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            
            NSString *serverUrl = [NSString stringWithFormat:@"%@auto_Checkin?lat=%@&lng=%@",ServerUrl,lat,lng];
           // NSLog(@"serverUrl %@",serverUrl);
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
            // Create a mutable copy of the immutable request and add more headers
            NSMutableURLRequest *mutableRequest = [request mutableCopy];
            [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            
            // Now set our request variable with an (immutable) copy of the altered request
            request = [mutableRequest copy];
            
            // Log the output to make sure our new headers are there
           // NSLog(@"%@", request.allHTTPHeaderFields);
            
            
            NSURLResponse *response;
            
            NSError *error = nil;
            
            NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            if(error!=nil)
            {
               // NSLog(@"web service error:%@",error);
            }
            else
            {
                if(receivedData !=nil)
                {
                    NSError *Jerror = nil;
                    
                    NSDictionary* json =[NSJSONSerialization
                                         JSONObjectWithData:receivedData
                                         options:kNilOptions
                                         error:&Jerror];
                    // NSLog(@"user data is %@",json);
                    
                    if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            //Run UI Updates
                            NSDictionary *successDic = [json[@"successData"]mutableCopy];
                         //   NSLog(@"successDic %@",successDic);
                            
                            
                            
                          
                            
                            
                        });
                        
                        
                        
                        
                        //  NSLog(@"live array is %@",commonUtils.comingSoonArray);
                        
                    }
                    if(Jerror!=nil)
                    {
                    //    NSLog(@"json error:%@",Jerror);
                    }
                }
            }
        });
    
}
-(void)getIceDetail{
    
    //Background Thread
    dispatch_async(dispatch_get_main_queue(), ^(void){
       
    });
    
    // commonUtils.noti_ice_id = @"418";//Demo ice id for testing
    NSString *serverUrl = [NSString stringWithFormat:@"%@get_ice/%@",ServerUrl,commonUtils.noti_ice_id];
    NSLog(@"server url %@",serverUrl);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
    // Create a mutable copy of the immutable request and add more headers
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
    
    
    
    NSURLResponse *response;
    
    NSError *error = nil;
    
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(error!=nil)
    {
        NSLog(@"web service error:%@",error);
    }
    else
    {
        if(receivedData !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:receivedData
                                 options:kNilOptions
                                 error:&Jerror];
            
            
            if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    NSDictionary *successDic = [json[@"successData"]mutableCopy];
                    commonUtils.alertDic = [successDic mutableCopy];
                    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                    DetailVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"DetailVC"];
                    vc.iceDetails = commonUtils.alertDic;
                    vc.comingFrom = @"alert";
                    vc.eventType = @"alert";
                    UINavigationController *navController = (UINavigationController  *)self.window.rootViewController;
                    [navController pushViewController:vc animated:YES];
                  
                    
                    
                    
                });
                
                
                
                
                NSLog(@"live array is %@",commonUtils.liveEventsArray);
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^(void){
               
                });
                
                //Run UI Updates
                
                
            }
            if(Jerror!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^(void){
               
                });
                
                NSLog(@"json error:%@",Jerror);
            }
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^(void){

    });
    
    
    
}
-(void)showIceDoneAlert{
    [_iceDoneAlert show];
}
-(void)iceDone:(UIButton *)sender {
    [AppDelegate.sharedAppDelegate.iceDoneAlert close];
    
    NSLog(@"passed array is %@",imagesArrayCopy);
    //  NSLog(@"images array copy is %@",imagesArrayCopy);
    NSMutableArray *imagesArrayCopy2 = [NSMutableArray new];
    
    for (int i= 0; i<imagesArrayCopy.count; i++) {
        if ([imagesArrayCopy[i][@"image"]isKindOfClass:[UIImage class]]) {
            [imagesArrayCopy2 addObject:imagesArrayCopy[i][@"image"]];
        }
        else{
            NSData *postData = imagesArrayCopy[i][@"videodata"];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSLog(@"paths is %@",paths);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"myMove%d.mp4",i+1]];
            
            [postData writeToFile:path atomically:YES];
            NSURL *moveUrl = [NSURL fileURLWithPath:path];
            [imagesArrayCopy2 addObject:moveUrl];
            NSLog(@"images array copy 2 is %@",imagesArrayCopy2);
        }
        
    }
    if ([commonUtils.sharingOptions[@"facebook"]isEqualToString:@"yes"]) {
        UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:imagesArrayCopy2 applicationActivities:nil];
        controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            // When completed flag is YES, user performed specific activity
            NSLog(@"pending alerts is %@",commonUtils.pendingAlerts);
            if (commonUtils.pendingAlerts.count > 0) {
                CustomIOSAlertView *alert = commonUtils.pendingAlerts.firstObject;
                [commonUtils.pendingAlerts removeObjectAtIndex:0];
                self.visibleAlertView = alert;
                [alert show];
                
            }
            else{
                self.visibleAlertView = nil;
                //             HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
                //             [homeVC onShowTabView:2];
                //             [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
                //             [self.sideMenuController hideLeftViewAnimated:sender];
                //             NSLog(@"%@", commonUtils.sharingOptions);
            }
            
            
        };
        // and present it
        
        [ROOTVIEW presentViewController:controller animated:YES completion:^{
            // executes after the user selects something
        }];
        NSLog(@"share fb");
    }
    else{
        //        HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
        //        [homeVC onShowTabView:2];
        //        [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
        //        [self.sideMenuController hideLeftViewAnimated:sender];
        
    }
    
    
}
    

@end

